<?php

define("MYSQL_HOST","");
define("MYSQL_PORT","");
define("MYSQL_USER","");
define("MYSQL_PASSWORD","");
define("MYSQL_DATABASE","");

define("TEXTURES_UPLOAD_SCRIPT","");
define("TEXTURES_UPLOAD_SECRET","");
define("TEXTURES_FINAL_URL","");

define("DISQUS_EMBED_URL","");

define("CHECK_UPDATE_ON_PROFILE_LOAD","");

define("SKIN_FILE_LIST_ITEMS_PER_PAGE","");

define("GIGADRIVE_API_KEY","");

define("CRONJOB_SECRET","");
