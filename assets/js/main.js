function setCookie(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function saveDismiss(id){
	setCookie("registeredAlert"+id,"closed",10);
}

function toggleFavorite(e,skinFileID){
	if(~e.innerHTML.indexOf("fas fa-heart")){
		$(e).tooltip('hide');
		e.innerHTML = '<i class="fas fa-spinner fa-pulse" style="font-size: 24px;"></i>';

		$.ajax({
			url: "/scripts/toggleFavorite/" + skinFileID,
			success: function(result){
				var json = result;

				if(json.hasOwnProperty("success")){
					if(json.success == "Favorite added"){
						e.innerHTML = '<i class="fas fa-heart" style="font-size: 24px; color: #FF0000;"></i>';
						$(e).attr('data-original-title', 'Remove from favorites').tooltip('show');
					} else {
						e.innerHTML = '<i class="fas fa-heart" style="font-size: 24px; color: #ADB5BD;"></i>';
						$(e).attr('data-original-title', 'Add to favorites').tooltip('show');
					}
				} else {
					console.log("Favorite failed: " + json.error);
				}
			}
		});
	}
}

function number_format (number, decimals, dec_point, thousands_sep) {
    // Strip all characters but numerical ones.
    number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
    var n = !isFinite(+number) ? 0 : +number,
        prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
        sep = (typeof thousands_sep === 'undefined') ? ',' : thousands_sep,
        dec = (typeof dec_point === 'undefined') ? '.' : dec_point,
        s = '',
        toFixedFix = function (n, prec) {
            var k = Math.pow(10, prec);
            return '' + Math.round(n * k) / k;
        };
    // Fix for IE parseFloat(0.55).toFixed(0) = 0;
    s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
    if (s[0].length > 3) {
        s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
    }
    if ((s[1] || '').length < prec) {
        s[1] = s[1] || '';
        s[1] += new Array(prec - s[1].length + 1).join('0');
    }
    return s.join(dec);
}