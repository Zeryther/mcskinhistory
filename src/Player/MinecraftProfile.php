<?php 

class MinecraftProfile {
    private $username;
    private $uuid;
    private $properties;

    /**
     * @param string $username The player's username.
     * @param string $uuid The player's UUID.
     * @param array $properties The player's properties specified on their Mojang profile.
     */
    function __CONSTRUCT($username, $uuid, $properties = array()) {
        $this->username = $username;
        $this->uuid = $uuid;
        $this->properties = $properties;
    }

    /**
     * @return string The player's username.
     */
    public function getUsername() {
        return $this->username;
    }

    /**
     * @return string The player's UUID.
     */
    public function getUUID() {
        return $this->uuid;
    }

    /**
     * @return array The player's properties listed on their mojang profile.
     */
    public function getProperties() {
        return $this->properties;
    }

    /**
     * @return array Returns an array with keys of 'properties, usernname and uuid'.
     */
    public function getProfileAsArray() {
        return array("username" => $this->username, "uuid" => $this->uuid, "properties" => $this->properties);
	}

	public function getSkinURL(){
		if(isset(json_decode(base64_decode($this->getProperties()[0]["value"]),true)["textures"]["SKIN"]["url"])){
			return json_decode(base64_decode($this->getProperties()[0]["value"]),true)["textures"]["SKIN"]["url"];
		} else {
			return null;
		}
	}

	public function getSkinSignature(){
		if(isset($this->getProperties()[0]["signature"])){
			return $this->getProperties()[0]["signature"];
		} else {
			return null;
		}
	}

	public function getSkinModel(){
		if(isset(json_decode(base64_decode($this->getProperties()[0]["value"]),true)["textures"]["SKIN"]["metadata"])){
			return "ALEX";
		} else {
			return "STEVE";
		}
	}

	public function getCapeURL(){
		if(isset(json_decode(base64_decode($this->getProperties()[0]["value"]),true)["textures"]["CAPE"]["url"])){
			return json_decode(base64_decode($this->getProperties()[0]["value"]),true)["textures"]["CAPE"]["url"];
		} else {
			return null;
		}
	}
	
	public static function getProfile($id){
		$n = "MinecraftProfile_" . $id;
	
		if(\CacheHandler::existsInCache($n)){
			return \CacheHandler::getFromCache($n);
		} else {
			$profile = ProfileUtils::getProfile($id);
	
			if($profile != null){
				\CacheHandler::setToCache("MinecraftProfile_" . $profile->getUUID(),$profile,3600);
			}
	
			return $profile;
		}
	}
}