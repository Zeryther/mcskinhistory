<?php

namespace SkinHistory\Player;

use SkinHistory\Account\Account;
use SkinHistory\Player\Name\MinecraftName;
use SkinHistory\Skin\SkinFile;
use SkinHistory\Skin\Texture;
use SkinHistory\Util\Util;


/**
 * Represents a Minecraft player from MCSkinHistory's database
 * 
 * @package Player
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class Player {
	/**
	 * Gets a player from their UUID or username.
	 * 
	 * @access public
	 * @param string $id The UUID or username of this player.
	 * @return Player|null The Player object, null if the player could not be found.
	 */
	public static function getPlayer($id){
		$id = str_replace("-","",$id);
		$n = "player_" . strtolower($id);

		if(\CacheHandler::existsInCache($n)){
			return \CacheHandler::getFromCache($n);
		} else {
			$profile = \MinecraftProfile::getProfile($id);

			if(!is_null($profile)){
				new Player($profile);
				
				if(\CacheHandler::existsInCache($n)){
					return \CacheHandler::getFromCache($n);
				} else {
					return null;
				}
			} else {
				\CacheHandler::setToCache($n,null,5*60);
				return null;
			}
		}
	}

	/**
	 * @access private
	 * @var MinecraftProfile $profile The profile of this user, holding their UUID, username and texture properties.
	 */
	private $profile;

	/**
	 * @access private
	 * @var string|null $lastCheckTextures The timestamp of when the textures of this player were last updated, null if they were never updated.
	 */
	private $lastCheckTextures;

	/**
	 * @access private
	 * @var string|null $lastCheckNameHistory The timestamp of when the name history of this player was last updated, null if it was never updated.
	 */
	private $lastCheckNameHistory;

	/**
	 * @access private
	 * @var array|null $socialMedia The social media data of this player.
	 */
	private $socialMedia;

	/**
	 * @access private
	 * @var MinecraftName[]|null $nameHistory The name history of this player, null if it has not been fetched yet.
	 */
	private $nameHistory;

	/**
	 * @access private
	 * @var string $timeFound The timestamp of when this player was added to the database.
	 */
	private $timeFound;

	/**
	 * @access private
	 * @var int $claimingAccount The ID of the account that has claimed this player, -1 if the claiming account has not been fetched yet, 0 if there is no claiming account.
	 */
	private $claimingAccount = -1;

	/**
	 * @access private
	 * @var Texture[]|null $textures The textures of this player, null if the textures have not been fetched yet.
	 */
	private $textures;

	/**
	 * Constructor
	 * 
	 * @access protected
	 * @param MinecraftProfile $profile The profile of this player.
	 */
	protected function __construct($profile) {
		$mysqli = \Database::Instance()->get();

		$this->profile = $profile;
		$uuid = $this->getUUID();

		if($stmt = $mysqli->prepare("SELECT `uuid`,`username`,`lastCheck.textures`,`lastCheck.nameHistory`,`socialMedia`,`time` FROM `skinhistory_players` WHERE `uuid` = ?")){
			$stmt->bind_param("s",$uuid);
			$stmt->execute();
			$stmt->bind_result($uuid,$username,$lastCheckTextures,$lastCheckNameHistory,$socialMedia,$time);

			if($stmt->fetch()){
				// PLAYER IS ALREADY IN DATABASE
				if(is_null($socialMedia)){
					$this->socialMedia = array();
				} else {
					$this->socialMedia = json_decode($socialMedia,true);
				}

				$this->timeFound = $time;
				$this->saveToCache();
				$stmt->close();
				$this->syncName();
			} else {
				// PLAYER IS NOT IN DATABASE
				$insert = $mysqli->prepare("INSERT INTO `skinhistory_players` (`uuid`,`username`) VALUES(?,?);");
				$insert->bind_param("ss",$this->getUUID(),$this->getUsername());
				$insert->execute();
				$insert->close();
				new Player($profile);
				$stmt->close();
			}
		}
	}

	/**
	 * @return MinecraftProfile The profile of this user, holding their UUID, username and texture properties.
	 */
	public function getProfile(){
		return $this->profile;
	}

	/**
	 * @return string The UUID of this player without dashes.
	 */
	public function getUUID(){
		return str_replace("-","",$this->profile->getUUID());
	}

	/**
	 * @return string The UUID of this player with dashes.
	 */
	public function getFormattedUUID(){
		return \ProfileUtils::formatUUID($this->getUUID());
	}

	/**
	 * @return string The username of this player.
	 */
	public function getUsername(){
		return $this->profile->getUsername();
	}

	/**
	 * @return string The timestamp of when the textures of this player were last updated, null if they were never updated.
	 */
	public function getLastTexturesCheck(){
		return $this->lastCheckTextures;
	}

	/**
	 * @return string The timestamp of when the name history of this player was last updated, null if it was never updated.
	 */
	public function getLastNameHistoryCheck(){
		return $this->lastCheckNameHistory;
	}

	/**
	 * @return array The social media data of this player.
	 */
	public function getSocialMedia(){
		return $this->socialMedia;
	}

	/**
	 * Updates this player's social media object.
	 * 
	 * @access public
	 * @param array $socialMedia The new social media object
	 */
	public function setSocialMedia($socialMedia){
		$this->socialMedia = $socialMedia;
		$this->saveToCache();
	}

	/**
	 * @access public
	 * @return MinecraftName[] The name history of this player, null if it has not been fetched yet.
	 */
	public function getNameHistory(){
		$this->loadNameHistory();

		return $this->nameHistory;
	}

	/**
	 * Updates the timestamp of when this player was added to the database.
	 * 
	 * @access public
	 * @param string $time The new timestamp
	 */
	public function setFoundTime($time){
		$this->timeFound = $time;
	}

	/**
	 * @return string The timestamp of when this player was added to the database.
	 */
	public function getFoundTime(){
		return $this->timeFound;
	}

	/**
	 * @return Account|null The claiming account of this player, null if there is no claiming account.
	 */
	public function getClaimingAccount(){
		if($this->claimingAccount == -1){
			$this->reloadClaimingAccount();
		}

		return $this->claimingAccount == 0 ? null : Account::getAccount($this->claimingAccount);
	}

	/**
	 * @return SkinFile|null The SkinFile object of the current skin, null if it could not be fetched.
	 */
	public function getCurrentSkinAsSkinFile(){
		$currentSkinURL = $this->getProfile()->getSkinURL();

		foreach($this->getTextures() as $texture){
			if($texture->getSkin()->getFullURL() == $currentSkinURL){
				return $texture->getSkin();
			}
		}

		return null;
	}

	/**
	 * @access private
	 */
	public function reloadClaimingAccount(){
		$mysqli = \Database::Instance()->get();

		$uuid = $this->getUUID();

		$stmt = $mysqli->prepare("SELECT * FROM `skinhistory_linkedPlayers` WHERE `uuid` = ?");
		$stmt->bind_param("s",$uuid);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows){
			$row = $result->fetch_assoc();

			$this->claimingAccount = $row["account"];
		} else {
			$this->claimingAccount = 0;
		}

		$stmt->close();
		$this->saveToCache();
	}

	/**
	 * @access private
	 */
	public function clearClaimingAccount(){
		$this->claimingAccount = 0;
		$this->saveToCache();
	}

	/**
	 * Updates all changes made to this player object in the database and the cache.
	 * 
	 * @access public
	 */
	public function saveToDatabase(){
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("UPDATE `skinhistory_players` SET `username` = ?, `lastCheck.textures` = ?, `lastCheck.nameHistory` = ?, `socialMedia` = ?, `time` = ? WHERE `uuid` = ?");
		$stmt->bind_param("ssssss",$this->getUsername(),$this->getLastTexturesCheck(),$this->getLastNameHistoryCheck(),json_encode($this->getSocialMedia()),$this->timeFound,$this->getUUID());
		$stmt->execute();
		$stmt->close();

		$this->saveToCache();
	}

	/**
	 * Saves this player to the cache.
	 * 
	 * @access public
	 */
	public function saveToCache(){
		\CacheHandler::setToCache("player_" . $this->getUUID(),$this,60*60);
		\CacheHandler::setToCache("player_" . strtolower($this->getUsername()),$this,60*60);
	}

	/**
	 * Updates this player's username in the database.
	 * 
	 * @access public
	 */
	public function syncName(){
		$mysqli = \Database::Instance()->get();

		$name = $this->getUsername();
		$uuid = $this->getUUID();

		$u = $mysqli->prepare("UPDATE `skinhistory_players` SET `username` = ? WHERE `uuid` = ?");
		$u->bind_param("ss",$name,$uuid);
		$u->execute();
		$u->close();
	}

	/**
	 * Reloads the textures of this player.
	 * 
	 * @access public
	 */
	public function reloadTextures(){
		$this->textures = array();
		$mysqli = \Database::Instance()->get();

		$uuid = $this->getUUID();

		$stmt = $mysqli->prepare("SELECT t.id,t.uuid,t.time,s.id,s.title,s.md5,s.full_url,s.firstUser,s.type,s.model,s.isBanned,s.randomizer,s.textureData,s.time FROM `skinhistory_textures` AS t INNER JOIN `skinhistory_skinfiles` AS s ON t.skinFile = s.id WHERE `uuid` = ? ORDER BY t.time DESC");
		$stmt->bind_param("s",$uuid);
		$stmt->execute();
		$stmt->bind_result($id,$uuid,$time,$skin_id,$skin_title,$skin_md5,$skin_fullURL,$skin_firstUser,$skin_type,$skin_model,$skin_isBanned,$skin_randomizer,$skin_textureData,$skin_time);

		while($stmt->fetch()){
			$texture = Texture::getTextureFromData($id,$uuid,array(
				"id" => $skin_id,
				"title" => $skin_title,
				"md5" => $skin_md5,
				"full_url" => $skin_fullURL,
				"firstUser" => $skin_firstUser,
				"type" => $skin_type,
				"model" => $skin_model,
				"isBanned" => $skin_isBanned,
				"randomizer" => $skin_randomizer,
				"textureData" => $skin_textureData,
				"time" => $skin_time
			),$time);

			array_push($this->textures,$texture);
		}
	
		$stmt->close();
	}

	/**
	 * Removes a texture from this player's record.
	 * 
	 * @access public
	 * @param int $id The ID of the texture to remove.
	 * @return bool If true, the texture was successfully removed.
	 */
	public function removeTexture($id){
		$removed = false;
		$newArray = array();

		foreach($this->getTextures() as $texture){
			if($texture->getID() == $id){
				$removed = true;
			} else {
				array_push($newArray,$texture);
			}
		}

		$this->textures = $newArray;

		if($removed){
			$mysqli = \Database::Instance()->get();
			$stmt = $mysqli->prepare("DELETE FROM `skinhistory_textures` WHERE `id` = ?");
			$stmt->bind_param("i",$id);
			$stmt->execute();
			if(!empty($stmt->error)) $removed = $stmt->error;
			$stmt->close();
		}

		$this->saveToCache();
		return $removed;
	}

	/**
	 * Gets this players textures
	 * 
	 * @access public
	 * @param string $type The type of skin file to filter, if null, all textures will be returned.
	 * @return Texture[]
	 */
	public function getTextures($type = NULL){
		if(is_null($this->textures)){
			$this->reloadTextures();
		}

		if($type != NULL){
			$array = array();

			foreach($this->textures as $texture){
				if($texture->getSkin()->getType() == $type){
					array_push($array,$texture);
				}
			}

			return $array;
		} else {
			return $this->textures;
		}
	}

	/**
	 * @access public
	 * @param int $skinFileID The skin file ID
	 * @return Texture|null The texture object with the passed skin file ID, null if there is none.
	 */
	public function getTextureBySkinFileID($skinFileID){
		foreach($this->getTextures() as $texture){
			if($texture->getSkin()->getID() == $skinFileID){
				return $texture;
			}
		}

		return null;
	}

	/**
	 * @access public
	 * @param string $md5 The 
	 * @return bool If true, the player has a texture with this md5 hash.
	 */
	public function hasTexture($md5){
		foreach($this->getTextures() as $texture){
			$skin = $texture->getSkin();

			if($skin->getMD5() == $md5){
				return true;
			}
		}

		return false;
	}

	/**
	 * Adds this player to the check queue.
	 * 
	 * @access public
	 */
	public function addToQueue(){
		$mysqli = \Database::Instance()->get();
		$cacheName = "queue_" . $this->getUUID();

		if(!\CacheHandler::existsInCache($cacheName)){
			$inQueue = false;

			$uuid = $this->getUUID();

			$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_checkQueue` (`uuid`) VALUES(?);");
			$stmt->bind_param("s",$uuid);
			$stmt->execute();
			$stmt->close();

			\CacheHandler::setToCache($cacheName,$cacheName,10*60);
		}
	}

	// load data

	/**
	 * @access private
	 */
	private function loadNameHistory(){
		if($this->nameHistory == null || !isset($this->nameHistory) || empty($this->nameHistory) || count($this->nameHistory) == 0){
			$url = "https://api.mojang.com/user/profiles/" . $this->getUUID() . "/names";
            $curl = curl_init();
            $options = array(
                CURLOPT_URL => $url,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_TIMEOUT => 5,
                CURLOPT_USERAGENT => USER_AGENT_BOT_NAME,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_MAXREDIRS => 10
            );
            curl_setopt_array($curl,$options);

            $result = curl_exec($curl);
            curl_close($curl);
            $this->nameHistory = array();
            if(isset($result) && $result != null && $result != false){
                $data = json_decode($result,true);

                $i = 0;
                while(isset($data[$i])){
                    $date = 0;

                    if(isset($data[$i]["changedToAt"])) $date = $data[$i]["changedToAt"];

                    array_push($this->nameHistory,new MinecraftName($this->getUUID(),$data[$i]["name"],$date));

                    Util::tryLoggingName($this->getUUID(),$data[$i]["name"],$date);

                    $i++;
                }
            } else {
                array_push($this->nameHistory,new MinecraftName($this->getUUID(),$name,0));
			}
			
			$this->lastCheckNameHistory = date("Y-m-d H:i:s");

			$lc = $this->getLastNameHistoryCheck();
			$uuid = $this->getUUID();

			$mysqli = \Database::Instance()->get();
            $stmt = $mysqli->prepare("UPDATE `skinhistory_players` SET `lastCheck.nameHistory` = ? WHERE `uuid` = ?");
			$stmt->bind_param("ss",$lc,$uuid);
			$stmt->execute();
			$stmt->close();

            $this->nameHistory = array_reverse($this->nameHistory);

            $this->saveToCache();
		}
	}

	/**
	 * Updates the textures of this player
	 * 
	 * @access public
	 * @param string $type The type of texture to update (use SKIN_TYPE_ constants)
	 * @param bool $verbose If true, will print debug information
	 */
	public function checkUpdate($type, $verbose = false){
		if($type == SKIN_TYPE_MOJANG){
			$this->_checkmojang($verbose);
		} else if($type == SKIN_TYPE_OPTIFINE){
			$this->_checkoptifine($verbose);
		} else if($type == SKIN_TYPE_LABYMOD){
			$this->_checklabymod($verbose);
		}
	}

	/////////////////////////////////////////////

	/**
	 * @access private
	 */
	private function _checkmojang($verbose){
		$mysqli = \Database::Instance()->get();

		// UPDATE SKIN
		$skinURL = $this->getProfile()->getSkinURL();
		$skinModel = $this->getProfile()->getSkinModel();
		$skinContent = file_get_contents($skinURL,false,stream_context_create(array("http"=>array("method"=>"GET","user_agent"=>USER_AGENT_BOT_NAME))));
		$skinMD5 = md5($skinContent);
		$skinSignature = $this->getProfile()->getSkinSignature();

		if(get_headers($skinURL)[0] == "HTTP/1.1 200 OK"){
			if($this->hasTexture($skinMD5) == false){
				$tmp = null;
				while($tmp == null || file_exists($tmp)) $tmp = $_SERVER["DOCUMENT_ROOT"] . "/tmp/" . rand(1,99999) . ".png";
				file_put_contents($tmp,$skinContent);
	
				if(file_exists($tmp)){
					//$fullURL = Util::storeTexture($tmp);
					$fullURL = $skinURL; // keep minecraft texture server skin url to reduce used disk space
	
					if($fullURL != null){
						$skinFileID = SkinFile::createSkinFile(array(
							"md5" => $skinMD5,
							"full_url" => $fullURL,
							"firstUser" => $this->getUUID(),
							"type" => "SKIN",
							"model" => $skinModel,
							"textureData" => array(
								"url" => $fullURL,
								"signature" => $skinSignature,
								"uuid" => $this->getUUID(),
								"value" => $this->getProfile()->getProperties()[0]["value"]
							)
						));
	
						if($skinFileID != null){
							$texture = Texture::createTexture(array(
								"uuid" => $this->getUUID(),
								"skinFile" => $skinFileID
							));
	
							if($texture != null){
								array_unshift($this->textures,Texture::getTextureFromID($texture));
								$this->saveToCache();
	
								if($verbose == true)
									echo $this->getUUID() . ': ' . "Skin added!" . '<br/>';
							} else {
								if($verbose == true)
									echo $this->getUUID() . ': ' . "Skin texture is null!" . '<br/>';
							}
						} else {
							if($verbose == true)
								echo $this->getUUID() . ': ' . "Skin ID is null!" . '<br/>';
						}
					} else {
						if($verbose == true)
							echo $this->getUUID() . ': ' . "Skin Full URL is null" . '<br/>';
					}
	
					unlink($tmp);
				} else {
					if($verbose == true)
						echo $this->getUUID() . ': ' . "Skin Temp file was not saved!" . '<br/>';
				}
			} else {
				if($verbose == true)
					echo $this->getUUID() . ': ' . "Player already has skin with md5" . '<br/>';
			}
		} else {
			if($verbose == true)
				echo $this->getUUID() . ': ' . "Could not fetch image" . '<br/>';
		}

		// UPDATE CAPE
		$capeURL = $this->getProfile()->getCapeURL();
		
		if($capeURL != null && get_headers($capeURL)[0] == "HTTP/1.1 200 OK"){
			if($capeURL != null){
				$capeModel = null;
				$capeContent = file_get_contents($capeURL,false,stream_context_create(array("http"=>array("method"=>"GET","user_agent"=>USER_AGENT_BOT_NAME))));
				$capeMD5 = md5($capeContent);
	
				if($this->hasTexture($capeMD5) == false){
					$tmp = null;
					while($tmp == null || file_exists($tmp)) $tmp = $_SERVER["DOCUMENT_ROOT"] . "/tmp/" . rand(1,99999) . ".png";
					file_put_contents($tmp,$capeContent);
	
					if(file_exists($tmp)){
						//$fullURL = Util::storeTexture($tmp);
						$fullURL = $capeURL; // keep minecraft texture server skin url to reduce used disk space
	
						if($fullURL != null){
							$skinFileID = SkinFile::createSkinFile(array(
								"md5" => $capeMD5,
								"full_url" => $fullURL,
								"firstUser" => $this->getUUID(),
								"type" => "CAPE_MOJANG",
								"model" => $capeModel,
								"textureData" => null
							));
	
							if($skinFileID != null){
								$texture = Texture::createTexture(array(
									"uuid" => $this->getUUID(),
									"skinFile" => $skinFileID
								));
	
								if($texture != null){
									array_unshift($this->textures,Texture::getTextureFromID($texture));
									$this->saveToCache();
	
									if($verbose == true)
										echo $this->getUUID() . ': ' . "Cape added!" . '<br/>';
								} else {
									if($verbose == true)
										echo $this->getUUID() . ': ' . "Cape texture is null!" . '<br/>';
								}
							} else {
								if($verbose == true)
									echo $this->getUUID() . ': ' . "Cape ID is null!" . '<br/>';
							}
						} else {
							if($verbose == true)
								echo $this->getUUID() . ': ' . "Cape Full URL is null" . '<br/>';
						}
	
						unlink($tmp);
					} else {
						if($verbose == true)
							echo $this->getUUID() . ': ' . "Cape Temp file was not saved!" . '<br/>';
					}
				} else {
					if($verbose == true)
						echo $this->getUUID() . ': ' . "Player already has Cape with md5" . '<br/>';
				}
			}
		} else {
			if($verbose == true)
				echo $this->getUUID() . ': ' . "Could not fetch image" . '<br/>';
		}

		// UPDATE LAST CHECK DATE
		$this->lastCheckTextures = date("Y-m-d H:i:s");

		$lc = $this->getLastTexturesCheck();
		$uuid = $this->getUUID();

		$stmt = $mysqli->prepare("UPDATE `skinhistory_players` SET `lastCheck.textures` = ? WHERE `uuid` = ?");
		$stmt->bind_param("ss",$lc,$uuid);
		$stmt->execute();
		$stmt->close();

		$this->saveToCache();
	}

	/**
	 * @access private
	 */
	private function _checkoptifine($verbose){
		$mysqli = \Database::Instance()->get();

		// UPDATE CAPE
		$capeURL = "http://optifine.net/capes/" . $this->getUsername() . ".png";
		
		if(get_headers($capeURL)[0] == "HTTP/1.1 200 OK"){
			if($capeURL != null){
				$capeModel = null;
				$capeContent = file_get_contents($capeURL,false,stream_context_create(array("http"=>array("method"=>"GET","user_agent"=>USER_AGENT_BOT_NAME))));
				$capeMD5 = md5($capeContent);
	
				if($this->hasTexture($capeMD5) == false){
					$tmp = null;
					while($tmp == null || file_exists($tmp)) $tmp = $_SERVER["DOCUMENT_ROOT"] . "/tmp/" . rand(1,99999) . ".png";
					file_put_contents($tmp,$capeContent);
	
					if(file_exists($tmp)){
						$fullURL = Util::storeTexture($tmp);
	
						if($fullURL != null){
							$skinFileID = SkinFile::createSkinFile(array(
								"md5" => $capeMD5,
								"full_url" => $fullURL,
								"firstUser" => $this->getUUID(),
								"type" => "CAPE_OPTIFINE",
								"model" => $capeModel,
								"textureData" => null
							));
	
							if($skinFileID != null){
								$texture = Texture::createTexture(array(
									"uuid" => $this->getUUID(),
									"skinFile" => $skinFileID
								));
	
								if($texture != null){
									array_unshift($this->textures,Texture::getTextureFromID($texture));
									$this->saveToCache();
	
									if($verbose == true)
										echo $this->getUUID() . ': ' . "Cape added!" . '<br/>';
								} else {
									if($verbose == true)
										echo $this->getUUID() . ': ' . "Cape texture is null!" . '<br/>';
								}
							} else {
								if($verbose == true)
									echo $this->getUUID() . ': ' . "Cape ID is null!" . '<br/>';
							}
						} else {
							if($verbose == true)
								echo $this->getUUID() . ': ' . "Cape Full URL is null" . '<br/>';
						}
	
						unlink($tmp);
					} else {
						if($verbose == true)
							echo $this->getUUID() . ': ' . "Cape Temp file was not saved!" . '<br/>';
					}
				} else {
					if($verbose == true)
						echo $this->getUUID() . ': ' . "Player already has Cape with md5" . '<br/>';
				}
			}
		} else {
			if($verbose == true)
				echo $this->getUUID() . ': ' . "Could not fetch image" . '<br/>';
		}

		// UPDATE LAST CHECK DATE
		$this->lastCheckTextures = date("Y-m-d H:i:s");

		$lc = $this->getLastTexturesCheck();
		$uuid = $this->getUUID();

		$stmt = $mysqli->prepare("UPDATE `skinhistory_players` SET `lastCheck.textures` = ? WHERE `uuid` = ?");
		$stmt->bind_param("ss",$lc,$uuid);
		$stmt->execute();
		$stmt->close();

		$this->saveToCache();
	}

	/**
	 * @access private
	 */
	private function _checklabymod($verbose){
		$mysqli = \Database::Instance()->get();

		// UPDATE CAPE
		$capeURL = "http://info.labymod.net/capes/" . $this->getFormattedUUID();
		
		if(get_headers($capeURL)[0] == "HTTP/1.1 200 OK"){
			if($capeURL != null){
				$capeModel = null;
				$capeContent = file_get_contents($capeURL,false,stream_context_create(array("http"=>array("method"=>"GET","user_agent"=>USER_AGENT_BOT_NAME))));
				$capeMD5 = md5($capeContent);
	
				if($this->hasTexture($capeMD5) == false){
					$tmp = null;
					while($tmp == null || file_exists($tmp)) $tmp = $_SERVER["DOCUMENT_ROOT"] . "/tmp/" . rand(1,99999) . ".png";
					file_put_contents($tmp,$capeContent);
	
					if(file_exists($tmp)){
						$fullURL = Util::storeTexture($tmp);
	
						if($fullURL != null){
							$skinFileID = SkinFile::createSkinFile(array(
								"md5" => $capeMD5,
								"full_url" => $fullURL,
								"firstUser" => $this->getUUID(),
								"type" => "CAPE_LABYMOD",
								"model" => $capeModel,
								"textureData" => null
							));
	
							if($skinFileID != null){
								$texture = Texture::createTexture(array(
									"uuid" => $this->getUUID(),
									"skinFile" => $skinFileID
								));
	
								if($texture != null){
									array_unshift($this->textures,Texture::getTextureFromID($texture));
									$this->saveToCache();
	
									if($verbose == true)
										echo $this->getUUID() . ': ' . "Cape added!" . '<br/>';
								} else {
									if($verbose == true)
										echo $this->getUUID() . ': ' . "Cape texture is null!" . '<br/>';
								}
							} else {
								if($verbose == true)
									echo $this->getUUID() . ': ' . "Cape ID is null!" . '<br/>';
							}
						} else {
							if($verbose == true)
								echo $this->getUUID() . ': ' . "Cape Full URL is null" . '<br/>';
						}
	
						unlink($tmp);
					} else {
						if($verbose == true)
							echo $this->getUUID() . ': ' . "Cape Temp file was not saved!" . '<br/>';
					}
				} else {
					if($verbose == true)
						echo $this->getUUID() . ': ' . "Player already has Cape with md5" . '<br/>';
				}
			}
		} else {
			if($verbose == true)
				echo $this->getUUID() . ': ' . "Could not fetch image" . '<br/>';
		}

		// UPDATE LAST CHECK DATE
		$this->lastCheckTextures = date("Y-m-d H:i:s");

		$lc = $this->getLastTexturesCheck();
		$uuid = $this->getUUID();

		$stmt = $mysqli->prepare("UPDATE `skinhistory_players` SET `lastCheck.textures` = ? WHERE `uuid` = ?");
		$stmt->bind_param("ss",$lc,$uuid);
		$stmt->execute();
		$stmt->close();

		$this->saveToCache();
	}
}