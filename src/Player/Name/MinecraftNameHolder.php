<?php

namespace SkinHistory\Player\Name;

/**
 * Represents an object that holds data about the names used by a Minecraft player
 * 
 * @package Name
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class MinecraftNameHolder {
	/**
	 * @var string $uuid The UUID of this name holder.
	 */
	private $uuid;

	/**
	 * @var MinecraftName[] $names The names of this name holder.
	 */
	private $names;

	public function __construct($uuid,$names){
		$this->uuid = $uuid;
		$this->names = $names;
	}

	/**
	 * @var string The UUID of this name holder.
	 */
	public function getUUID(){
		return $this->uuid;
	}

	/**
	 * @return MinecraftName[] The names of this name holder.
	 */
	public function getNames(){
		return $this->names;
	}
}