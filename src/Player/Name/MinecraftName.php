<?php

namespace SkinHistory\Player\Name;

/**
 * Represents data about a Minecraft name used by a player
 * 
 * @package Name
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class MinecraftName {
	/**
	 * @var string $uuid The UUID associated with this name object.
	 */
	private $uuid;

	/**
	 * @var string $name The name associated with this name object.
	 */
	private $name;

	/**
	 * @var string $date The date of when this name was applied.
	 */
	private $date;

	public function __construct($uuid,$name,$date){
		$this->uuid = $uuid;
		$this->name = $name;
		$this->date = $date;
	}

	/**
	 * @return string The UUID associated with this name.
	 */
	public function getUUID(){
		return $this->uuid;
	}

	/**
	 * @return string The name associated with this name object.
	 */
	public function getName(){
		return $this->name;
	}

	/**
	 * @return string The date of when this name was applied.
	 */
	public function getDate(){
		return $this->date;
	}
}