<?php

namespace SkinHistory\Skin;

/**
 * Represents data about a skin file used by a Minecraft player at some point
 * 
 * @package Skin
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class Texture {
	/**
	 * @access private
	 * @var int $id The texture ID.
	 */
	private $id;

	/**
	 * @access private
	 * @var string $uuid The UUID of the player that owns this texture.
	 */
	private $uuid;

	/**
	 * @access private
	 * @var SkinFile The skin file of this texture.
	 */
	private $skin;

	/**
	 * @access private
	 * @var string $time The timestamp of when this texture was created.
	 */
	private $time;

	/**
	 * @access private
	 * @var bool Whether this account exists, used for object construction.
	 */
	private $exists;

	/**
	 * Gets a texture from it's ID.
	 * 
	 * @access public
	 * @param int $id The texture ID.
	 * @return Texture|null The texture object, null if the texture could not be found.
	 */
	public static function getTextureFromID($id){
		$n = "textures_" . $id;

		if(\CacheHandler::existsInCache($n)){
			return \CacheHandler::getFromCache($n);
		} else {
			$texture = new Texture($id);
			$texture->load();

			return $texture;
		}
	}

	/**
	 * Creates a texture from the passed data.
	 * 
	 * @access public
	 * @param int $id
	 * @param string $uuid
	 * @param array $skinFileData
	 * @param string $time
	 * @return Texture The texture object.
	 */
	public static function getTextureFromData($id,$uuid,$skinFileData,$time){
		$texture = new Texture($id);

		$texture->uuid = $uuid;
		$texture->time = $time;

		if(is_array($skinFileData)){
			$texture->skin = SkinFile::getSkinFileFromData($skinFileData["id"],$skinFileData["title"],$skinFileData["md5"],$skinFileData["full_url"],$skinFileData["firstUser"],$skinFileData["type"],$skinFileData["model"],$skinFileData["isBanned"],$skinFileData["randomizer"],$skinFileData["textureData"],$skinFileData["time"]);
		} else {
			$texture->skin = SkinFile::getSkinFileFromID($skinFileData);
		}

		$texture->saveToCache();

		return $texture;
	}

	/**
	 * Creates a texture from the passed data and saves it to the database.
	 * 
	 * @access public
	 * @param array $data The texture data
	 * @return int|null The new texture ID, null if the texture could not be created.
	 */
	public static function createTexture($data){
		if(is_array($data)){
			$id = null;

			$mysqli = \Database::Instance()->get();

			$uuid = $data["uuid"];
			$skinFile = $data["skinFile"];

			$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_textures` (`uuid`,`skinFile`) VALUES(?,?)");
			$stmt->bind_param("si",$uuid,$skinFile);
			$stmt->execute();
			$id = $stmt->insert_id;
			$stmt->close();

			$skin = SkinFile::getSkinFileFromID($data["skinFile"]);
			if($skin != null){
				$skin->updateUserCount();
			}

			return $id;
		}

		return null;
	}

	/**
	 * Constructor
	 * 
	 * @access protected
	 * @param int $id The texture ID.
	 */
	protected function __construct($id){
		$this->id = $id;
	}

	/**
	 * @access private
	 */
	private function load(){
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("SELECT t.id,t.uuid,t.time,s.id,s.title,s.md5,s.full_url,s.firstUser,s.type,s.model,s.isBanned,s.randomizer,s.textureData,s.time FROM `skinhistory_textures` AS t INNER JOIN `skinhistory_skinfiles` AS s ON t.skinFile = s.id WHERE t.id = ?");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$stmt->bind_result($id,$uuid,$time,$skin_id,$skin_title,$skin_md5,$skin_fullURL,$skin_firstUser,$skin_type,$skin_model,$skin_isBanned,$skin_randomizer,$skin_textureData,$skin_time);

		if($stmt->fetch()){
			$this->id = $id;
			$this->uuid = $uuid;
			$this->time = $time;

			$this->skin = SkinFile::getSkinFileFromData($skin_id,$skin_title,$skin_md5,$skin_fullURL,$skin_firstUser,$skin_type,$skin_model,$skin_isBanned,$skin_randomizer,$skin_textureData,$skin_time);

			$this->exists = true;
			$this->saveToCache();
		}

		$stmt->close();
	}

	/**
	 * @access public
	 * @return string The texture ID.
	 */
	public function getID(){
		return $this->id;
	}

	/**
	 * @access public
	 * @return string The UUID of the player that owns this texture.
	 */
	public function getUUID(){
		return $this->uuid;
	}

	/**
	 * @access public
	 * @return SkinFile The skin file of this texture.
	 */
	public function getSkin(){
		return $this->skin;
	}

	/**
	 * @access public
	 * @return string The timestamp of when this texture was created.
	 */
	public function getTime(){
		return $this->time;
	}

	/**
	 * @access public
	 * @return bool True if the account is valid and exists in the database.
	 */
	public function exists(){
		return $this->exists;
	}

	/**
	 * Saves this account object to the cache for future retrieval for 60 seconds.
	 * 
	 * @access public
	 */
	public function saveToCache(){
		\CacheHandler::setToCache("textures_" . $this->id,$this,2*60*60);
	}
}