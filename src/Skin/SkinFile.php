<?php

namespace SkinHistory\Skin;

use SkinHistory\Player\Player;


/**
 * Represents either a skin or cape documented on MCSkinHistory
 * 
 * @package Skin
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class SkinFile {
	/**
	 * Gets a skin file from it's ID.
	 * 
	 * @access public
	 * @param int $id The skin file ID.
	 * @return SkinFile|null The skin file object, null if the skin file could not be found.
	 */
	public static function getSkinFileFromID($id){
		$n = "skinFile_" . $id;

		if(\CacheHandler::existsInCache($n)){
			return \CacheHandler::getFromCache($n);
		} else {
			$skin = new SkinFile($id);
			$skin->load();
			if($skin->exists == true){
				return $skin;
			} else {
				return null;
			}
		}
	}

	/**
	 * Creates a skin file from the passed data.
	 * 
	 * @access public
	 * @param int $id
	 * @param string $title
	 * @param string $md5
	 * @param string $fullURL
	 * @param string $firstUser
	 * @param string $type
	 * @param string $model
	 * @param bool $isBanned
	 * @param int $randomizer
	 * @param array $textureData
	 * @param string $time
	 * @param int $users
	 * @return SkinFile The skin file object.
	 */
	public static function getSkinFileFromData($id,$title,$md5,$fullURL,$firstUser,$type,$model,$isBanned,$randomizer,$textureData,$time,$users = -1){
		$skin = new SkinFile($id);

		$skin->id = $id;
		$skin->title = $title;
		$skin->md5 = $md5;
		$skin->fullURL = $fullURL;
		$skin->firstUser = $firstUser;
		$skin->type = $type;
		$skin->model = $model;
		$skin->isBanned = $isBanned;
		$skin->randomizer = $randomizer;
		$skin->textureData = $textureData == null ? null : json_decode($textureData,true);
		$skin->time = $time;
		$skin->users = $users;
		$skin->exists = true;

		$skin->saveToCache();

		return $skin;
	}

	/**
	 * Creates a skin file from the passed data and saves it to the database.
	 * 
	 * @access public
	 * @param array $data The skin file data
	 * @return int|null The new skin file ID, null if the skin file could not be created.
	 */
	public static function createSkinFile($data){
		if(is_array($data)){
			$id = null;

			$mysqli = \Database::Instance()->get();
			$textureData = $data["textureData"] == null ? null : json_encode($data["textureData"]);

			$md5 = $data["md5"];
			$url = $data["full_url"];
			$firstUser = $data["firstUser"];
			$type = $data["type"];
			$model = $data["model"];
			$rand = rand(1,getrandmax());

			$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_skinfiles` (`md5`,`full_url`,`firstUser`,`type`,`model`,`textureData`,`randomizer`,`users`) VALUES(?,?,?,?,?,?,?,0)");
			$stmt->bind_param("ssssssi",$md5,$url,$firstUser,$type,$model,$textureData,$rand);
			$stmt->execute();
			$id = $stmt->insert_id;
			$stmt->close();

			if($id != null || $id <= 0){
				$stmt = $mysqli->prepare("SELECT `id` FROM `skinhistory_skinfiles` WHERE `md5` = ?");
				$stmt->bind_param("s",$md5);
				$stmt->execute();
				$result = $stmt->get_result();
				$id = $result->fetch_assoc()["id"];
				$stmt->fetch();
				$stmt->close();
			}

			return $id;
		}

		return null;
	}

	/**
	 * @access public
	 * @param int $id The skin file id.
	 * @return bool If true, a skin file with the passed id is cached.
	 */
	public static function isCached($id){
		return \CacheHandler::existsInCache("skinFile_" . $id);
	}

	/**
	 * @access private
	 * @var int $id The ID of this skin file.
	 */
	private $id;

	/**
	 * @access private
	 * @var string|null $title The custom title of this skin file, null if there is none.
	 */
	private $title;

	/**
	 * @access private
	 * @var string $md5 The md5 hash of this skin file
	 */
	private $md5;

	/**
	 * @access private
	 * @var string $fullURL The full URL of this skin file.
	 */
	private $fullURL;

	/**
	 * @access private
	 * @var string $firstUser The UUID of the first user of this skin file.
	 */
	private $firstUser;

	/**
	 * @access private
	 * @var string $type The type of this skin file (SKIN,CAPE_MOJANG,CAPE_OPTIFINE,CAPE_LABYMOD).
	 */
	private $type;

	/**
	 * @access private
	 * @var string|null $model The model of this skin file, either ALEX, STEVE or null if this skin file is not a skin.
	 */
	private $model;

	/**
	 * @access private
	 * @var bool $isBanned If true, this skin file is banned and will not show on skin file lists or have ads on it's page.
	 */
	private $isBanned;

	/**
	 * @access private
	 * @var bool $randomizer The randomizer number used to create the browse list.
	 */
	private $randomizer;

	/**
	 * @access private
	 * @var array $textureData The texture data passed from mojang, if available.
	 */
	private $textureData;

	/**
	 * @access private
	 * @var string $time The timestamp of when this skin file was registered to the database.
	 */
	private $time;

	/**
	 * @access private
	 * @var int $users The user count of this skin file, -1 if the user count has not been loaded yet.
	 */
	private $users;

	/**
	 * @access private
	 * @var bool $exists Whether this skin file exists, used for object construction.
	 */
	private $exists = false;

	/**
	 * Constructor
	 * 
	 * @access protected
	 * @param int $id The skin file ID.
	 */
	protected function __construct($id){
		$this->id = $id;
	}

	/**
	 * @access private
	 */
	private function load(){
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("SELECT `id`,`title`,`md5`,`full_url`,`firstUser`,`type`,`model`,`isBanned`,`randomizer`,`textureData`,`time`,`users` FROM `skinhistory_skinfiles` WHERE `id` = ?");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$stmt->bind_result($id,$title,$md5,$fullURL,$firstUser,$type,$model,$isBanned,$randomizer,$textureData,$time,$users);

		if($stmt->fetch()){
			$this->id = $id;
			$this->title = $title;
			$this->md5 = $md5;
			$this->fullURL = $fullURL;
			$this->firstUser = $firstUser;
			$this->type = $type;
			$this->model = $model;
			$this->isBanned = $isBanned;
			$this->randomizer = $randomizer;
			$this->textureData = $textureData == null ? null : json_decode($textureData,true);
			$this->time = $time;
			$this->users = $users;

			if($this->users == -1){
				$this->updateUserCount(false);
			}

			$this->exists = true;

			$this->saveToCache();
		}

		$stmt->close();
	}

	/**
	 * @access public
	 * @return int The ID of this skin file.
	 */
	public function getID(){
		return $this->id;
	}

	/**
	 * @access public
	 * @return string The custom title of this skin file, null if there is none.
	 */
	public function getTitle(){
		return $this->title;
	}

	/**
	 * @access public
	 * @return string The md5 hash of this skin file
	 */
	public function getMD5(){
		return $this->md5;
	}

	/**
	 * @access public
	 * @return string The full URL of this skin file.
	 */
	public function getFullURL(){
		return $this->fullURL;
	}

	/**
	 * @access public
	 * @return Player|null The player object of the first user, if available.
	 */
	public function getFirstUser(){
		return $this->firstUser != null ? Player::getPlayer($this->firstUser) : null;
	}

	/**
	 * @access public
	 * @return string The type of this skin file (SKIN,CAPE_MOJANG,CAPE_OPTIFINE,CAPE_LABYMOD).
	 */
	public function getType(){
		return $this->type;
	}

	/**
	 * @access public
	 * @return string The model of this skin file, either ALEX, STEVE or null if this skin file is not a skin.
	 */
	public function getModel(){
		return $this->model;
	}

	/**
	 * @access public
	 * @return bool If true, this skin file is banned and will not show on skin file lists or have ads on it's page.
	 */
	public function isBanned(){
		return $this->isBanned;
	}

	/**
	 * @access public
	 * @return int The randomizer number used to create the browse list.
	 */
	public function getRandomizer(){
		return $this->randomizer;
	}

	/**
	 * @access public
	 * @return array The texture data passed from mojang, if available.
	 */
	public function getTextureData(){
		return $this->textureData;
	}

	/**
	 * @access public
	 * @return string The timestamp of when this skin file was registered to the database.
	 */
	public function getTime(){
		return $this->time;
	}

	/**
	 * @access public
	 * @return int The user count of this skin file.
	 */
	public function getUsers(){
		if($this->users == -1) $this->updateUserCount();

		return $this->users;
	}

	/**
	 * Evaluates the user count of this skin file and updates it in the database.
	 * 
	 * @access public
	 * @param bool $cache If true, the cache of this skin file will be updated.
	 */
	public function updateUserCount($cache = true){
		$users = 0;
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("SELECT COUNT(id) AS count FROM skinhistory_textures WHERE `skinFile` = ?");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows){
			$users = $result->fetch_assoc()["count"];
		}

		$stmt->close();

		$this->users = $users;

		$stmt = $mysqli->prepare("UPDATE `skinhistory_skinfiles` SET `users` = ? WHERE `id` = ?");
		$stmt->bind_param("ii",$this->users,$this->id);
		$stmt->execute();
		$stmt->close();

		if($cache == true) $this->saveToCache();
	}

	/**
	 * @access public
	 * @return bool True if the skin file is valid and exists in the database.
	 */
	public function exists(){
		return $this->exists;
	}

	/**
	 * Saves this skin file object to the cache for future retrieval for 5 minutes.
	 * 
	 * @access public
	 */
	public function saveToCache(){
		\CacheHandler::setToCache("skinFile_" . $this->id,$this,5*60);
	}
}