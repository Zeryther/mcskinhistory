<?php

namespace SkinHistory\Skin;

use SkinHistory\Server\Server;
use SkinHistory\Util\Util;

/**
 * Represents a list of skin files rendering by certain criteria
 * 
 * @package Skin
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class SkinFileList {
	/**
	 * @access private
	 * @var bool $pagination If true, pagination will render.
	 */
	private $pagination;

	/**
	 * @access private
	 * @var string The type of skin files to list, use "SERVER" to create a server list.
	 */
	private $type;

	/**
	 * @access private
	 * @var string The type of list to use (use SKIN_FILE_LIST_TYPE_ constants), only works for skin file lists.
	 */
	private $listType;

	/**
	 * @access private
	 * @var string The url pattern to use for the pagination (use "(:num)" as a number placeholder).
	 */
	private $urlPattern;

	/**
	 * @access private
	 * @var int The current page, used for pagination.
	 */
	private $currentPage;

	/**
	 * @access private
	 * @var SkinFile[]|Server[]|null The results fetched from loading, null if the results have not been fetched yet.
	 */
	private $skinFiles;

	/**
	 * Constructor
	 * 
	 * @access public
	 */
	public function __construct(){
		$this->pagination = true;
		$this->type = "";
		$this->listType = SKIN_FILE_LIST_TYPE_BROWSE;
		$this->urlPattern = "";

		$this->currentPage = 1;
	}

	/**
	 * @access public
	 * @return bool If true, pagination will render.
	 */
	public function hasPagination(){
		return $this->pagination;
	}

	/**
	 * @access public
	 * @param $pagination If true, pagination will render.
	 */
	public function setPagination($pagination){
		$this->pagination = $pagination;
	}

	/**
	 * @access public
	 * @return string The type of skin files to list, use "SERVER" to create a server list.
	 */
	public function getType(){
		return $this->type;
	}

	/**
	 * @access public
	 * @param $type The type of skin files to list, use "SERVER" to create a server list.
	 */
	public function setType($type){
		$this->type = $type;
	}

	/**
	 * @access public
	 * @return string The type of list to use (use SKIN_FILE_LIST_TYPE_ constants), only works for skin file lists.
	 */
	public function getListType(){
		return $this->listType;
	}

	/**
	 * @access public
	 * @param string $listType The type of list to use (use SKIN_FILE_LIST_TYPE_ constants), only works for skin file lists.
	 */
	public function setListType($listType){
		$this->listType = $listType;
	}

	/**
	 * @access public
	 * @return int The current page, used for pagination.
	 */
	public function getCurrentPage(){
		return $this->currentPage;
	}

	/**
	 * @access public
	 * @param int $currentPage The current page, used for pagination.
	 */
	public function setCurrentPage($currentPage){
		$this->currentPage = $currentPage;
	}

	/**
	 * @access public
	 * @return string The url pattern to use for the pagination (use "(:num)" as a number placeholder).
	 */
	public function getURLPattern(){
		return $this->urlPattern;
	}

	/**
	 * @access public
	 * @param string $urlPattern The url pattern to use for the pagination (use "(:num)" as a number placeholder).
	 */
	public function setURLPattern($urlPattern){
		$this->urlPattern = $urlPattern;
	}

	/**
	 * Renders the skin file list.
	 * 
	 * @access public
	 */
	public function render(){
		global $app;
		$this->load();

		$l = $this->type == "SKIN" ? "skin" : "cape";
		$s = Util::isDevMode() ? "?ip=" : "";

		$this->renderPagination();

		echo '<center>';
		Util::renderAd(AD_TYPE_LEADERBOARD);
		echo '</center>';

		if($this->type == "SERVER"){
			foreach($this->skinFiles as $server){
				?>
			<a href="<?= $app->routeUrl("/server/" . $s . $server->getIP()) ?>">
				<div class="bg-dark text-white px-3 py-3 rounded mb-2" style="min-height: 96px">
					<img src="<?= !is_null($server->getIcon()) ? $server->getIcon() : $app->baseUrl("assets:img/defaultServerIcon.png") ?>" class="float-left"/>

					<div class="float-left ml-2" style="max-width: 65%; max-height: 60px; overflow: hidden">
						<div class="font-weight-bold"><?= (!is_null($server->getName()) ? $server->getName() : $server->getIP()) ?></div>

						<?= $server->getConvertedMotd() ?>
					</div>

					<div class="float-right">
						<?= $server->getPlayersOnline() ?> / <?= $server->getPlayersMax() ?> | #<?= $i ?>
					</div>
				</div>
			</a>
				<?php
			}
		} else {
			echo '<div class="row">';

			foreach($this->skinFiles as $skin){
				$title = $skin->getTitle() != null ? $skin->getTitle() : "#" . $skin->getID();
				$img = $skin->getType() == "SKIN" ? "/avatar-3d-custom.php?skinFileID=" . $skin->getID() : "/avatar/cape/" . $skin->getID();

				echo '<div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12" style="padding: 0.25rem !important;">';
				echo '<div class="card text-center">';
				echo '<h5 class="card-header">' . $title;
				Util::renderFavoriteButton($skin->getID(),"right");
				echo '</h5>';
				echo '<a style="text-decoration:none" href="' . $app->routeUrl("/" . $l . "/" . $skin->getID()) . '">';
				echo '<div style="position:relative;padding:5px 0px 5px 0px">';
				echo '<img class="skinShadow" height="204" src="' . $app->routeUrl($img) . '"/>';
				echo '<div class="text-muted" style="position:absolute;bottom:0;left:0.5rem;margin-top: 10px">';
				echo '<i class="fas fa-users"></i> ' . $skin->getUsers();
				echo '</div>';
				echo '</div>';
				echo '</a>';
				echo '</div>';
				echo '</div>';
			}

			echo '</div>';
		}

		echo '<center>';
		Util::renderAd(AD_TYPE_LEADERBOARD);
		echo '</center>';

		$this->renderPagination();
	}

	/**
	 * @access private
	 */
	private function load(){
		$mayCache = $this->pagination == false && !empty($this->urlPattern);
		$cacheName = "skinFileList_" . urlencode($this->urlPattern);

		if($this->skinFiles == null){
			if($mayCache && \CacheHandler::existsInCache($cacheName)){
				$this->skinFiles = \CacheHandler::getFromCache($cacheName);
			} else {
				$this->skinFiles = array();

				$mysqli = \Database::Instance()->get();
				$currentPage = $this->currentPage;
				$itemsPerPage = SKIN_FILE_LIST_ITEMS_PER_PAGE;
				$total = $this->totalItems();

				if($this->type == "SERVER"){
					$stmt = $mysqli->prepare("SELECT s.ip,s.name,s.icon,s.motd,s.`players.online`,s.`players.max`,s.`players.peak`,s.version,s.uptime,s.website,s.twitter,s.parent,s.blacklisted,s.time FROM skinhistory_servers AS s WHERE `parent` IS NULL AND `blacklisted` = 0 ORDER BY `players.online` DESC LIMIT " . (($currentPage-1)*$itemsPerPage) . " , " . $itemsPerPage);
				} else if($this->getListType() == SKIN_FILE_LIST_TYPE_BROWSE){
					$stmt = $mysqli->prepare("SELECT s.id,s.title,s.md5,s.full_url,s.firstUser,s.type,s.model,s.isBanned,s.randomizer,s.textureData,s.time,s.users FROM skinhistory_skinfiles AS s WHERE s.type IN (?) AND s.isBanned = 0 GROUP BY s.id ORDER BY s.randomizer DESC LIMIT " . (($currentPage-1)*$itemsPerPage) . " , " . $itemsPerPage);
				} else if($this->getListType() == SKIN_FILE_LIST_TYPE_POPULAR){
					$stmt = $mysqli->prepare("SELECT s.id,s.title,s.md5,s.full_url,s.firstUser,s.type,s.model,s.isBanned,s.randomizer,s.textureData,s.time,s.users FROM skinhistory_skinfiles AS s WHERE s.type IN (?) AND s.isBanned = 0 GROUP BY s.id ORDER BY s.users DESC LIMIT " . (($currentPage-1)*$itemsPerPage) . " , " . $itemsPerPage);
				} else if($this->getListType() == SKIN_FILE_LIST_TYPE_NEW){
					$stmt = $mysqli->prepare("SELECT s.id,s.title,s.md5,s.full_url,s.firstUser,s.type,s.model,s.isBanned,s.randomizer,s.textureData,s.time,s.users FROM skinhistory_skinfiles AS s WHERE s.type IN (?) AND s.isBanned = 0 GROUP BY s.id ORDER BY s.time DESC LIMIT " . (($currentPage-1)*$itemsPerPage) . " , " . $itemsPerPage);
				}

				if(isset($stmt)){
					if($this->type != "SERVER") $stmt->bind_param("s",$this->type);
					
					if($stmt->execute()){
						$result = $stmt->get_result();

						if($result->num_rows > 0){
							while($row = $result->fetch_assoc()){
								if($this->type == "SERVER"){
									array_push($this->skinFiles,Server::getServerFromData(
										$row["ip"],
										$row["name"],
										$row["icon"],
										$row["motd"],
										$row["players.online"],
										$row["players.max"],
										$row["players.peak"],
										$row["version"],
										$row["uptime"],
										$row["website"],
										$row["twitter"],
										$row["parent"],
										$row["blacklisted"],
										$row["time"]
									));
								} else {
									array_push($this->skinFiles,SkinFile::getSkinFileFromData(
										$row["id"],
										$row["title"],
										$row["md5"],
										$row["full_url"],
										$row["firstUser"],
										$row["type"],
										$row["model"],
										$row["isBanned"],
										$row["randomizer"],
										$row["textureData"],
										$row["time"],
										$row["users"]
									));
								}
							}
						}
					}

					$stmt->close();

					if($mayCache){
						\CacheHandler::setToCache($cacheName,$this->skinFiles,3*60);
					}
				}
			}
		}
	}

	/**
	 * @access private
	 */
	private function renderPagination(){
		if($this->pagination == true){
			$currentPage = $this->currentPage;
			$itemsPerPage = SKIN_FILE_LIST_ITEMS_PER_PAGE;
			$total = $this->totalItems();

			$paginator = new \JasonGrimes\Paginator($total,$itemsPerPage,$currentPage,$this->urlPattern);

			if ($paginator->getNumPages() > 1){ ?>
				<nav aria-label="Page navigation example"><ul class="pagination justify-content-center">
					<?php if ($paginator->getPrevUrl()){ ?>
						<li class="page-item"><a class="page-link" href="<?= $paginator->getPrevUrl(); ?>">&laquo; <?= tr("paginator.prev") ?></a></li>
						<?php } ?>
			
					<?php foreach ($paginator->getPages() as $page){ ?>
						<?php if ($page['url']){ ?>
							<li class="page-item<?= $page['isCurrent'] ? ' active' : ''; ?>">
								<a class="page-link" href="<?= $page['url']; ?>"><?= $page['num']; ?></a>
							</li>
						<?php } else { ?>
							<li class="page-item disabled"><a class="page-link" href="#"><?= $page['num']; ?></a></li>
						<?php } ?>
					<?php } ?>
			
					<?php if ($paginator->getNextUrl()){ ?>
						<li class="page-item"><a class="page-link" href="<?= $paginator->getNextUrl(); ?>"><?= tr("paginator.next") ?> &raquo;</a></li>
						<?php } ?>
				</ul></nav>
						<?php }
		}
	}

	private function totalItems(){
		$count = 0;

		$mysqli = \Database::Instance()->get();
		$cacheName = "totalItems_" . $this->type;

		if(\CacheHandler::existsInCache($cacheName)){
			$count = \CacheHandler::getFromCache($cacheName);
		} else {
			if($this->type == "SERVER"){
				$stmt = $mysqli->prepare("SELECT COUNT(ip) AS count FROM skinhistory_servers WHERE `parent` IS NULL AND `blacklisted` = 0");
			} else {
				$stmt = $mysqli->prepare("SELECT COUNT(id) AS count FROM skinhistory_skinfiles WHERE `type` = ?");
				$stmt->bind_param("s",$this->type);
			}

			$stmt->execute();
			$result = $stmt->get_result();

			if($result->num_rows){
				$count = $result->fetch_assoc()["count"];
			}

			$stmt->close();

			\CacheHandler::setToCache($cacheName,$count,10*60);
		}
		
		return $count;
	}
}