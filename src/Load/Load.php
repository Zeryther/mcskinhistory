<?php

# Loads all library files

if(isset($_SERVER["HTTP_HOST"]) && (explode(":",$_SERVER["HTTP_HOST"])[0] == "localhost" || explode(":",$_SERVER["HTTP_HOST"])[0] == "127.0.0.1")){
	require_once __DIR__ . "/../../../mcskinhistory-config.php";
} else {
	require_once __DIR__ . "/../../config.php";
}

require_once __DIR__ . "/../../vendor/autoload.php";
require_once __DIR__ . "/../Util/Util.php";

require_once __DIR__ . "/../Account/Account.php";
require_once __DIR__ . "/../Account/Favorite.php";
require_once __DIR__ . "/../Database/Database.php";
require_once __DIR__ . "/../Handler/Shutdown.php";
require_once __DIR__ . "/../Lime/App.php";
require_once __DIR__ . "/../Player/Name/MinecraftName.php";
require_once __DIR__ . "/../Player/Name/MinecraftNameHolder.php";
require_once __DIR__ . "/../Player/MCAuth.php";
require_once __DIR__ . "/../Player/MinecraftProfile.php";
require_once __DIR__ . "/../Player/Player.php";
require_once __DIR__ . "/../Player/ProfileUtils.php";
require_once __DIR__ . "/../Skin/SkinFile.php";
require_once __DIR__ . "/../Skin/SkinFileList.php";
require_once __DIR__ . "/../Skin/Texture.php";
require_once __DIR__ . "/../Cache/CacheHandler.php";
require_once __DIR__ . "/../Util/DisqusThread.php";
require_once __DIR__ . "/../Server/Server.php";
require_once __DIR__ . "/../Server/ServerPing.php";

use \SkinHistory\Util\Util;

if(Util::isDevMode()){
	error_reporting(E_ALL);
	ini_set("display_errors",1);
	ini_set("display_startup_errors",1);
}

Util::cleanupTempFolder();

require_once __DIR__ . "/../i18n/Locales/autoload.php";

use \Gigadrive\i18n\i18n;

i18n::Instance("website",!(\SkinHistory\Util\Util::isDevMode()));

/**
 * Alias for i18n::getTranslatedMessage()
 * 
 * @param string $phrase
 * @param array $variables
 * @return string
 */
function tr($phrase,$variables = null){
	return i18n::getTranslatedMessage($phrase,$variables);
}