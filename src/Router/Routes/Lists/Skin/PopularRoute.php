<?php

$app->bind("/skins/popular", function(){
	return $this->render("views:Lists/Skin/Popular.php with views:Layout.php",[
		"title" => "Popular Skins",
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_SKINS
	]);
});