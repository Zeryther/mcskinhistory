<?php

$app->bind("/skins/browse", function(){
	return $this->reroute("/skins/browse/1");
});

$app->bind("/skins/browse/:page", function($params){
	$page = $params["page"];

	if(!empty($page)){
		return $this->render("views:Lists/Skin/Browse.php with views:Layout.php",[
			"title" => "Browse Skins (Page " . $page . ")",
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_SKINS,
			"page" => $page
		]);
	} else {
		return $this->reroute("/skins/browse/1");
	}
});