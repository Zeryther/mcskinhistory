<?php

$app->bind("/skins/new", function(){
	return $this->render("views:Lists/Skin/New.php with views:Layout.php",[
		"title" => "Newest Skins",
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_SKINS
	]);
});