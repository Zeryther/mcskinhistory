<?php

$app->bind("/capes/optifine/new", function(){
	return $this->render("views:Lists/Cape/OptiFine/New.php with views:Layout.php",[
		"title" => "Newest OptiFine Capes",
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_CAPES
	]);
});