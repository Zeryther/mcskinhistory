<?php

$app->bind("/capes/optifine/browse", function(){
	return $this->reroute("/capes/optifine/browse/1");
});

$app->bind("/capes/optifine/browse/:page", function($params){
	$page = $params["page"];

	if(!empty($page)){
		return $this->render("views:Lists/Cape/OptiFine/Browse.php with views:Layout.php",[
			"title" => "Browse OptiFine Capes (Page " . $page . ")",
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_CAPES,
			"page" => $page
		]);
	} else {
		return $this->reroute("/cape/optifine/browse/1");
	}
});