<?php

$app->bind("/capes/optifine/popular", function(){
	return $this->render("views:Lists/Cape/OptiFine/Popular.php with views:Layout.php",[
		"title" => "Popular OptiFine Capes",
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_CAPES
	]);
});