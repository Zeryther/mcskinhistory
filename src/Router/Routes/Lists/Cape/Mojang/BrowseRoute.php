<?php

$app->bind("/capes/mojang/browse", function(){
	return $this->render("views:Lists/Cape/Mojang/Browse.php with views:Layout.php",[
		"title" => "Browse Mojang Capes",
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_CAPES
	]);
});