<?php

$app->bind("/capes/labymod/new", function(){
	return $this->render("views:Lists/Cape/LabyMod/New.php with views:Layout.php",[
		"title" => "Newest LabyMod Capes",
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_CAPES
	]);
});