<?php

$app->bind("/capes/labymod/popular", function(){
	return $this->render("views:Lists/Cape/LabyMod/Popular.php with views:Layout.php",[
		"title" => "Popular LabyMod Capes",
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_CAPES
	]);
});