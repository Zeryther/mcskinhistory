<?php

$app->bind("/capes/labymod/browse", function(){
	return $this->reroute("/capes/labymod/browse/1");
});

$app->bind("/capes/labymod/browse/:page", function($params){
	$page = $params["page"];

	if(!empty($page)){
		return $this->render("views:Lists/Cape/LabyMod/Browse.php with views:Layout.php",[
			"title" => "Browse LabyMod Capes (Page " . $page . ")",
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_CAPES,
			"page" => $page
		]);
	} else {
		return $this->reroute("/cape/labymod/browse/1");
	}
});