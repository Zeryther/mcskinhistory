<?php

$app->bind("/", function(){
	return $this->render("views:Home.php with views:Layout.php",[
		"title" => tr("home.seoTitle"),
		"description" => DEFAULT_DESCRIPTION,
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_HOME
	]);
});