<?php

use SkinHistory\Skin\SkinFile;
use SkinHistory\Util\Util;

$app->get("/scripts/toggleFavorite/:skinFileID",function($params){
	$this->response->mime = "json";

	$skinFileID = $params["skinFileID"];

	if(!empty($skinFileID)){
		if(Util::isLoggedIn()){
			$account = Util::getCurrentAccount();

			if($account != null){
				$skinFile = SkinFile::getSkinFileFromID($skinFileID);

				if($skinFile != null){
					if($account->hasFavorite($skinFile->getID())){
						$account->removeFavorite($skinFile->getID());
						return json_encode(["success" => "Favorite removed"]);
					} else {
						$account->addFavorite($skinFile->getID());
						return json_encode(["success" => "Favorite added"]);
					}
				} else {
					return json_encode(["error" => "Invalid skin file ID"]);	
				}
			} else {
				return json_encode(["error" => "Invalid account"]);
			}
		} else {
			return json_encode(["error" => "Invalid account"]);
		}
	} else {
		return json_encode(["error" => "Invalid skin file ID"]);
	}
});
