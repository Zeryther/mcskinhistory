<?php

use SkinHistory\Skin\SkinFile;
use SkinHistory\Util\Util;

$app->bind("/skin/:id", function($params){
	$id = $params["id"];
	
	if(!empty($id)){
		$skin = SkinFile::getSkinFileFromID($id);

		if($skin != null && $skin->getType() == "SKIN"){
			$d = isset($_SERVER["HTTPS"]) ? "s" : "";
			$head = Util::proxyImage("http" . $d . "://" . $_SERVER["HTTP_HOST"] . "/avatar/skin/face/" . $skin->getID());

			return $this->render("views:Skin.php with views:Layout.php",[
				"title" => tr("cape.headline",[$skin->getTitle() != null ? $skin->getTitle() : "#" . $skin->getID()]),
				"description" => DEFAULT_DESCRIPTION,
				"twitterImage" => $head,
				"skin" => $skin
			]);
		}
	} else {
		return $this->reroute("/");
	}
});