<?php

use \SkinHistory\Server\Server;
use \SkinHistory\Util\Util;

$app->bind("/server/:ip",function($params){
    $ip = !empty($params["ip"]) ? $params["ip"] : (isset($_GET["ip"]) ? $_GET["ip"] : "");
    $s = Util::isDevMode() ? "?ip=" : "";

    if(!empty($ip)){
        $server = Server::getServer($ip);

        if(!is_null($server)){
            // force redirect to parent
            $parent = $server->getParent();
            if(!is_null($parent)){
                return $this->reroute("/server/" . $s . $parent->getIP());
            }

            if($ip !== $server->getIP()){
                return $this->reroute("/server/" . $s . $server->getIP());
            } else {
                $title = !is_null($server->getName()) ? $server->getName() : $server->getIP();

                return $this->render("views:Server.php with views:Layout.php",[
                    "title" => $title,
					"description" => "Lookup information about the Minecraft server " . $title,
					"twitterImage" => DEFAULT_TWITTER_IMAGE,
                    "server" => $server,
                    "searchPlaceholder" => $server->getIP(),
                    "nav" => NAV_SERVERS
                ]);
            }
        }
    }
});

$app->bind("/servers", function(){
	return $this->reroute("/servers/1");
});

$app->bind("/servers/:page", function($params){
	$page = $params["page"];

	if(!empty($page)){
		return $this->render("views:ServerList.php with views:Layout.php",[
            "title" => tr("serverList.headline",[$page]),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_SERVERS,
			"page" => $page
        ]);
	} else {
		return $this->reroute("/skins/browse/1");
	}
});