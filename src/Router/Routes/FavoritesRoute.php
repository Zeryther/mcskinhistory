<?php

use SkinHistory\Util\Util;

$app->bind("/account/favorites",function(){
	return $this->reroute("/account/favorites/skins");
});

$app->bind("/account/favorites/skins",function(){
	return $this->reroute("/account/favorites/skins/1");
});

$app->bind("/account/favorites/skins/:page",function($params){
	if(Util::isLoggedIn()){
		return $this->render("views:Favorites.php with views:Layout.php",[
			"title" => tr("favorites.skins") . " - " . tr("favorites.headline"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_ACCOUNT,
			"includeAccountNav" => TRUE,
			"accountNav" => ACCOUNT_NAV_FAVORITES,
			"favoriteType" => "SKIN",
			"page" => $params["page"]
		]);
	} else {
		return $this->reroute("/login");
	}
});

$app->bind("/account/favorites/mojangCapes",function(){
	return $this->reroute("/account/favorites/mojangCapes/1");
});

$app->bind("/account/favorites/mojangCapes/:page",function($params){
	if(Util::isLoggedIn()){
		return $this->render("views:Favorites.php with views:Layout.php",[
			"title" => tr("favorites.mojang") . " - " . tr("favorites.headline"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_ACCOUNT,
			"includeAccountNav" => TRUE,
			"accountNav" => ACCOUNT_NAV_FAVORITES,
			"favoriteType" => "CAPE_MOJANG",
			"page" => $params["page"]
		]);
	} else {
		return $this->reroute("/login");
	}
});

$app->bind("/account/favorites/labymod",function(){
	return $this->reroute("/account/favorites/labymod/1");
});

$app->bind("/account/favorites/labymod/:page",function($params){
	if(Util::isLoggedIn()){
		return $this->render("views:Favorites.php with views:Layout.php",[
			"title" => tr("favorites.labymod") . " - " . tr("favorites.headline"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_ACCOUNT,
			"includeAccountNav" => TRUE,
			"accountNav" => ACCOUNT_NAV_FAVORITES,
			"favoriteType" => "CAPE_LABYMOD",
			"page" => $params["page"]
		]);
	} else {
		return $this->reroute("/login");
	}
});

$app->bind("/account/favorites/optifine",function(){
	return $this->reroute("/account/favorites/optifine/1");
});

$app->bind("/account/favorites/optifine/:page",function($params){
	if(Util::isLoggedIn()){
		return $this->render("views:Favorites.php with views:Layout.php",[
			"title" => tr("favorites.optifine") . " - " . tr("favorites.headline"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_ACCOUNT,
			"includeAccountNav" => TRUE,
			"accountNav" => ACCOUNT_NAV_FAVORITES,
			"favoriteType" => "CAPE_OPTIFINE",
			"page" => $params["page"]
		]);
	} else {
		return $this->reroute("/login");
	}
});