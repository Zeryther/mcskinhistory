<?php

use SkinHistory\Player\Player;

mysqli_report(MYSQLI_REPORT_ERROR);
// SCRIPTS USED FOR MIGRATING OLD DATABSE TO NEW ONE

define("LIMIT",15);

$app->get("/convertScript/players",function(){
	$mysqli = \Database::Instance()->get();
	$limit = LIMIT;
	$data = array();

	$stmt = $mysqli->prepare("SELECT * FROM `mcskinhistory_accounts` ORDER BY `foundAt` ASC LIMIT " . $limit*5);
	if($stmt){
		$stmt->execute();
		$result = $stmt->get_result();
		if($result->num_rows){
			while($row = $result->fetch_assoc()){
				/*if(Player::getPlayer($row["uuid"]) != null)*/ array_push($data,$row);

				if(count($data) >= $limit) break;
			}
		}
		$stmt->close();
	}

	foreach($data as $legacyData){
		$socialMedia = array();

		if($legacyData["social_beam"] != null) $socialMedia["beam"] = $legacyData["social_beam"];
		if($legacyData["social_facebook"] != null) $socialMedia["facebook"] = $legacyData["social_facebook"];
		if($legacyData["social_github"] != null) $socialMedia["github"] = $legacyData["social_github"];
		if($legacyData["social_googleplus"] != null) $socialMedia["googleplus"] = $legacyData["social_googleplus"];
		if($legacyData["social_instagram"] != null) $socialMedia["instagram"] = $legacyData["social_instagram"];
		if($legacyData["social_reddit"] != null) $socialMedia["reddit"] = $legacyData["social_reddit"];
		if($legacyData["social_soundcloud"] != null) $socialMedia["soundcloud"] = $legacyData["social_soundcloud"];
		if($legacyData["social_steam"] != null) $socialMedia["steam"] = $legacyData["social_steam"];
		if($legacyData["social_twitch"] != null) $socialMedia["twitch"] = $legacyData["social_twitch"];
		if($legacyData["social_twitter"] != null) $socialMedia["twitter"] = $legacyData["social_twitter"];
		if($legacyData["social_youtube"] != null) $socialMedia["youtube"] = $legacyData["social_youtube"];

		if(count($socialMedia) <= 0){
			$socialMedia = null;
		}

		$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_players` (`uuid`,`username`,`lastCheck.textures`,`lastCheck.nameHistory`,`socialMedia`,`time`) VALUES(?,?,NULL,NULL,?,?);");
		if($stmt){
			$stmt->bind_param("ssss",$legacyData["uuid"],$legacyData["lastUsername"],$legacyData["socialMedia"],$legacyData["foundAt"]);
			$stmt->execute();
			$stmt->close();
		}
		/*$player = Player::getPlayer($legacyData["uuid"]);
		if($player == null) continue;

		if($player->getSocialMedia() == null){
			$socialMedia = array();

			if($legacyData["social_beam"] != null) $social["beam"] = $legacyData["social_beam"];
			if($legacyData["social_facebook"] != null) $social["facebook"] = $legacyData["social_facebook"];
			if($legacyData["social_github"] != null) $social["github"] = $legacyData["social_github"];
			if($legacyData["social_googleplus"] != null) $social["googleplus"] = $legacyData["social_googleplus"];
			if($legacyData["social_instagram"] != null) $social["instagram"] = $legacyData["social_instagram"];
			if($legacyData["social_reddit"] != null) $social["reddit"] = $legacyData["social_reddit"];
			if($legacyData["social_soundcloud"] != null) $social["soundcloud"] = $legacyData["social_soundcloud"];
			if($legacyData["social_steam"] != null) $social["steam"] = $legacyData["social_steam"];
			if($legacyData["social_twitch"] != null) $social["twitch"] = $legacyData["social_twitch"];
			if($legacyData["social_twitter"] != null) $social["twitter"] = $legacyData["social_twitter"];
			if($legacyData["social_youtube"] != null) $social["youtube"] = $legacyData["social_youtube"];

			if(count($socialMedia) > 0){
				$player->setSocialMedia($socialMedia);
			}
		}

		$player->setFoundTime($legacyData["foundAt"]);

		$player->saveToCache();
		$player->saveToDatabase();*/

		$stmt = $mysqli->prepare("DELETE FROM `mcskinhistory_accounts` WHERE `id` = ?");
		if($stmt){
			$stmt->bind_param("i",$legacyData["id"]);
			$stmt->execute();
			$stmt->close();
		}

		echo "SYNCED: " . $legacyData["uuid"] . "<br/>";
	}

	echo '<meta http-equiv="refresh" content="1; url=/convertScript/players" />';

	return "";
});

$app->get("/convertScript/skinFiles",function(){
	$mysqli = \Database::Instance()->get();
	$limit = LIMIT;
	$data = array();

	$stmt = $mysqli->prepare("SELECT * FROM `mcskinhistory_skinfiles` ORDER BY `date_added` ASC LIMIT " . $limit);
	$stmt->execute();
	$result = $stmt->get_result();
	if($result->num_rows){
		while($row = $result->fetch_assoc()){
			array_push($data,$row);
		}
	}
	$stmt->close();

	foreach($data as $legacyData){
		$fullURL = "";
		if($legacyData["type"] == "SKIN") $fullURL = "https://textures.mcskinhistory.com/skins/" . $legacyData["id"] . ".png";
		if($legacyData["type"] == "CAPE_MOJANG") $fullURL = "https://textures.mcskinhistory.com/capes/mojang/" . $legacyData["id"] . ".png";
		if($legacyData["type"] == "CAPE_OPTIFINE") $fullURL = "https://textures.mcskinhistory.com/capes/optifine/" . $legacyData["id"] . ".png";
		if($legacyData["type"] == "CAPE_LABYMOD") $fullURL = "https://textures.mcskinhistory.com/capes/labymod/" . $legacyData["id"] . ".png";

		if($fullURL != ""){
			$content = file_get_contents($fullURL);
			$md5 = md5($content);

			$textureData = null;
			if($legacyData["textureData"] != null){
				$legacyTexture = json_decode($legacyData["textureData"],true);

				$textureData = [
					"url" => $legacyTexture["texture"]["urls"]["skin"],
					"signature" => $legacyTexture["texture"]["signature"],
					"uuid" => $legacyTexture["uuid"],
					"value" => $legacyTexture["texture"]["value"]
				];

				$fullURL = $textureData["url"];
			}

			$model = $legacyData["type"] == "SKIN" ? $legacyData["model"] : null;

			$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_skinfiles` (`id`,`title`,`md5`,`full_url`,`firstUser`,`type`,`model`,`isBanned`,`randomizer`,`textureData`,`time`) VALUES(?,?,?,?,?,?,?,?,?,?,?);");
			if($stmt){
				$stmt->bind_param("issssssiiss",$legacyData["id"],$legacyData["title"],$md5,$fullURL,$legacyData["fetchedFrom"],$legacyData["type"],$model,$legacyData["isBanned"],$legacyData["randomizer"],$textureData,$legacyData["date_added"]);
				if($stmt->execute()){
					$stmt->close();
		
					$stmt = $mysqli->prepare("DELETE FROM `mcskinhistory_skinfiles` WHERE `id` = ?");
					$stmt->bind_param("i",$legacyData["id"]);
					$stmt->execute();
					$stmt->close();
		
					echo "SYNCED: #" . $legacyData["id"] . "<br/>";
				} else {
					echo "ERROR: #" . $legacyData["id"] . " " . $stmt->error . "<br/>";
					$stmt->close();
				}
			}
		}
	}

	echo '<meta http-equiv="refresh" content="1; url=/convertScript/skinFiles" />';

	return "";
});

$app->get("/convertScript/textures",function(){
	$mysqli = \Database::Instance()->get();
	$limit = LIMIT;
	$data = array();

	$stmt = $mysqli->prepare("SELECT * FROM `mcskinhistory_textures` ORDER BY `date` ASC LIMIT " . $limit);
	$stmt->execute();
	$result = $stmt->get_result();
	if($result->num_rows){
		while($row = $result->fetch_assoc()){
			array_push($data,$row);
		}
	}
	$stmt->close();

	foreach($data as $legacyData){
		$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_textures` (`id`,`uuid`,`skinFile`,`time`) VALUES(?,?,?,?);");
		if($stmt){
			$stmt->bind_param("isis",$legacyData["id"],$legacyData["uuid"],$legacyData["skinFileID"],$legacyData["date"]);
			if($stmt->execute()){
				$stmt->close();

				$stmt = $mysqli->prepare("DELETE FROM `mcskinhistory_textures` WHERE `id` = ?");
				$stmt->bind_param("i",$legacyData["id"]);
				$stmt->execute();
				$stmt->close();

				echo "SYNCED: #" . $legacyData["id"] . "<br/>";
			} else {
				echo "ERROR: #" . $legacyData["id"] . " " . $stmt->error . "<br/>";
				$stmt->close();
			}
		}
	}

	echo '<meta http-equiv="refresh" content="1; url=/convertScript/textures" />';

	return "";
});

$app->get("/convertScript/names",function(){
	$mysqli = \Database::Instance()->get();
	$limit = LIMIT;
	$data = array();

	$stmt = $mysqli->prepare("SELECT * FROM `mcskinhistory_usedNames` ORDER BY `date_added` ASC LIMIT " . $limit);
	$stmt->execute();
	$result = $stmt->get_result();
	if($result->num_rows){
		while($row = $result->fetch_assoc()){
			array_push($data,$row);
		}
	}
	$stmt->close();

	foreach($data as $legacyData){
		$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_usedNames` (`id`,`uuid`,`name`,`timeChanged`,`date_added`) VALUES(?,?,?,?,?);");
		if($stmt){
			$stmt->bind_param("issss",$legacyData["id"],$legacyData["uuid"],$legacyData["name"],$legacyData["timeChanged"],$legacyData["date_added"]);
			if($stmt->execute()){
				$stmt->close();

				$stmt = $mysqli->prepare("DELETE FROM `mcskinhistory_usedNames` WHERE `id` = ?");
				$stmt->bind_param("i",$legacyData["id"]);
				$stmt->execute();
				$stmt->close();

				echo "SYNCED: #" . $legacyData["id"] . "<br/>";
			} else {
				echo "ERROR: #" . $legacyData["id"] . " " . $stmt->error . "<br/>";
				var_dump($legacyData);
				$stmt->close();
			}
		}
	}

	echo '<meta http-equiv="refresh" content="1; url=/convertScript/names" />';

	return "";
});