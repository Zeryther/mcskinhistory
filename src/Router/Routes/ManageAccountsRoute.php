<?php

use SkinHistory\Player\Player;
use SkinHistory\Util\Util;

$app->bind("/account/linked/manage/:uuid",function($params){
	$uuid = $params["uuid"];

	if(Util::isLoggedIn()){
		$player = Player::getPlayer($uuid);

		if($player != null && $player->getClaimingAccount() != null && $player->getClaimingAccount()->getID() == Util::getCurrentAccount()->getID()){
			return $this->render("views:AccountManager/Manage.php with views:Layout.php",[
				"title" => tr("navigation.account.linkedMinecraftAccounts"),
				"description" => DEFAULT_DESCRIPTION,
				"twitterImage" => DEFAULT_TWITTER_IMAGE,
				"nav" => NAV_ACCOUNT,
				"includeAccountNav" => TRUE,
				"accountNav" => ACCOUNT_NAV_LINKED,
				"player" => $player
			]);
		} else {
			return $this->render("views:AccountManager/InvalidAccount.php with views:Layout.php",[
				"title" => tr("profile.invalidAccount"),
				"description" => DEFAULT_DESCRIPTION,
				"twitterImage" => DEFAULT_TWITTER_IMAGE,
				"nav" => NAV_ACCOUNT,
				"includeAccountNav" => TRUE,
				"accountNav" => ACCOUNT_NAV_LINKED
			]);
		}
	} else {
		return $this->reroute("/login");
	}
});