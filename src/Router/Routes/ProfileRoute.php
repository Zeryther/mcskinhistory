<?php

use SkinHistory\Player\Player;

$app->bind("/player/:query", function($params){
	$query = $params["query"];
	
	if(!empty($query)){
		$player = Player::getPlayer($query);

		if($player != null){
			if($query != $player->getUUID()){
				$this->reroute("/player/" . $player->getUUID());
			} else {
				$player->getNameHistory(); // load name history
				$player->getTextures(); // load textures

				$dateTimeNow = time();
				$dateTimeTextures = $player->getLastTexturesCheck() != null ? strtotime($player->getLastTexturesCheck()) : null;

				if(CHECK_UPDATE_ON_PROFILE_LOAD == true){
					if($dateTimeTextures == null || (($dateTimeNow-$dateTimeTextures) >= 3600)){
						$player->checkUpdate(SKIN_TYPE_MOJANG);
						$player->checkUpdate(SKIN_TYPE_OPTIFINE);
						$player->checkUpdate(SKIN_TYPE_LABYMOD);
					}
				} else {
					if($dateTimeTextures == null || (($dateTimeNow-$dateTimeTextures) >= 3600)){
						$player->addToQueue();
					}
				}

				return $this->render("views:Profile/Profile.php with views:Layout.php",[
					"title" => $player->getUsername(),
					"description" => "Lookup old Minecraft skins, OptiFine capes and LabyMod capes used by " . $player->getUsername(),
					"twitterImage" => "https://crafatar.com/avatars/" . $player->getUUID() . "?overlay&default=MHF_Steve",
					"player" => $player,
                    "searchPlaceholder" => $player->getUsername()
				]);
			}
		} else {
			if(strlen($query) == 32 || strlen($query) == 36){
				return $this->render("views:UnknownUser.php with views:Layout.php",[
					"title" => $query,
					"description" => DEFAULT_DESCRIPTION,
					"twitterImage" => DEFAULT_TWITTER_IMAGE,
					"query" => $query,
					"searchPlaceholder" => $query
				]);
			} else {
				return $this->reroute("/name/" . $query);
			}
		}
	} else {
		return $this->reroute("/");
	}
});