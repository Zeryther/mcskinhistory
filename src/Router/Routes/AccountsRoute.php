<?php

use SkinHistory\Account\Account;
use SkinHistory\Player\Player;
use SkinHistory\Util\Util;

if(Util::isDevMode()){
	$app->get("/login", function(){
		if(isset($_GET["id"]) && is_numeric($_GET["id"])){
			$account = Account::getAccount($_GET["id"]);

			if(!is_null($account)){
				$_SESSION["id"] = $account->getID();
				$_SESSION["username"] = $account->getUsername();
			}
		}

		return $this->reroute("/");
	});
} else {
	$app->get("/login", function(){
		if(!Util::isLoggedIn()){
			$redir = "http" . (isset($_SERVER["HTTPS"]) ? "s" : "") . "://" . $_SERVER["HTTP_HOST"] . "/loginCallback";
			//$url = "https://gigadrivegroup.com/login?redir=" . urlencode($redir) . "&requiredServices=MINECRAFT&permissions=email,externalAccounts";
			$url = "https://gigadrivegroup.com/authorize?app=1&scopes=user:info,user:email,user:external";
	
			return $this->reroute($url);
		} else {
			return $this->reroute("/");
		}
	});
}

$app->get("/logout",function(){
	if(Util::isLoggedIn()){
		session_destroy();
	}

	return $this->reroute("/");
});

$app->get("/register",function(){
	if(!Util::isLoggedIn()){
		return $this->reroute("https://gigadrivegroup.com/register");
	} else {
		return $this->reroute("/");
	}
});

$app->bind("/account",function(){
	if(Util::isLoggedIn()){
		return $this->render("views:AccountSettings.php with views:Layout.php",[
			"title" => tr("account.myAccount.headline"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_ACCOUNT,
			"includeAccountNav" => TRUE,
			"accountNav" => ACCOUNT_NAV_ACCOUNT
		]);
	} else {
		return $this->reroute("/login");
	}
});

$app->bind("/account/restoreLegacy",function(){
	if(Util::isLoggedIn()){
		return $this->render("views:RestoreLegacy.php with views:Layout.php",[
			"title" => tr("restoreLegacy.headline"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_ACCOUNT,
			"includeAccountNav" => TRUE,
			"accountNav" => ACCOUNT_NAV_ACCOUNT
		]);
	} else {
		return $this->reroute("/login");
	}
});

$app->bind("/account/linked",function(){
	if(Util::isLoggedIn()){
		return $this->render("views:LinkedAccounts.php with views:Layout.php",[
			"title" => tr("navigation.account.linkedMinecraftAccounts"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"nav" => NAV_ACCOUNT,
			"includeAccountNav" => TRUE,
			"accountNav" => ACCOUNT_NAV_LINKED
		]);
	} else {
		return $this->reroute("/login");
	}
});

$app->get("/loginCallback", function(){
	if(!Util::isLoggedIn()){
		if(isset($_GET["code"])){
			$url = "https://gigadrivegroup.com/api/v3/token?secret=" . GIGADRIVE_API_KEY . "&code=" . urlencode($_GET["code"]);
			$result = file_get_contents($url);
			$j = json_decode($result,true);

			if(isset($j["success"]) && !empty($j["success"]) && isset($j["token"]) && !empty($j["token"])){
				$token = $j["token"];

				$url = "https://gigadrivegroup.com/api/v3/user?secret=" . GIGADRIVE_API_KEY . "&token=" . urlencode($token);
				$result = file_get_contents($url);
				$j = json_decode($result,true);

				if(isset($j["success"]) && !empty($j["success"]) && isset($j["user"])){
					$userData = $j["user"];

					if(isset($userData["email"]) && isset($userData["id"]) && isset($userData["username"])){
						if(isset($userData["externalAccounts"]) && isset($userData["externalAccounts"]["MINECRAFT"])){
							$uuid = $userData["externalAccounts"]["MINECRAFT"];
						}
	
						$userID = $userData["id"];
						$userName = $userData["username"];
						$userEmail = $userData["email"];
	
						$_SESSION["id"] = $userID;
						$_SESSION["username"] = $userName;
	
						Account::registerAccount($userID,$userName,$userEmail,$token);
	
						$account = Account::getAccount($userID);
						if($account != null && $uuid != null && !empty($uuid)){
							$p = Player::getPlayer($uuid);
	
							if($p != null){
								if($p->getClaimingAccount() == null){
									$uuid = $p->getUUID();
									$uid = $account->getID();

									$mysqli = \Database::Instance()->get();
									$stmt = $mysqli->prepare("INSERT INTO `skinhistory_linkedPlayers` (`uuid`,`account`) VALUES(?,?);");
									$stmt->bind_param("si",$uuid,$uid);
									$stmt->execute();
									$stmt->close();
	
									$account->reloadLinkedAccounts();
									$p->reloadClaimingAccount();
								}
							}

							if(is_null($account->getPrimaryPlayer())){
								$linkedAccounts = $account->getLinkedAccounts();

								if(!is_null($linkedAccounts) && count($linkedAccounts) > 0){
									$account->setPrimaryPlayer($linkedAccounts[0]["uuid"]);
								}
							}
						}
	
						return $this->reroute("/?msg=loggedIn");
					} else {
						return $this->reroute("/");
					}
				} else {
					return $this->reroute("/");
				}
			} else {
				return $this->reroute("/");
			}
		} else {
			return $this->reroute("/");
		}
	} else {
		return $this->reroute("/");
	}
});