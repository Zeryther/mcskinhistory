<?php

$app->bind("/name/:query", function($params){
	$query = $params["query"];
	
	if(!empty($query)){
		$name = $query;

		return $this->render("views:Name.php with views:Layout.php",[
			"title" => $name . " - " . tr("profile.part.name"),
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => DEFAULT_TWITTER_IMAGE,
			"query" => $query,
			"name" => $name,
			"searchPlaceholder" => $name
		]);
	} else {
		return $this->reroute("/");
	}
});