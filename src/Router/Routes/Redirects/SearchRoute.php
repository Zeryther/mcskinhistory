<?php

use SkinHistory\Util\Util;
use SkinHistory\Server\Server;

$app->bind("/search", function(){
	if(isset($_GET["query"]) && !empty($_GET["query"])){
		if(Server::isValidIP($_GET["query"])){
			$i = Util::isDevMode() ? "?ip=" : "";

			return $this->reroute("/server/" . $i . $_GET["query"]);
		} else if(count(Util::getNameInfoResults($_GET["query"])) >= 2){
			return $this->reroute("/name/" . $_GET["query"]);
		} else {
			return $this->reroute("/player/" . $_GET["query"]);
		}
	} else {
		return $this->reroute("/");
	}
});