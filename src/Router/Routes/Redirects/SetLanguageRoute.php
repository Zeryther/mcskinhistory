<?php

use \SkinHistory\Util\Util;

$app->bind("/set-language",function(){
    if(isset($_GET["code"])){
        $locale = \Gigadrive\i18n\i18n::getLocale($_GET["code"]);

        if(!is_null($locale)){
            \SkinHistory\Util\Util::setCookie("lang",$locale->getCode(),30);
        }
    }

    return $this->reroute("/");
});