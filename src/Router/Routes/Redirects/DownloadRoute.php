<?php

use SkinHistory\Skin\SkinFile;
use SkinHistory\Util\Util;

$app->bind("/download/:id",function($params){
	$id = $params["id"];

	if(!empty($id)){
		$skinFile = SkinFile::getSkinFileFromID($id);
		if($skinFile != null){
			$fullURL = $skinFile->getFullURL();
			$path = $_SERVER["DOCUMENT_ROOT"] . "/tmp/" . Util::getRandomString() . ".png";
			file_put_contents($path,file_get_contents($fullURL));
			http_response_code(200);
			$this->response->status = "200";

			//$this->response->mime = "png";
			header("Content-Description: File Transfer");
			header("Content-Type: application/octet-stream");
			header("Content-Disposition: attachment; filename=skinfile-".basename($path));
			header("Content-Transfer-Encoding: binary");
			header("Expires: 0");
			header("Cache-Control: must-revalidate");
			header("Pragma: public");
			header("Content-Length: " . filesize($path));
			ob_clean();
			flush();
			readfile($path);
			exit;
		} else {
			return $this->reroute("/");	
		}
	} else {
		return $this->reroute("/");
	}
});