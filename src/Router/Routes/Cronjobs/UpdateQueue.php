<?php

use SkinHistory\Player\Player;
use SkinHistory\Util\Util;

$app->get("/cron/updateQueue",function(){
	$this->response->mime = "json";

	if(Util::validateCronjob()){
		$uuids = array();
		$mysqli = \Database::Instance()->get();

		$limit = 5;

		$stmt = $mysqli->prepare("SELECT `uuid` FROM `skinhistory_checkQueue` ORDER BY `time` ASC LIMIT " . $limit);
		$stmt->execute();
		$result = $stmt->get_result();
		if($result->num_rows){
			while($row = $result->fetch_assoc()){
				if(count($uuids) < $limit){
					array_push($uuids,$row["uuid"]);
				}
			}
		}

		$stmt->close();

		foreach($uuids as $uuid){
			$player = Player::getPlayer($uuid);

			if($player != null){
				$player->checkUpdate(SKIN_TYPE_MOJANG);
				$player->checkUpdate(SKIN_TYPE_OPTIFINE);
				$player->checkUpdate(SKIN_TYPE_LABYMOD);
            }
        }

        $d = "'" . implode("','",$uuids) . "'";

        $stmt = $mysqli->prepare("DELETE FROM `skinhistory_checkQueue` WHERE `uuid` IN ($d)");
        $stmt->execute();
        $stmt->close();

		return json_encode(["success" => $uuids]);
	} else {
		return $this->reroute("/");
	}
});