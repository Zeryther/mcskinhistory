<?php

use SkinHistory\Util\Util;

$app->get("/cron/staleServers",function(){
	$this->response->mime = "json";

	if(Util::validateCronjob()){
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("DELETE FROM `skinhistory_servers` WHERE `lastSuccessfulPing` < (NOW() - INTERVAL 3 MONTH)");
		$r = $stmt->execute();
		$stmt->close();

		if($r){
			return json_encode(["success" => "task executed"]);
		} else {
			return json_encode(["error" => $stmt->error]);
		}
	} else {
		return $this->reroute("/");
	}
});