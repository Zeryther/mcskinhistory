<?php

use SkinHistory\Util\Util;

$app->get("/cron/clearCache",function(){
	$this->response->mime = "json";

	if(Util::validateCronjob()){
        \CacheHandler::clearCache();
        
        return json_encode(["success" => "task executed"]);
	} else {
		return $this->reroute("/");
	}
});