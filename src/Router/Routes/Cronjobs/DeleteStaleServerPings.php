<?php

use SkinHistory\Util\Util;

$app->get("/cron/staleServerPings",function(){
	$this->response->mime = "json";

	if(Util::validateCronjob()){
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("DELETE FROM `skinhistory_serverpings` WHERE `date` < (NOW() - INTERVAL 3 MONTH)");
		$r = $stmt->execute();
		$stmt->close();

		if($r){
			return json_encode(["success" => "task executed"]);
		} else {
			return json_encode(["error" => $stmt->error]);
		}
	} else {
		return $this->reroute("/");
	}
});