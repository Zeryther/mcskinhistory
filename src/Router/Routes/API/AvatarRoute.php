<?php

use SkinHistory\Skin\SkinFile;

$app->bind("/avatar/cape/:id", function($params){
	$id = $params["id"];
	if(!empty($id)){
		$skinFile = SkinFile::getSkinFileFromID($id);

		if($skinFile != null){
			$url = $skinFile->getFullURL();

			if($skinFile->getType() == "CAPE_MOJANG" || $skinFile->getType() == "CAPE_OPTIFINE" || $skinFile->getType() == "CAPE_LABYMOD"){
				$cacheName = "avatar_cape_" . $id;
				$cached = false;

				$avatarString = null;
				if(\CacheHandler::existsInCache($cacheName)){
					$cached = true;
					$avatarString = \CacheHandler::getFromCache($cacheName);
				} else {
					$avatarString = file_get_contents($url,false,stream_context_create(["http" => ["method" => "GET","user_agent" => USER_AGENT_BOT_NAME]]));

					if($avatarString != null){
						\CacheHandler::setToCache($cacheName,$avatarString,30*60);
					}
				}

				if($avatarString != null){
					$this->response->mime = "png";

					$width = 0;
					$height = 0;
					list($width,$height) = getimagesizefromstring($avatarString);

					$face_x = 0;
					$face_y = 0;
					$face_width = 0;
					$face_height = 0;

					$avatar_width = 0;
					$avatar_height = 0;

					if($width == 22 && $height == 17 || $width == 64 && $height == 32){
						// MOJANG FORMAT CAPE
						$face_x = 0;
						$face_y = 0;
						$face_width = 12;
						$face_height = 17;
						$avatar_width = $face_width*12;
						$avatar_height = $face_height*12;
					} else if($width == 44 && $height == 34){
						// BANNER FORMAT CAPE
						$face_x = 0;
						$face_y = 0;
						$face_width = 12*2;
						$face_height = 17*2;
						$avatar_width = $face_width*(12/2);
						$avatar_height = $face_height*(12/2);
					} else if($width == 44*2 && $height == 34*2){
						 // SPECIAL FORMAT CAPE
						$face_x = 0;
						$face_y = 0;
						$face_width = 12*4;
						$face_height = 17*4;
						$avatar_width = $face_width*(12/4);
						$avatar_height = $face_height*(12/4);
					} else if($width == 355 && $height == 275){
						 // LABYMOD HD CAPE
						$face_x = 17;
						$face_y = 17;
						$face_width = 162;
						$face_height = 259;
						$avatar_width = $face_width;
						$avatar_height = $face_height;
					} else if($width == 120 && $height == 240){
						 // ONE SIDED
						$face_x = 0;
						$face_y = 0;
						$face_width = 120;
						$face_height = 240;
						$avatar_width = $face_width;
						$avatar_height = $face_height;
					} else {
						$face_x = 0;
						$face_y = 0;
						$face_width = $width/2;
						$face_height = $height;
						$avatar_width = $face_width;
						$avatar_height = $face_height;
					}

					$avatar = imagecreatetruecolor($avatar_width,$avatar_height);
					imagecopyresized($avatar,imagecreatefromstring($avatarString),0,0,$face_x,$face_y,$avatar_width,$avatar_height,$face_width,$face_height);

					imagepng($avatar);
					imagedestroy($avatar);

					return;
				} else {
					$this->response->mime = "json";
					return json_encode(["error" => "Unable to get file content (cached: " . ($cached ? "true" : "false") . ")"]);
				}
			} else {
				$this->response->mime = "json";
				return json_encode(["error" => "Passed id is not a cape"]);
			}
		} else {
			$this->response->mime = "json";
			return json_encode(["error" => "Unknown id passed"]);
		}
	} else {
		$this->response->mime = "json";
		return json_encode(["error" => "Empty id passed"]);
	}
});

$app->bind("/avatar/skin/face/:id", function($params){
	$id = $params["id"];
	if(!empty($id)){
		$skinFile = SkinFile::getSkinFileFromID($id);

		if($skinFile != null){
			$url = $skinFile->getFullURL();

			if($skinFile->getType() == "SKIN"){
				$cacheName = "avatar_face_" . $id;
				$cached = false;

				$avatarString = null;
				if(\CacheHandler::existsInCache($cacheName)){
					$avatarString = \CacheHandler::getFromCache($cacheName);
					$cached = true;
				} else {
					$avatarString = file_get_contents($url,false,stream_context_create(["http" => ["method" => "GET","user_agent" => USER_AGENT_BOT_NAME]]));
					\CacheHandler::setToCache($cacheName,$avatarString,30*60);
				}

				if($avatarString != null){
					$this->response->mime = "png";

					$face_x = 8;
					$face_y = 8;

					$face_width = 8;
					$face_height = 8;

					$mask_x = 40;
					$mask_y = 8;

					$mask_width = 8;
					$mask_height = 8;

					$avatar_width = $face_width*12;
					$avatar_height = $face_height*12;
					
					$skin = @imagecreatefromstring($avatarString);
					$avatar = imagecreatetruecolor($avatar_width, $avatar_height);

					imagecopyresized($avatar, $skin, 0, 0, $face_x, $face_y, $avatar_width, $avatar_height, $face_width, $face_height);
					imagecopyresized($avatar, $skin, 0, 0, $mask_x, $mask_y, $avatar_width, $avatar_height, $mask_width, $mask_height);

					imagepng($avatar);
					imagedestroy($avatar);
					return;
				} else {
					$this->response->mime = "json";
					return json_encode(["error" => "Unable to get file content (cached: " . ($cached ? "true" : "false") . ")"]);
				}
			} else {
				$this->response->mime = "json";
				return json_encode(["error" => "Passed id is not a skin"]);
			}
		} else {
			$this->response->mime = "json";
			return json_encode(["error" => "Unknown id passed"]);
		}
	} else {
		$this->response->mime = "json";
		return json_encode(["error" => "Empty id passed"]);
	}
});