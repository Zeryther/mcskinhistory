<?php

$app->get("/proxy",function(){
	if(!isset($_GET["url"]) || empty($_GET["url"])){
		return $this->reroute("/");
	} else {
		$url = urldecode($_GET["url"]);
		$cacheName = "proxyImage_" . urlencode($url);
		$imageContent = null;

		if(\CacheHandler::existsInCache($cacheName)){
			$imageContent = \CacheHandler::getFromCache($cacheName);
		} else {
			$imageContent = file_get_contents($url,false,stream_context_create(array("http"=>array("method"=>"GET","user_agent"=>USER_AGENT_BOT_NAME))));
			\CacheHandler::setToCache($cacheName,$imageContent,10*60);
		}

		$width = 0;
		$height = 0;
		list($width,$height) = getimagesizefromstring($imageContent);

		$img = imagecreatetruecolor($width,$height);
		imagealphablending($img, false);
		imagesavealpha($img, true);
		imagecopyresized($img,imagecreatefromstring($imageContent),0,0,0,0,$width,$height,$width,$height);

		$this->response->mime = "png";

		imagepng($img);
		imagedestroy($img);

		return;
	}
});