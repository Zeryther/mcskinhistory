<?php

use SkinHistory\Player\Player;
use SkinHistory\Server\Server;

$app->bind("/api/playerData/:id",function($params){
	$this->response->mime = "json";

	$id = $params["id"];

	if(!empty($id)){
		$player = Player::getPlayer($id);

		if(!is_null($player)){
			return json_encode(["uuid" => $player->getUUID(),"username" => $player->getUsername()]);
		} else {
			return json_encode(["error" => "Invalid UUID or username!"]);
		}
	} else {
		return json_encode(["error" => "Invalid UUID or username!"]);
	}
});

$app->bind("/api/serverData/:ip",function($params){
	$this->response->mime = "json";

	$ip = !empty($params["ip"]) ? $params["ip"] : isset($_GET["ip"]) ? $_GET["ip"] : "";

	if(!empty($ip)){
		$server = Server::getServer($ip);
		
		if(!is_null($server)){
			while(!is_null($server->getParent())){
				$server = $server->getParent();
			}
	
			if(!is_null($server)){
				return json_encode([
						"ip" => $server->getIP(),
						"name" => $server->getName(),
						"children" => $server->getChildren(),
						"players" => [
							"online" => $server->getPlayersOnline(),
							"max" => $server->getPlayersMax(),
							"peak" => $server->getPlayerPeak()
						],
						"motd" => $server->getMotd(),
						"uptime" => $server->getUptime(),
						"version" => $server->getVersion(),
						"time" => $server->getTime(),
						"icon" => $server->getIcon()
					]);
			} else {
				return json_encode(["error" => "Invalid server IP."]);
			}
		} else {
			return json_encode(["error" => "Invalid server IP."]);
		}
	} else {
		return json_encode(["error" => "Invalid server IP."]);
	}
});