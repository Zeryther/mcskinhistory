<?php

use SkinHistory\Skin\SkinFile;
use SkinHistory\Util\Util;

$app->bind("/cape/:id", function($params){
	$id = $params["id"];
	
	if(!empty($id)){
		$cape = SkinFile::getSkinFileFromID($id);

		if($cape != null && Util::contains($cape->getType(),"CAPE_")){
			$d = isset($_SERVER["HTTPS"]) ? "s" : "";
			$head = Util::proxyImage("http" . $d . "://" . $_SERVER["HTTP_HOST"] . "/avatar/cape/" . $cape->getID());

			return $this->render("views:Cape.php with views:Layout.php",[
				"title" => tr("cape.headline",[$cape->getTitle() != null ? $cape->getTitle() : "#" . $cape->getID()]),
				"description" => DEFAULT_DESCRIPTION,
				"twitterImage" => $head,
				"cape" => $cape
			]);
		}
	} else {
		return $this->reroute("/");
	}
});