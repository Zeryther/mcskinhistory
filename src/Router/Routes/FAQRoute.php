<?php

$app->bind("/faq", function(){
	return $this->render("views:FAQ.php with views:Layout.php",[
		"title" => tr("faq.headline"),
		"description" => tr("faq.description"),
		"twitterImage" => DEFAULT_TWITTER_IMAGE,
		"nav" => NAV_FAQ
	]);
});