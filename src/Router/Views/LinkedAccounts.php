<?php

use SkinHistory\Player\Player;
use SkinHistory\Util\Util;

if(Util::getCurrentAccount()->getLegacyAccount() == null) Util::createAlert("linkedAccountsLegacyInfo","If you registered before May 11th 2018, click <a href=\"" . $app->routeUrl("/account/restoreLegacy") . "\">here</a> for information on how to retrieve your old account&apos;s data.",ALERT_TYPE_INFO,true,true);

?>
<div class="modal fade" id="linkModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Linking a new Minecraft account</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<div class="mx-2 my-2">
				<ul class="nav nav-pills nav-fill mb-3" id="tabs" role="tablist">
					<li class="nav-item"><a id="tablist-mcoauth" class="nav-link active" href="#mcOAuthTab" aria-controls="#mcOAuthTab" aria-selected="true" data-toggle="pill">Minecraft OAuth</a></li>
					<li class="nav-item"><a id="tablist-classic" class="nav-link" href="#classicAuthTab" aria-controls="#infoTab" aria-selected="false" data-toggle="pill">Classic Authentication</a></li>
				</ul>
			</div>

			<div class="tab-content" id="tabsContent">
				<div class="tab-pane fade show active" id="mcOAuthTab" role="tabpanel" aria-labelledby="tablist-mcoauth">
					<form action="/account/linked" method="post">
						<div class="modal-body">
							<p class="mt-0 mb-2 text-center">
								Use Minecraft OAuth to add your Minecraft account by joining a server and entering a code given to you.<br/><br/>
								MCSkinHistory is not affiliated with MCAuth and does not control their services, any downtimes are outside of our reach.<br/><br/>
								<b>Connect to <u>srv.mc-oauth.net:25565</u> with the to-be-linked account and enter the token given to you below.</b>
							</p>

							<input type="hidden" name="action" value="mcOAuth"/>
							<input type="text" class="form-control" name="token" placeholder="Your token"/>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Link</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>

				<div class="tab-pane fade" id="classicAuthTab" role="tabpanel" aria-labelledby="tablist-classic">
					<form action="/account/linked" method="post">
						<div class="modal-body">
							<p class="mt-0 mb-2 text-center">
								Use the classic authentication to add your Minecraft account by entering your Minecraft credentials.<br/><br/>
								MCSkinHistory does <b>not</b> store any of your authentication information including your Mojang password, it is merely used for authentication.
							</p>

							<input type="hidden" name="action" value="classicAuth"/>
							<input type="text" class="form-control my-3" name="minecraftemail" placeholder="Your Mojang email address"/>
							<input type="password" class="form-control" name="minecraftpassword" placeholder="Your Mojang password"/>
						</div>

						<div class="modal-footer">
							<button type="submit" class="btn btn-primary">Link</button>
							<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
<?php

if(isset($_POST["action"])){
	$action = $_POST["action"];

	if($action == "unlink"){
		if(isset($_POST["uuid"])){
			$p = Player::getPlayer($_POST["uuid"]);

			if($p != null){
				if($p->getClaimingAccount() != null && $p->getClaimingAccount()->getID() == Util::getCurrentAccount()->getID()){
					$mysqli = \Database::Instance()->get();

					$stmt = $mysqli->prepare("DELETE FROM `skinhistory_linkedPlayers` WHERE `uuid` = ?");
					$stmt->bind_param("s",$p->getUUID());
					$stmt->execute();
					$stmt->close();

					Util::getCurrentAccount()->reloadLinkedAccounts();
					$p->reloadClaimingAccount();

					Util::createAlert("unlinked","The account has been unlinked successfully.",ALERT_TYPE_SUCCESS);
				}
			}
		}
	} else if($action == "classicAuth"){
		if(isset($_POST["minecraftemail"]) && isset($_POST["minecraftpassword"])){
			$minecraftEmail = trim($_POST["minecraftemail"]);
			$minecraftPassword = trim($_POST["minecraftpassword"]);

			if(!empty($minecraftEmail) && !empty($minecraftPassword)){
				if(filter_var($minecraftEmail, FILTER_VALIDATE_EMAIL)){
					$mcAuth = new \MCAuth();

					try {
						if($mcAuth->authenticate($minecraftEmail,$minecraftPassword) == true){
							$uuid = $mcAuth->account["id"];
							$username = $mcAuth->account["username"];
							$token = $mcAuth->account["token"];

							$p = Player::getPlayer($uuid);

							if(!is_null($p)){
								$uuid = $p->getUUID();
								$uid = Util::getCurrentAccount()->getID();

								if(is_null($p->getClaimingAccount())){
									$mysqli = \Database::Instance()->get();
									
									$stmt = $mysqli->prepare("INSERT INTO `skinhistory_linkedPlayers` (`uuid`,`account`) VALUES(?,?);");
									$stmt->bind_param("si",$uuid,$uid);
									$stmt->execute();
									$stmt->close();

									Util::getCurrentAccount()->reloadLinkedAccounts();
									$p->reloadClaimingAccount();

									Util::createAlert("successMsg",tr("account.linkedAccounts.success"),ALERT_TYPE_SUCCESS);
								} else {
									Util::createAlert("errorMsg",tr("account.linkedAccounts.alreadyClaimed"),ALERT_TYPE_DANGER);
								}
							} else {
								Util::createAlert("errorMsg",tr("account.linkedAccounts.playerNotFound"),ALERT_TYPE_DANGER);
							}
						} else {
							Util::createAlert("errorMsg",tr("account.linkedAccounts.invalidCredentials"),ALERT_TYPE_DANGER);
						}
					} catch(Exception $e){
						Util::createAlert("errorMsg",$e->getMessage(),ALERT_TYPE_DANGER);
					}
				} else {
					Util::createAlert("errorMsg",tr("account.linkedAccounts.invalidEmail"),ALERT_TYPE_DANGER);
				}
			} else {
				Util::createAlert("errorMsg",tr("error.fillAllFields"),ALERT_TYPE_DANGER);
			}
		}
	} else if($action == "mcOAuth"){
		if(isset($_POST["token"])){
			$token = trim($_POST["token"]);

			if(!empty($token)){
				$curl = curl_init();

				curl_setopt_array($curl,array(
					CURLOPT_URL => "https://mc-oauth.net/api/api?token",
					CURLOPT_POST => false,
					CURLOPT_RETURNTRANSFER => true,
					CURLOPT_HTTPHEADER => [
						"token: " . $token
					]
				));
		
				$result = curl_exec($curl);
				curl_close($curl);

				$result = json_decode($result,true);

				if($result["status"] && $result["status"] == "success" && $result["uuid"]){
					$uuid = $result["uuid"];

					$p = Player::getPlayer($uuid);

					if(!is_null($p)){
						$uuid = $p->getUUID();
						$uid = Util::getCurrentAccount()->getID();

						if(is_null($p->getClaimingAccount())){
							$mysqli = \Database::Instance()->get();
									
							$stmt = $mysqli->prepare("INSERT INTO `skinhistory_linkedPlayers` (`uuid`,`account`) VALUES(?,?);");
							$stmt->bind_param("si",$uuid,$uid);
							$stmt->execute();
							$stmt->close();

							Util::getCurrentAccount()->reloadLinkedAccounts();
							$p->reloadClaimingAccount();

							Util::createAlert("successMsg",tr("account.linkedAccounts.success"),ALERT_TYPE_SUCCESS);
						} else {
							Util::createAlert("errorMsg",tr("account.linkedAccounts.alreadyClaimed"),ALERT_TYPE_DANGER);
						}
					} else {
						Util::createAlert("errorMsg",tr("account.linkedAccounts.playerNotFound"),ALERT_TYPE_DANGER);
					}
				} else {
					if($result["message"]){
						Util::createAlert("errorMsg",$result["message"],ALERT_TYPE_DANGER);
					} else {
						Util::createAlert("errorMsg",tr("error.any"),ALERT_TYPE_DANGER);
					}
				}
			} else {
				Util::createAlert("errorMsg",tr("error.fillAllFields"),ALERT_TYPE_DANGER);
			}
		}
	}
}

if(count(Util::getCurrentAccount()->getLinkedAccounts()) > 0){
	foreach(Util::getCurrentAccount()->getLinkedAccounts() as $account){
		$player = Player::getPlayer($account["uuid"]);

		?>
<div class="modal fade" id="unlinkModal<?= $player->getUUID(); ?>">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?= tr("account.linkedAccounts.unlinkModal.headline",$player->getUsername()) ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<form action="/account/linked" method="post">
				<input type="hidden" name="action" value="unlink"/>
				<input type="hidden" name="uuid" value="<?= $player->getUUID(); ?>"/>
				<div class="modal-body">
					<p>
						<?= tr("account.linkedAccounts.unlinkModal.text",$player->getUsername()) ?>
					</p>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-danger"><?= tr("account.linkedAccounts.unlinkModal.unlinkButton") ?></button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= tr("modalButton.cancel") ?></button>
				</div>
			</form>
		</div>
	</div>
</div>
		<?php
	}
}

if(isset($_GET["name"])){
	?>
<script type="text/javascript">
    $(window).on('load',function(){
        $('#linkModal').modal('show');
    });
</script>
	<?php
}

?>
<div class="card">
	<h5 class="card-header"><button type="button" data-toggle="modal" data-target="#linkModal" class="my-0 btn btn-success btn-sm float-right"><i class="fas fa-plus"></i> Link Account</button>Linked Minecraft Accounts</h5>

	<?php

		if(count(Util::getCurrentAccount()->getLinkedAccounts()) > 0){
			?>
	<table class="table my-0">
		<thead>
			<tr>
				<th style="width: 50%"><?= tr("account.linkedAccounts.list.name") ?></th>
				<th><?= tr("account.linkedAccounts.list.dateLinked") ?></th>
				<th>&nbsp;</th>
			</tr>
		</thead>
		<tbody>
			<?php

				foreach(Util::getCurrentAccount()->getLinkedAccounts() as $account){
					$player = Player::getPlayer($account["uuid"]);
					$time = $account["time"];

					?>
			<tr>
				<td style="width: 50%"><a href="<?= "/player/" . $player->getUUID(); ?>"><img style="margin-right: 10px" src="https://crafatar.com/avatars/<?= $player->getUUID(); ?>?overlay&size=24&default=MHF_Steve" /><?= $player->getUsername(); ?></a></td>
				<td><?= Util::timeago($time); ?></td>
				<td>
					<div class="float-right">
						<a href="<?= "/account/linked/manage/" . $player->getUUID(); ?>"><button type="button" class="btn btn-info btn-sm"><?= tr("account.linkedAccounts.list.manageButton") ?></button></a>
						<button type="button" data-toggle="modal" data-target="#unlinkModal<?= $player->getUUID(); ?>" class="btn btn-primary btn-sm"><?= tr("account.linkedAccounts.unlinkModal.unlinkButton") ?></button>
					</div>
				</td>
			</tr>
					<?php
				}

			?>
		</tbody>
	</table>
			<?php
		} else {
			?>
	<div class="card-body">
		<?= tr("account.linkedAccounts.noAccountsLinked") ?>
	</div>
			<?php
		}

	?>
</div>