<div class="container my-3">
	<h1><?= tr("faq.headline") ?></h1>

	<?php

		use SkinHistory\Util\Util;

		echo '<center>';
		Util::renderAd(AD_TYPE_LEADERBOARD);
		echo '</center>';

	?>

	<small><?= tr("faq.lastUpdate",[Util::timeago("2016-04-26")]) ?></small>

	<p>
		<b><?= tr("faq.1.question") ?></b><br/>
		<?= tr("faq.1.answer") ?>
	</p>

	<p>
		<b><?= tr("faq.2.question") ?></b><br/>
		<?= tr("faq.2.answer") ?>
	</p>

	<p>
		<b><?= tr("faq.3.question") ?></b><br/>
		<?= tr("faq.3.answer") ?>
	</p>

	<p>
		<b><?= tr("faq.4.question") ?></b><br/>
		<?= tr("faq.4.answer") ?>
	</p>

	<p>
		<b><?= tr("faq.5.question") ?></b><br/>
		<?= tr("faq.5.answer") ?>
	</p>

	<p>
		<b><?= tr("faq.6.question") ?></b><br/>
		<?= tr("faq.6.answer",['href="' . $app->routeUrl("/player/Zeryther") . '"']) ?>
	</p>

	<p>
		<b><?= tr("faq.7.question") ?></b><br/>
		<?= tr("faq.7.answer") ?>
	</p>

	<p>
		<b><?= tr("faq.8.question") ?></b><br/>
		<?= tr("faq.8.answer",['href="https://patreon.com/Zeryther" target="_blank"']) ?>
	</p>

	<?php

		echo '<center>';
		Util::renderAd(AD_TYPE_LEADERBOARD);
		echo '</center>';

	?>
</div>
