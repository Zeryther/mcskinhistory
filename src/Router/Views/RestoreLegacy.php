<?php

use SkinHistory\Player\Player;
use SkinHistory\Util\Util;

$mysqli = \Database::Instance()->get();

function canBeClaimed($mysqli,$legacyAccountID){
	$r = true;

	$stmt = $mysqli->prepare("SELECT COUNT(id) AS count FROM `skinhistory_accounts` WHERE `legacyAccount` = ?");
	$stmt->bind_param("i",$legacyAccountID);
	$stmt->execute();
	$result = $stmt->get_result();
	if($result->num_rows){
		$row = $result->fetch_assoc();

		if($row["count"] > 0){
			$r = false;
		}
	}
	$stmt->close();

	return $r;
}

$account = Util::getCurrentAccount();

if($account->getLegacyAccount() == null){
	if(!isset($_POST["finish"]) && isset($_POST["email"]) && isset($_POST["password"]) && !empty($_POST["email"]) && !empty($_POST["password"])){
		Util::createAlert("info","Please note that transferring your legacy data can NOT be undone!",ALERT_TYPE_PRIMARY);

		$legacyAccountID = 0;
		$legacyRegistrationDate = null;

		$email = $_POST["email"];
		$password = md5($_POST["password"]);

		$stmt = $mysqli->prepare("SELECT `id`,`registerDate` FROM `mcskinhistory_websiteAccounts` WHERE `email` = ? AND `password` = ?");
		$stmt->bind_param("ss",$email,$password);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows){
			$row = $result->fetch_assoc();

			$legacyAccountID = $row["id"];
			$legacyRegistrationDate = $row["registerDate"];
		}

		$stmt->close();

		if($legacyAccountID > 0 && $legacyRegistrationDate != null){
			if(canBeClaimed($mysqli,$legacyAccountID) == true){
				$uuids = array();

				$stmt = $mysqli->prepare("SELECT `uuid`,`date` FROM `mcskinhistory_linkedAccounts` WHERE `accountID` = ?");
				$stmt->bind_param("i",$legacyAccountID);
				$stmt->execute();
				$result = $stmt->get_result();

				if($result->num_rows){
					while($row = $result->fetch_assoc()){
						$uuids[$row["uuid"]] = $row["date"];
					}
				}

				$stmt->close();

				?>
<div class="card">
	<h5 class="card-header"><?= tr("restoreLegacy.accountInfo.headline") ?></h5>

	<table class="table my-0">
		<tr>
			<td style="width: 50%"><b><?= tr("restoreLegacy.accountInfo.id") ?></b></td>
			<td style="width: 50%">#<?= $legacyAccountID; ?></td>
		</tr>
		<tr>
			<td style="width: 50%"><b><?= tr("restoreLegacy.accountInfo.registerDate") ?></b></td>
			<td style="width: 50%"><?= Util::timeago($legacyRegistrationDate); ?></td>
		</tr>
	</table>
</div>

<?php

if(count($uuids) > 0){
	?>
<div class="card my-2">
	<h5 class="card-header"><?= tr("restoreLegacy.linkedAccounts.headline") ?></h5>

	<table class="table my-0">
		<thead>
			<tr>
				<th><?= tr("restoreLegacy.linkedAccounts.name") ?></th>
				<th><?= tr("restoreLegacy.linkedAccounts.dateLinked") ?></th>
				<th><?= tr("restoreLegacy.linkedAccounts.status") ?></th>
			</tr>
		</thead>
		<tbody>
		<?php

		foreach($uuids as $uuid => $time){
			$player = Player::getPlayer($uuid);

			$status = '<span style="color:green">' . tr("restoreLegacy.linkedAccounts.status.valid") . '</span>';
			if($player->getClaimingAccount() != null && $player->getClaimingAccount()->getID() == Util::getCurrentAccount()->getID()) $status = '<span style="color:orange">' . tr("restoreLegacy.linkedAccounts.status.alreadyClaimed") . '</span>';
			if($player->getClaimingAccount() != null && $player->getClaimingAccount()->getID() != Util::getCurrentAccount()->getID()) $status = '<span style="color:red">' . tr("restoreLegacy.linkedAccounts.status.notClaimable") . '</span>';

			?>
			<tr>
				<td><a href="<?= "/player/" . $player->getUUID(); ?>"><img style="margin-right: 10px" src="https://crafatar.com/avatars/<?= $player->getUUID(); ?>?overlay&size=24&default=MHF_Steve" /><?= $player->getUsername(); ?></a></td>
				<td><?= Util::timeago($time); ?></td>
				<td><?= $status; ?></td>
			</tr>
			<?php
		}

		?>
		</tbody>
	</table>
</div>
	<?php
}

?>
<form action="/account/restoreLegacy" method="post">
	<input type="hidden" name="finish" value="finish"/>
	<input type="hidden" name="email" value="<?= $_POST["email"]; ?>"/>
	<input type="hidden" name="password" value="<?= $_POST["password"]; ?>"/>
	<input type="submit" class="btn btn-primary btn-block" value="Claim"/>
</form>
<?php

			} else {
				Util::createAlert("info",tr("restoreLegacy.alreadyClaimed"),ALERT_TYPE_PRIMARY);
			}
		} else {
			Util::createAlert("info",tr("restoreLegacy.accountNotFound"),ALERT_TYPE_PRIMARY);
		}
	} else if(isset($_POST["finish"]) && isset($_POST["email"]) && isset($_POST["password"]) && !empty($_POST["email"]) && !empty($_POST["password"])){
		$legacyAccountID = 0;
		$legacyRegistrationDate = null;

		$email = $_POST["email"];
		$password = $_POST["password"];

		$stmt = $mysqli->prepare("SELECT `id`,`registerDate` FROM `mcskinhistory_websiteAccounts` WHERE `email` = ? AND `password` = ?");
		$stmt->bind_param("ss",$email,$password);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows){
			$row = $result->fetch_assoc();

			$legacyAccountID = $row["id"];
			$legacyRegistrationDate = $row["registerDate"];
		}

		$stmt->close();

		if($legacyAccountID > 0 && $legacyRegistrationDate != null){
			if(canBeClaimed($mysqli,$legacyAccountID) == true){
				$uuids = array();

				$stmt = $mysqli->prepare("SELECT `uuid`,`date` FROM `mcskinhistory_linkedAccounts` WHERE `accountID` = ?");
				$stmt->bind_param("i",$legacyAccountID);
				$stmt->execute();
				$result = $stmt->get_result();

				if($result->num_rows){
					while($row = $result->fetch_assoc()){
						$uuids[$row["uuid"]] = $row["date"];
					}
				}

				$stmt->close();

				$account->setRegisterDate($legacyRegistrationDate);
				$account->setLegacyAccount($legacyAccountID);

				foreach($uuids as $uuid => $time){
					$player = Player::getPlayer($uuid);

					if($player->getClaimingAccount() == null){
						$uuid = $player->getUUID();
						$uid = $account->getID();

						$stmt = $mysqli->prepare("INSERT INTO `skinhistory_linkedPlayers` (`uuid`,`account`) VALUES(?,?);");
						$stmt->bind_param("si",$uuid,$uid);
						$stmt->execute();
						$stmt->close();

						$account->reloadLinkedAccounts();
						$player->reloadClaimingAccount();
					}
				}

				Util::createAlert("info",tr("restoreLegacy.success"),ALERT_TYPE_SUCCESS);
			} else {
				Util::createAlert("info",tr("restoreLegacy.alreadyClaimed"),ALERT_TYPE_PRIMARY);
			}
		} else {
			Util::createAlert("info",tr("restoreLegacy.accountNotFound"),ALERT_TYPE_PRIMARY);
		}
	} else {
		?>
<div class="card mb-2">
	<h5 class="card-header"><?= tr("restoreLegacy.headline") ?></h5>

	<div class="card-body">
		<p>
			<?= tr("restoreLegacy.text.1") ?><br/><br/>
			<b><?= tr("restoreLegacy.text.2") ?></b>
		</p>
	</div>
</div>

<div class="card">
	<div class="card-body">
		<form action="<?= $app->routeUrl("/account/restoreLegacy"); ?>" method="post">
			<div class="form-group row">
				<label for="email" class="control-label col-sm-2 col-form-label"><?= tr("restoreLegacy.form.email") ?></label>
				<div class="col-sm-10 input-group mb-3">
					<input type="email" name="email" class="form-control" id="email"/>
				</div>
			</div>

			<div class="form-group row">
				<label for="password" class="control-label col-sm-2 col-form-label"><?= tr("restoreLegacy.form.password") ?></label>
				<div class="col-sm-10 input-group mb-3">
					<input type="password" name="password" class="form-control" id="password"/>
				</div>
			</div>

			<button type="submit" class="btn btn-primary btn-block"><?= tr("modalButton.submit") ?></button>
		</form>
	</div>
</div>
		<?php
	}
} else {
	Util::createAlert("info",tr("restoreLegacy.alreadyDoneRestore"),ALERT_TYPE_PRIMARY);
}