<div class="container my-3">
	<h1><?= tr("errorPage.404.headline") ?></h1>
	<p>
		<?= tr("errorPage.404.text") ?>
	</p>
</div>