<?php

use SkinHistory\Account\Favorite;
use SkinHistory\Skin\SkinFile;
use SkinHistory\Util\Util;

?>
<nav class="nav nav-pills nav-justified">
	<a class="nav-item nav-link<?php if($favoriteType == "SKIN") echo " active"; ?>" href="<?= $app->routeUrl("/account/favorites/skins"); ?>"><?= tr("favorites.skins") ?></a>
	<a class="nav-item nav-link<?php if($favoriteType == "CAPE_MOJANG") echo " active"; ?>" href="<?= $app->routeUrl("/account/favorites/mojangCapes"); ?>"><?= tr("favorites.mojang") ?></a>
	<a class="nav-item nav-link<?php if($favoriteType == "CAPE_OPTIFINE") echo " active"; ?>" href="<?= $app->routeUrl("/account/favorites/optifine"); ?>"><?= tr("favorites.optifine") ?></a>
	<a class="nav-item nav-link<?php if($favoriteType == "CAPE_LABYMOD") echo " active"; ?>" href="<?= $app->routeUrl("/account/favorites/labymod"); ?>"><?= tr("favorites.labymod") ?></a>
</nav>

<?php

$currentPage = $page;
$itemsPerPage = 4*4;

Util::getCurrentAccount()->getFavorites();
paginate($page,$favoriteType,$currentPage,$itemsPerPage);

$mysqli = \Database::Instance()->get();
$stmt = $mysqli->prepare("SELECT f.id AS favoriteID, f.time AS favoriteTime, s.id AS skinFileID, s.title AS skinFileTitle, s.md5 AS skinFileMD5, s.full_url AS skinFileFullURL, s.firstUser AS skinFileFirstUser, s.model AS skinFileModel, s.isBanned AS skinFileBanned, s.randomizer AS skinFileRandomizer, s.textureData AS skinFileTextureData, s.time AS skinFileTime FROM skinhistory_favorites AS f INNER JOIN skinhistory_skinfiles AS s ON f.skinFile = s.id WHERE f.account = ? AND s.type = ? ORDER BY favoriteTime DESC LIMIT " . (($currentPage-1)*$itemsPerPage) . " , " . $itemsPerPage);
$uid = Util::getCurrentAccount()->getID();
$stmt->bind_param("is",$uid,$favoriteType);
$stmt->execute();
$result = $stmt->get_result();
$l = $favoriteType == "SKIN" ? "skin" : "cape";
if($result->num_rows){
	echo '<div class="row">';
	while($row = $result->fetch_assoc()){
		$favorite = Favorite::getFavoriteFromData($row["favoriteID"],Util::getCurrentAccount()->getID(),$row["skinFileID"],$row["favoriteTime"]);
		$skin = SkinFile::getSkinFileFromData($row["skinFileID"],$row["skinFileTitle"],$row["skinFileMD5"],$row["skinFileFullURL"],$row["skinFileFirstUser"],$favoriteType,$row["skinFileModel"],$row["skinFileBanned"],$row["skinFileRandomizer"],$row["skinFileTextureData"],$row["skinFileTime"]);

		$title = $skin->getTitle() != null ? $skin->getTitle() : "#" . $skin->getID();
		$img = $skin->getType() == "SKIN" ? "/avatar-3d-custom.php?skinFileID=" . $skin->getID() : "/avatar/cape/" . $skin->getID();

		echo '<div class="col-xl-3 col-lg-4 col-sm-6 col-xs-12" style="padding: 0.25rem !important;">';
		echo '<div class="card text-center">';
		echo '<h5 class="card-header">' . $title;
		Util::renderFavoriteButton($skin->getID(),"right");
		echo '</h5>';
		echo '<a style="text-decoration:none" href="' . $app->routeUrl("/" . $l . "/" . $skin->getID()) . '">';
		echo '<div style="position:relative;padding:5px 0px 5px 0px">';
		echo '<img class="skinShadow" height="204" src="' . $app->routeUrl($img) . '"/>';
		echo '</div>';
		echo '</a>';
		echo '</div>';
		echo '</div>';
	}
	echo '</div>';
}
$stmt->close();

paginate($page,$favoriteType,$currentPage,$itemsPerPage);

function total($favoriteType){
	$cacheName = "totalFavorites_" . Util::getCurrentAccount()->getID() . "_" . $favoriteType;

	if(\CacheHandler::existsInCache($cacheName)){
		return \CacheHandler::getFromCache($cacheName);
	} else {
		$count = -1;
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("SELECT COUNT(*) AS count FROM skinhistory_favorites AS f INNER JOIN skinhistory_skinfiles AS s ON f.skinFile = s.id WHERE f.account = ? AND s.type = ?");
		$uid = Util::getCurrentAccount()->getID();
		$stmt->bind_param("is",$uid,$favoriteType);
		$stmt->execute();
		$result = $stmt->get_result();
		if($result->num_rows){
			$row = $result->fetch_assoc();
			$count = $row["count"];
		}

		$stmt->close();
		\CacheHandler::setToCache($cacheName,$count,60);

		return $count;
	}
}

function paginate($page,$favoriteType,$currentPage,$itemsPerPage){
	$total = total($favoriteType);

	$urlPattern = "";
	if($favoriteType == "SKIN") $urlPattern = "/account/favorites/skins/(:num)";
	if($favoriteType == "CAPE_MOJANG") $urlPattern = "/account/favorites/mojangCapes/(:num)";
	if($favoriteType == "CAPE_OPTIFINE") $urlPattern = "/account/favorites/optifine/(:num)";
	if($favoriteType == "CAPE_LABYMOD") $urlPattern = "/account/favorites/labymod/(:num)";

	$paginator = new JasonGrimes\Paginator($total,$itemsPerPage,$currentPage,$urlPattern);

	if ($paginator->getNumPages() > 1){ ?>
		<nav aria-label="Page navigation example"><ul class="pagination justify-content-center mt-3">
			<?php if ($paginator->getPrevUrl()){ ?>
				<li class="page-item"><a class="page-link" href="<?= $paginator->getPrevUrl(); ?>">&laquo; <?= tr("paginator.prev") ?></a></li>
				<?php } ?>
	
			<?php foreach ($paginator->getPages() as $page){ ?>
				<?php if ($page['url']){ ?>
					<li class="page-item<?= $page['isCurrent'] ? ' active' : ''; ?>">
						<a class="page-link" href="<?= $page['url']; ?>"><?= $page['num']; ?></a>
					</li>
				<?php } else { ?>
					<li class="page-item disabled"><a class="page-link" href="#"><?= $page['num']; ?></a></li>
				<?php } ?>
			<?php } ?>
	
			<?php if ($paginator->getNextUrl()){ ?>
				<li class="page-item"><a class="page-link" href="<?= $paginator->getNextUrl(); ?>"><?= tr("paginator.next") ?> &raquo;</a></li>
				<?php } ?>
		</ul></nav>
				<?php }
}

?>