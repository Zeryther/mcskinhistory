<div class="container my-3">
<?php

use SkinHistory\Player\Player;
use SkinHistory\Util\Util;

$isAvailable = true;

$player = Player::getPlayer($query);
if($player != null){
	$isAvailable = false;
}

if($isAvailable){
	?>
	<div class="alert alert-success text-center" role="alert">
		<h1 style="font-weight: bold"><?= $query; ?></h1>
		<h5><?= tr("namePage.statusHeadline.available") ?></h5>
	</div>
	<?php
} else {
	?>
	<div class="alert alert-primary text-center" role="alert">
		<h1 style="font-weight: bold; color: #b94a48 !important;"><?= $query; ?></h1>
		<h5 style="color: #b94a48 !important;"><?= tr("namePage.statusHeadline.notAvailable") ?></h5>
	</div>
	<?php
}

$results = Util::getNameInfoResults($query);

?>
	<hr/>
	<p class="text-muted"><?= tr("namePage.results",[$results != null ? count($results) : "0"]) ?></p>
<?php

if($results != null && count($results) > 0){
	?>
	<div class="row">
		<div class="col-lg-8">
			<?php

				foreach($results as $holder){
					if($holder == null) continue;
					$holderPlayer = Player::getPlayer($holder->getUUID());
					if($holderPlayer == null) continue;

					?>
			<div class="card mb-3">
				<a href="<?= $app->routeUrl("/player/" . $holderPlayer->getUUID()); ?>">
					<div class="card-body">
						<img src="https://crafatar.com/avatars/<?= $holderPlayer->getUUID(); ?>?overlay" width="64" class="float-left" style="margin-right: 12px"/>
						<h2><?= $holderPlayer->getUsername(); ?></h2>
						<span><?= $holderPlayer->getFormattedUUID(); ?></span>
					</div>
				</a>

				<table class="table my-0">
					<?php

						foreach($holder->getNames() as $name){
							?>
					<tr>
						<td><?php
						
						if(strtolower($query) == strtolower($name->getName())){
							print '<b><a href="' . $app->routeUrl("/name/" . $name->getName()) . '">' . $name->getName() . '</a></b>';
						} else {
							print '<a href="' . $app->routeUrl("/name/" . $name->getName()) . '">' . $name->getName() . '</a>';
						}

						?></td>
						<td><?php
                        
                        if($name->getDate() == null || $name->getDate() == "1970-01-01 00:00:00" || $name->getDate() == "0000-00-00 00:00:00"){
                            echo '<em>' . tr("namePage.originalName") . '</em>';
                        } else {
                            echo Util::timeago($name->getDate());
                        }

                        ?></td>
					</tr>
							<?php
						}

					?>
				</table>
			</div>
					<?php
				}

			?>
		</div>

		<div class="col-lg-4">
			<center>
				<?php
				
				Util::renderNewestTweet();
				Util::renderAd(AD_TYPE_BLOCK);
				
				?>
            </center>
		</div>
	<?php
}

?>
</div>