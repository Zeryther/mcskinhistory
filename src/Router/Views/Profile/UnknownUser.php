<div class="container my-3">
	<div class="alert alert-primary text-center" role="alert">
		<h1 style="font-weight: bold; color: #b94a48 !important;"><?= $query; ?></h1>
		<h5 style="color: #b94a48 !important;"><?= tr("profile.invalidAccount") ?></h5>
	</div>
</div>