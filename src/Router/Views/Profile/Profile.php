<?php

use SkinHistory\Player\Player;
use SkinHistory\Util\DisqusThread;
use SkinHistory\Util\Util;

if(count($player->getTextures()) > 0){
	?>
<script type="text/javascript">
	<?php

		foreach($player->getTextures() as $texture){
			?>
(new Image()).src = "<?= Util::proxyImage($texture->getSkin()->getFullURL()); ?>";

			<?php
		}

	?>
</script>
	<?php
}

?><div class="container my-3">
	<h1><?= $player->getUsername(); ?></h1>

	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<div class="skinContainer" id="skinContainer" style="text-align: center; height: 510px">
						<!--<img style="height: 360px; padding-top: 50%" id="placeholder" src="<?= $app->baseUrl("assets:img/minerender_steve_gray_blur.png"); ?>"/>-->
					</div>
				</div>
			</div>

			<center>
				<?php

				if($player->getClaimingAccount() == null){
					?>
				<a class="clearUnderline" href="<?= $app->routeUrl("/account/linked?name=" . $player->getUsername()); ?>">
					<button type="button" class="my-2 btn btn-danger btn-block btn-lg"><?= tr("profile.claimButton") ?></button>
				</a>
					<?php
				} else {
					if(count($player->getClaimingAccount()->getLinkedAccounts()) > 1){
						?>
				<div class="card text-center my-2">
					<h5 class="card-header">Other accounts</h5>

					<div class="card-body">
						<?php

							foreach($player->getClaimingAccount()->getLinkedAccounts() as $account){
								$linked = Player::getPlayer($account["uuid"]);
								if($linked != null && $linked->getUUID() != $player->getUUID()){
									?>
						<a href="<?= $app->routeUrl("/player/" . $linked->getUUID()); ?>" class="clearUnderline" data-toggle="tooltip" title="<?= $linked->getUsername(); ?>">
							<img style="margin-left: 5px; margin-right: 5px" src="https://crafatar.com/avatars/<?= $linked->getUUID(); ?>?overlay&size=32&default=MHF_Steve"/>
						</a>
									<?php
								}
							}

						?>
					</div>
				</div>
						<?php
					}
				}

				if($player->getCurrentSkinAsSkinFile() != null){
				?>
				<a class="clearUnderline" href="<?= $app->routeUrl("/download/" . $player->getCurrentSkinAsSkinFile()->getID()); ?>" id="downloadSkinButton">
					<button type="button" class="my-2 btn btn-danger btn-block btn-lg"><?= tr("skin.downloadButton") ?></button>
				</a>
				<?php
				}
				
				Util::renderNewestTweet();
				Util::renderAd(AD_TYPE_BLOCK);
				
				?>
			</center>
			
			<center class="mt-3">
				<iframe src="https://www.instant-gaming.com/affgames/igr1710165/350x350" scrolling="no" frameborder="0" style="border: 1px solid #ccc; border-radius: 5px; overflow:hidden; width:350px; height:350px;" allowTransparency="true"></iframe>
			</center>
		</div>

		<div class="col-md-8">
			<div class="card">
				<h5 class="card-header">Information</h5>

				<table class="table my-0">
					<tr>
						<td style="width: 50%"><b><?= tr("profile.uuid") ?></b></td>
						<td style="width: 50%"><span id="uuid"><?= $player->getUUID(); ?></span> <a class="float-right copyclip" data-clipboard-target="#uuid" href="#"><i class="fas fa-copy"></i></a></td>
					</tr>

					<tr>
						<td style="width: 50%"><b><?= tr("profile.uuid.separated") ?></b></td>
						<td style="width: 50%"><span id="formattedUUID"><?= $player->getFormattedUUID(); ?></span> <a class="float-right copyclip" data-clipboard-target="#formattedUUID" href="#"><i class="fas fa-copy"></i></a></td>
					</tr>

					<tr>
						<td style="width: 50%"><b><?= tr("profile.firstView") ?></b></td>
						<td style="width: 50%"><?= Util::timeago($player->getFoundTime()); ?></td>
					</tr>

					<?php

						if(count($player->getSocialMedia()) > 0){
							$socialMediaString = "";

							foreach($player->getSocialMedia() as $name => $value){
								if($value == null || empty($value)) continue;

								if($name == "beam"){
									$url = "https://mixer.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<img style="margin-top: -10px" src="' . $app->routeUrl("assets/img/MixerMerge_Light.png") . '" height="18"/>';
									$socialMediaString .= '</a>';
								} else if($name == "facebook"){
									$url = "https://facebook.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #3B5998; font-size: 24px" class="fab fa-facebook-square"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "github"){
									$url = "https://github.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #000; font-size: 24px" class="fab fa-github"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "googleplus"){
									$url = "https://plus.google.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #DE5E53; font-size: 24px" class="fab fa-google-plus-square"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "instagram"){
									$url = "https://instagram.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #D9447C; font-size: 24px" class="fab fa-instagram"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "reddit"){
									$url = "https://reddit.com/user/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #FF4500; font-size: 24px" class="fab fa-reddit"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "soundcloud"){
									$url = "https://soundcloud.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #FF5100; font-size: 24px" class="fab fa-soundcloud"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "steam"){
									$url = "https://steamcommunity.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #010002; font-size: 24px" class="fab fa-steam"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "twitch"){
									$url = "https://twitch.tv/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #6441A4; font-size: 24px" class="fab fa-twitch"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "twitter"){
									$url = "https://twitter.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #1DA1F2; font-size: 24px" class="fab fa-twitter-square"></i>';
									$socialMediaString .= '</a>';
								} else if($name == "youtube"){
									$url = "https://youtube.com/" . $value;
									$socialMediaString .= '<a href="' . $url . '" target="_blank" class="ml-1 mr-1">';
									$socialMediaString .= '<i style="color: #FF0000; font-size: 24px" class="fab fa-youtube-square"></i>';
									$socialMediaString .= '</a>';
								}
							}

							if($socialMediaString != ""){

							?>
					<tr>
						<td style="width: 50%"><b><?= tr("profile.socialMedia") ?></b></td>
						<td style="width: 50%"><?= $socialMediaString; ?></td>
					</tr>
							<?php
							}
						}

					?>
				</table>
			</div>

			<?php if(!empty($player->getNameHistory()) && count($player->getNameHistory()) > 0){ ?>
			<div class="card my-3">
				<h5 class="card-header"><?= tr("profile.nameHistory") ?></h5>

				<div style="max-height: 320px; overflow-y: scroll;">
					<table class="table my-0">
						<tr>
							<th style="width: 50"><?= tr("profile.part.name") ?></th>
							<th style="width: 50%"><?= tr("profile.part.dateAdded") ?></th>
						</tr>
						<?php

							foreach($player->getNameHistory() as $nameData){
								$nameHistoryDate = $nameData->getDate();
								if($nameHistoryDate != 0){
									$nameHistoryDate = $nameHistoryDate/1000;
									$nameHistoryDate = gmdate("Y-m-d H:i:s",$nameHistoryDate);
								}
								?>
						<tr>
							<td style="width: 50%"><?php 
							
							$name = $nameData->getName();
							if(strtolower($name) == strtolower($player->getUsername())){
								?>
							<a href="<?= $app->routeUrl("/name/" . $name); ?>"><b><?= $name; ?></b></a>
								<?php
							} else {
								?>
							<a href="<?= $app->routeUrl("/name/" . $name); ?>"><?= $name; ?></a>
								<?php
							}

							?></td>
							<td style="width: 50%"><?php

								if($nameHistoryDate == 0){
									print "<em>" . tr("namePage.originalName") . "</em>";
								} else {
									print Util::timeago($nameHistoryDate);
								}

							?></td>
						</tr>
								<?php
							}

						?>
					</table>
				</div>
			</div>
			<?php }

			echo '<center>';
			Util::renderAd(AD_TYPE_LEADERBOARD);
			echo '</center>';
			
			$textures = $player->getTextures("SKIN");
			if(!empty($textures) && count($textures) > 0){ ?>
			<div class="card my-3">
				<h5 class="card-header"><?= tr("profile.skins") ?></h5>

				<div style="max-height: 320px; overflow-y: scroll;">
					<table class="table my-0 skinFileTable">
						<tr>
							<th style="width: 50%"><?= tr("profile.part.name") ?></th>
							<th style="width: 50%"><?= tr("profile.part.dateAdded") ?></th>
						</tr>
						<?php

							foreach($textures as $texture){
								$skinFile = $texture->getSkin();
								$current = $player->getCurrentSkinAsSkinFile();
								$d = "";

								if($current != null && $current->getID() == $skinFile->getID()){
									$d = ' <em style="color:gray;">(' . tr("profile.skins.current") . ')</em>';
								}
								?>
						<tr id="skinFile-<?= $skinFile->getID(); ?>" onmouseover="setActiveSkin(<?= $skinFile->getID(); ?>,'<?= Util::proxyImage($skinFile->getFullURL()); ?>');">
							<td style="width: 50%">
								<a href="<?= $app->routeUrl("/skin/" . $skinFile->getID()); ?>"><img class="face" src="<?= $app->routeUrl("/avatar/skin/face/" . $skinFile->getID()); ?>" /><?= $skinFile->getTitle() != null ? $skinFile->getTitle() : "#" . $skinFile->getID(); ?></a><?= $d; ?>
							</td>

							<td style="width: 50%">
								<?= Util::timeago($texture->getTime()); ?>
							</td>
						</tr>
								<?php
							}

						?>
					</table>
				</div>
			</div>
			<?php }

			$textures = $player->getTextures("CAPE_MOJANG");
			if(!empty($textures) && count($textures) > 0){ ?>
			<div class="card my-3">
				<h5 class="card-header"><?= tr("profile.mojangCapes") ?></h5>

				<div style="max-height: 320px; overflow-y: scroll;">
					<table class="table my-0 skinFileTable">
						<tr>
							<th style="width: 50%"><?= tr("profile.part.name") ?></th>
							<th style="width: 50%"><?= tr("profile.part.dateAdded") ?></th>
						</tr>
						<?php

							foreach($textures as $texture){
								$skinFile = $texture->getSkin();
								?>
						<tr id="skinFile-<?= $skinFile->getID(); ?>" onmouseover="setActiveCape(<?= $skinFile->getID(); ?>,'<?= Util::proxyImage($skinFile->getFullURL()); ?>');">
							<td style="width: 50%">
								<a href="<?= $app->routeUrl("/cape/" . $skinFile->getID()); ?>"><img class="cape" src="<?= $app->routeUrl("/avatar/cape/" . $skinFile->getID()); ?>" /><?= $skinFile->getTitle() != null ? $skinFile->getTitle() : "#" . $skinFile->getID(); ?></a>
							</td>

							<td style="width: 50%">
								<?= Util::timeago($texture->getTime()); ?>
							</td>
						</tr>
								<?php
							}

						?>
					</table>
				</div>
			</div>
			<?php }

			$textures = $player->getTextures("CAPE_OPTIFINE");
			if(!empty($textures) && count($textures) > 0){ ?>
			<div class="card my-3">
				<h5 class="card-header"><?= tr("profile.optifineCapes") ?></h5>

				<div style="max-height: 320px; overflow-y: scroll;">
					<table class="table my-0 skinFileTable">
						<tr>
							<th style="width: 50%"><?= tr("profile.part.name") ?></th>
							<th style="width: 50%"><?= tr("profile.part.dateAdded") ?></th>
						</tr>
						<?php

							foreach($textures as $texture){
								$skinFile = $texture->getSkin();
								?>
						<tr id="skinFile-<?= $skinFile->getID(); ?>" onmouseover="setActiveCape(<?= $skinFile->getID(); ?>,'<?= Util::proxyImage($skinFile->getFullURL()); ?>');">
							<td style="width: 50%">
								<a href="<?= $app->routeUrl("/cape/" . $skinFile->getID()); ?>"><img class="cape" src="<?= $app->routeUrl("/avatar/cape/" . $skinFile->getID()); ?>" /><?= $skinFile->getTitle() != null ? $skinFile->getTitle() : "#" . $skinFile->getID(); ?></a>
							</td>

							<td style="width: 50%">
								<?= Util::timeago($texture->getTime()); ?>
							</td>
						</tr>
								<?php
							}

						?>
					</table>
				</div>
			</div>
			<?php }

			$textures = $player->getTextures("CAPE_LABYMOD");
			if(!empty($textures) && count($textures) > 0){ ?>
			<div class="card my-3">
				<h5 class="card-header"><?= tr("profile.labymodCapes") ?></h5>

				<div style="max-height: 320px; overflow-y: scroll;">
					<table class="table my-0 skinFileTable">
						<tr>
							<th style="width: 50%"><?= tr("profile.part.name") ?></th>
							<th style="width: 50%"><?= tr("profile.part.dateAdded") ?></th>
						</tr>
						<?php

							foreach($textures as $texture){
								$skinFile = $texture->getSkin();
								?>
						<tr id="skinFile-<?= $skinFile->getID(); ?>" onmouseover="setActiveCape(<?= $skinFile->getID(); ?>,'<?= Util::proxyImage($skinFile->getFullURL()); ?>');">
							<td style="width: 50%">
								<a href="<?= $app->routeUrl("/cape/" . $skinFile->getID()); ?>"><img class="cape" src="<?= $app->routeUrl("/avatar/cape/" . $skinFile->getID()); ?>" /><?= $skinFile->getTitle() != null ? $skinFile->getTitle() : "#" . $skinFile->getID(); ?></a>
							</td>

							<td style="width: 50%">
								<?= Util::timeago($texture->getTime()); ?>
							</td>
						</tr>
								<?php
							}

						?>
					</table>
				</div>
			</div>
			<?php }

				echo '<center>';
				Util::renderAd(AD_TYPE_LEADERBOARD);
				echo '</center>';

			?>

			<div class="card">
				<div class="card-body">
					<?php
					
						$disqus = new DisqusThread();
						
						$disqus->setPageURL("https://mcskinhistory.com/player/" . $player->getUUID());
						$disqus->setPageIdentifier("mcskinhistory_playerProfile_" . $player->getUUID());

						$disqus->render();

					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var updating = true;

	var element = document.getElementById("skinContainer");
	var skinRender = new SkinRender({
                autoResize:true,
				canvas: {
					width: element.offsetWidth,
                    height: element.offsetHeight
				}
            }, element);

	skinRender.render({"uuid": "<?= $player->getUUID(); ?>"},function(){updating = false;});

	function clearRender(){
		while(element.firstChild)
			element.removeChild(element.firstChild);
	}

	var currentSkinURL = null;
	var currentCapeURL = null;

	function updateRenderSkinFile(){
		if(updating == true){
			return;
		}

		updating = true;

		skinRender.clearScene();
		var skinURL = currentSkinURL;
		var capeURL = currentCapeURL;

		//clearRender();

		var options = null;
		if(skinURL == null){
			if(capeURL != null){
				options = {
					"uuid": "<?= $player->getUUID(); ?>",
					"capeUrl": capeURL,
					"optifine": true
				};
			} else {
				options = {
					"uuid": "<?= $player->getUUID(); ?>",
					"optifine": true
				};
			}
		} else {
			if(capeURL != null){
				options = {
					"url": skinURL,
					"capeUrl": capeURL,
					"optifine": true
				};
			} else {
				options = {
					"url": skinURL,
					"optifine": true
				};
			}
		}

		skinRender.render(options,function(){updating = false;});
	}

	var activeSkinID = 0;
	var activeCapeID = 0;

	function setActiveSkin(id,url){
		if(updating == true){
			return;
		}

		if(activeSkinID != id){
			if(activeSkinID != 0){
				document.getElementById("skinFile-" + activeSkinID).classList.remove("active");
			}

			activeSkinID = id;
			document.getElementById("skinFile-" + id).classList.add("active");
			currentSkinURL = url;
			if(typeof updateRenderSkinFile !== "undefined") updateRenderSkinFile();
			if(typeof updateDownloadSkinButton !== "undefined") updateDownloadSkinButton();
		}
	}

	function setActiveCape(id,url){
		if(updating == true){
			return;
		}

		if(activeCapeID != id){
			if(activeCapeID != 0){
				document.getElementById("skinFile-" + activeCapeID).classList.remove("active");
			}

			activeCapeID = id;
			document.getElementById("skinFile-" + id).classList.add("active");
			currentCapeURL = url;
			if(typeof updateRenderSkinFile !== "undefined") updateRenderSkinFile();
		}
	}

	<?php if($player->getCurrentSkinAsSkinFile() != null){ ?>
	function updateDownloadSkinButton(){
		var downloadSkinButton = document.getElementById("downloadSkinButton");

		if(activeSkinID == 0){
			downloadSkinButton.href = "<?= $app->routeUrl("/download/" . $player->getCurrentSkinAsSkinFile()->getID()); ?>";
		} else {
			downloadSkinButton.href = "<?= $app->routeUrl("/download/"); ?>" + activeSkinID;
		}
	}
	<?php } ?>
</script>