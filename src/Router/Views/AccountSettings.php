<?php

use SkinHistory\Util\Util;

$account = Util::getCurrentAccount();
if($account->getLegacyAccount() == null) Util::createAlert("linkedAccountsLegacyInfo","If you registered before May 11th 2018, click <a href=\"" . $app->routeUrl("/account/restoreLegacy") . "\">here</a> for information on how to retrieve your old account&apos;s data.",ALERT_TYPE_INFO,true,true);
?>
<div class="card">
	<h5 class="card-header"><?= tr("account.myAccount.headline") ?></h5>

	<table class="table my-0">
		<tr>
			<td style="width: 50%"><b><?= tr("account.myAccount.id") ?></b></td>
			<td style="width: 50%">#<?= $account->getID(); ?></td>
		</tr>

		<tr>
			<td style="width: 50%"><b><?= tr("account.myAccount.email") ?></b></td>
			<td style="width: 50%"><?= $account->getEmail(); ?></td>
		</tr>

		<tr>
			<td style="width: 50%"><b><?= tr("account.myAccount.registerDate") ?></b></td>
			<td style="width: 50%"><?= Util::timeago($account->getRegisterDate()); ?></td>
		</tr>

		<tr>
			<td style="width: 50%">&nbsp;</td>
			<td style="width: 50%"><b><?= tr("account.myAccount.visitGigadrive",['<a href="https://gigadrivegroup.com/account" target="_blank">gigadrivegroup.com/account</a>']) ?></b></td>
		</tr>
	</table>
</div>