<div class="container my-3">
<?php

use SkinHistory\Skin\SkinFileList;
use SkinHistory\Util\Util;

Util::createAlert("yoshinoAdvertisment","<b>" . tr("alert.yoshinoAdvertisment") . "</b>",ALERT_TYPE_INFO,true,true);
Util::createAlert("minecraftEditionInformation","<b>" . tr("alert.minecraftEditionInformation") . "</b>",ALERT_TYPE_PRIMARY,true,true);
?>
	<h1>
		<?= tr("fileList.labymod.popular.headline") ?>
	</h1>
<?php

	$skinFileList = new SkinFileList();

	$skinFileList->setPagination(false);
	$skinFileList->setType("CAPE_LABYMOD");
	$skinFileList->setListType(SKIN_FILE_LIST_TYPE_POPULAR);
	$skinFileList->setURLPattern($app->routeUrl("/capes/labymod/popular"));
	if(isset($page)) $skinFileList->setCurrentPage($page);

	$skinFileList->render();

?>
</div>