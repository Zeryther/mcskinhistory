<div class="container my-3">
<?php

use SkinHistory\Skin\SkinFileList;
use SkinHistory\Util\Util;

Util::createAlert("yoshinoAdvertisment","<b>" . tr("alert.yoshinoAdvertisment") . "</b>",ALERT_TYPE_INFO,true,true);
Util::createAlert("minecraftEditionInformation","<b>" . tr("alert.minecraftEditionInformation") . "</b>",ALERT_TYPE_PRIMARY,true,true);
?>
	<h1>
		<?= tr("fileList.mojang.browse.headline") ?>
	</h1>
<?php

	$skinFileList = new SkinFileList();

	$skinFileList->setPagination(true);
	$skinFileList->setType("CAPE_MOJANG");
	$skinFileList->setListType(SKIN_FILE_LIST_TYPE_POPULAR);
	$skinFileList->setURLPattern($app->routeUrl("/capes/mojang/browse"));
	if(isset($page)) $skinFileList->setCurrentPage($page);

	$skinFileList->render();

?>
</div>