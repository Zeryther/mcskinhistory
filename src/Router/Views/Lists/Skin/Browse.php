<div class="container my-3">
<?php

use SkinHistory\Skin\SkinFileList;
use SkinHistory\Util\Util;

Util::createAlert("yoshinoAdvertisment","<b>" . tr("alert.yoshinoAdvertisment") . "</b>",ALERT_TYPE_INFO,true,true);
Util::createAlert("minecraftEditionInformation","<b>" . tr("alert.minecraftEditionInformation") . "</b>",ALERT_TYPE_PRIMARY,true,true);
?>
	<h1>
		<?= tr("fileList.skin.browse.headline",[$page]) ?>
	</h1>
<?php

	$skinFileList = new SkinFileList();

	$skinFileList->setPagination(true);
	$skinFileList->setType("SKIN");
	$skinFileList->setListType(SKIN_FILE_LIST_TYPE_BROWSE);
	$skinFileList->setURLPattern($app->routeUrl("/skins/browse/(:num)"));
	if(isset($page)) $skinFileList->setCurrentPage($page);

	$skinFileList->render();

?>
</div>