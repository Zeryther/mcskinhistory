<?php

use SkinHistory\Util\DisqusThread;
use SkinHistory\Util\Util;

?><div class="container my-3">
	<h1><?= tr("skin.headline",[$skin->getTitle() != null ? $skin->getTitle() : "#" . $skin->getID()]) ?></h1>

	<div class="row">
		<div class="col-md-4">
			<div class="card">
				<div class="card-body">
					<span class="float-right"><?php Util::renderFavoriteButton($skin->getID()); ?></span>
					<div class="skinContainer" id="skinContainer" style="text-align: center; height: 510px">
						<!--<img style="height: 360px; padding-top: 50%" id="placeholder" src="<?= $app->baseUrl("assets:img/minerender_steve_gray_blur.png"); ?>"/>-->
					</div>
				</div>
			</div>

			<center>
				<a class="clearUnderline" href="<?= $app->routeUrl("/download/" . $skin->getID()); ?>">
					<button type="button" class="my-2 btn btn-danger btn-block btn-lg"><?= tr("skin.downloadButton") ?></button>
				</a>

				<a class="clearUnderline" href="https://minecraft.net/profile/skin/remote?url=<?= $skin->getFullURL(); ?>">
					<button type="button" class="my-2 btn btn-success btn-block btn-lg"><?= tr("skin.changeButton") ?></button>
				</a>
				<?php
				
				Util::renderNewestTweet();
				if($skin->isBanned() == false) Util::renderAd(AD_TYPE_BLOCK);
				
				?>
            </center>

			<center class="mt-3">
				<iframe src="https://www.instant-gaming.com/affgames/igr1710165/350x350" scrolling="no" frameborder="0" style="border: 1px solid #ccc; border-radius: 5px; overflow:hidden; width:350px; height:350px;" allowTransparency="true"></iframe>
			</center>
		</div>

		<div class="col-md-8">
			<div class="card mb-3">
				<h5 class="card-header"><?= tr("skinFilePage.information.headline") ?></h5>

				<table class="table my-0">
					<tr>
						<td style="width: 50%"><b><?= tr("skinFilePage.information.id") ?></b></td>
						<td style="width: 50%">#<?= $skin->getID(); ?></td>
					</tr>

					<tr>
						<td style="width: 50%"><b><?= tr("skin.information.model") ?></b></td>
						<td style="width: 50%"><?php
						
						if($skin->getModel() == "STEVE"){
							echo tr("skin.information.model.steve");
						} else if($skin->getModel() == "ALEX"){
							echo tr("skin.information.model.alex");
						}
						
						?></td>
					</tr>

					<tr>
						<td style="width: 50%"><b><?= tr("skinFilePage.information.dateAdded") ?></b></td>
						<td style="width: 50%"><?= Util::timeago($skin->getTime()); ?></td>
					</tr>

					<tr>
						<td style="width: 50%"><b><?= tr("skinFilePage.information.firstUser") ?></b></td>
						<td style="width: 50%"><a href="<?= $app->routeUrl("/player/" . $skin->getFirstUser()->getUUID()); ?>"><?= $skin->getFirstUser()->getUsername(); ?></a></td>
					</tr>
				</table>
			</div>

			<?php if($skin->isBanned() == false) Util::renderAd(AD_TYPE_LEADERBOARD); ?>

			<div class="card my-3">
				<h5 class="card-header"><?= tr("skin.users.headline") ?></h5>

				<div class="card-body text-justify" style="max-height: 320px; overflow-y: scroll;">
					<?php

						$users = array();
						$cacheName = "skinUsers_" . $skin->getID();
						$skinID = $skin->getID();
						if(\CacheHandler::existsInCache($cacheName)){
							$users = \CacheHandler::getFromCache($cacheName);
						} else {
							$mysqli = \Database::Instance()->get();
							$stmt = $mysqli->prepare("SELECT p.uuid,p.username FROM `skinhistory_textures` AS t INNER JOIN `skinhistory_players` AS p ON t.uuid = p.uuid WHERE t.skinFile = ? ORDER BY p.username ASC");
							$stmt->bind_param("i",$skinID);
							$stmt->execute();
							$stmt->bind_result($pUUID,$pNAME);

							while($stmt->fetch()){
								$users[$pUUID] = $pNAME;
							}

							$stmt->close();

							\CacheHandler::setToCache($cacheName,$users,30*60);
						}

						if(count($users) > 0){
							$s = "";
							foreach($users as $uuid => $name){
								$f = $uuid == $skin->getFirstUser()->getUUID() ? "<b>" : "";
								$l = $uuid == $skin->getFirstUser()->getUUID() ? "</b>" : "";

								if($s == ""){
									$s = '<a href="' . $app->routeUrl("/player/" . $uuid) . '">' . $f . $name . $l . '</a>';
								} else {
									$s .= ', <a href="' . $app->routeUrl("/player/" . $uuid) . '">' . $f . $name . $l . '</a>';
								}
							}

							echo $s;
						} else {
							echo tr("skin.users.noUsers");
						}

					?>
				</div>
			</div>



			<?php if($skin->isBanned() == false) Util::renderAd(AD_TYPE_LEADERBOARD); ?>

			<?php

				$related = array();
				$cacheName = "related_" . $skin->getID();
				if(\CacheHandler::existsInCache($cacheName)){
					$related = \CacheHandler::getFromCache($cacheName);
				} else {
					if(count($users) > 0){
						$a = array();
						foreach($users as $uuid => $name){
							array_push($a,$uuid);
						}

						$d = implode("','",$a);
						$skinID = $skin->getID();
						$skinType = $skin->getType();

						$mysqli = \Database::Instance()->get();
						$stmt = $mysqli->prepare("SELECT s.id FROM `skinhistory_skinfiles` AS s INNER JOIN `skinhistory_textures` AS t ON t.skinFile = s.id WHERE t.uuid IN (?) AND t.skinFile != ? AND s.type = ? AND s.isBanned = 0 ORDER BY RAND() LIMIT 6");
						$stmt->bind_param("sis",$d,$skinID,$skinType);
						$stmt->execute();
						$stmt->bind_result($sid);

						while($stmt->fetch()){
							if(!in_array($sid,$related)) array_push($related,$sid);
						}

						$stmt->close();

						\CacheHandler::setToCache($cacheName,$related,10*60);
					}
				}

				if(count($related) > 0){
					?>
			<div class="card my-3">
				<h5 class="card-header"><?= tr("skin.related") ?></h5>

				<div class="card-body text-center">
					<?php
						
						foreach($related as $skinFileID){
							?>
						<a href="<?= $app->routeUrl("/skin/" . $skinFileID); ?>">
							<img class="skinShadow" src="<?= $app->routeUrl("/avatar-3d-custom.php?skinFileID=" . $skinFileID); ?>" alt="" height="120"/>
						</a>
							<?php
						}

					?>
				</div>
			</div>
					<?php
				}

			?>

			<div class="card">
				<div class="card-body">
					<?php
					
						$disqus = new DisqusThread();
						
						$disqus->setPageURL("https://mcskinhistory.com/skin/" . $skin->getID());
						$disqus->setPageIdentifier("mcskinhistory_skin_" . $skin->getID());

						$disqus->render();

					?>
				</div>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
	var element = document.getElementById("skinContainer");

	function clearRender(){
		while(element.firstChild)
			element.removeChild(element.firstChild);
	}

	$(document).ready(function () {
		clearRender();

		var skinRender = new SkinRender({
                autoResize:true,
				canvas: {
					width: element.offsetWidth,
                    height: element.offsetHeight
				}
            }, element).render({ "url": "<?= Util::proxyImage($skin->getFullURL()); ?>" });
	});
</script>