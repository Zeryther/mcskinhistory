<?php

use SkinHistory\Util\Util;

?><!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://ogp.me/ns/fb#" lang="<?= substr(\Gigadrive\i18n\i18n::getCurrentLanguage()->getCode(),0,2) ?>">
	<head>
		<title><?= $title . " - " . $app["config.site/name"]; ?></title>

		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
		<meta http-equiv="x-ua-compatible" content="ie=edge"/>
		<meta name="apple-mobile-web-app-capable" content="yes">

		<meta name="og:site_name" content="MCSkinHistory" />
		<meta name="og:title" content='<?= $title; ?>' />
		<meta name="og:description" content="<?= $description; ?>" />
		<meta name="og:locale" content="<?= substr(\Gigadrive\i18n\i18n::getCurrentLanguage()->getCode(),0,2) ?>" />

		<meta name="twitter:title" content='<?= $title; ?>' />
		<meta name="twitter:description" content="<?= $description; ?>" />
		<meta name="twitter:image" content="<?= $twitterImage; ?>" />
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@mcskinhistory" />

		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
		<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
		<link rel="manifest" href="/site.webmanifest">
		<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
		<meta name="msapplication-TileColor" content="#da532c">
		<meta name="theme-color" content="#DC660E">

		<noscript><meta http-equiv="refresh" content="0; URL=https://gigadrivegroup.com/badbrowser"></noscript>

		<meta name="description" content="<?= $description; ?>" />
		<meta name="keywords" content="minecraft,skin,history,mc,get,old,skin,labymod,optifine,name,lookup,tool,uuid,database" />

		<link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.css" />
		<script src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/3.0.3/cookieconsent.min.js"></script>

		<link href="https://cdnjs.cloudflare.com/ajax/libs/flag-icon-css/0.8.2/css/flag-icon.min.css" rel="stylesheet"/>

		<link rel="search" type="application/opensearchdescription+xml" title="MCSkinHistory" href="/opensearch.xml" />
		<link rel="canonical" href="https://mcskinhistory.com/"/>
		<!--<script src="https://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css" integrity="sha384-+d0P83n9kaQMCwj8F4RJB66tzIwOKmrdb46+porD/OvrJ+37WqIM7UoBtwHO6Nlg" crossorigin="anonymous">
		<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js" integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4=" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/three.js/87/three.min.js" integrity="sha256-ZJ8+dqHwDpFyWtPH4vQ3/FRCPJiVTwpsMQxDIDLtsHY=" crossorigin="anonymous"></script>
		<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
		<script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
		<script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
		<script src="https://skinrender.ga/js/lib/OrbitControls.js"></script>
		<?= $app->style(array(
				"assets:css/bootstrap.min.css",
				"assets:css/main.css"
			)); ?>
		<?= $app->script(array(
				"assets:js/jquery.timeago.js",
				"assets:js/blockadblock.js",
				"assets:js/main.js"
			)); ?>
		<script src="https://minerender.org/dist/skin.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.0/clipboard.min.js"></script>

		<script type="text/javascript">
			window.addEventListener("load", function(){
				window.cookieconsent.initialise({
					"palette": {
						"popup": {
						"background": "#343c66",
						"text": "#cfcfe8"
						},
						"button": {
						"background": "#f71559"
						}
					},
					"theme": "classic",
					"position": "bottom-right",
					"content": {
						"href": "https://gigadrivegroup.com/legal/privacy-policy"
					}
				})
			});

			$(document).ready(function () {
				jQuery("time.timeago").timeago();
				new ClipboardJS('.copyclip');
				jQuery('[data-toggle="tooltip"]').tooltip({
					trigger: 'hover'
				});
			});
		</script>

		<?php

		$c = "assets:js/" . \Gigadrive\i18n\i18n::getCurrentLanguage()->getTimeAgoFileName();
		$f = realpath($app->path($c));


		if(file_exists($f)){
			echo $app->script(array(
				$c
			));
		}

		?>

		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-57891578-4"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());

			gtag('config', 'UA-57891578-4');
		</script>

		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<script>
			(adsbygoogle = window.adsbygoogle || []).push({
				google_ad_client: "ca-pub-6156128043207415",
				enable_page_level_ads: true
			});
		</script>
	</head>

	<body>
		<?php if(!isset($_COOKIE["ignoreAdblock"]) || $_COOKIE["ignoreAdblock"] != "true"){ ?>
		<div class="modal fade" id="adBlockModal" tabindex="-1" role="dialog" aria-labelledby="adBlockModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="adBlockModalLabel">Please disable your Adblocker.</h5>

						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>

					<div class="modal-body">
						We know ads can be annoying, but hosting all of these skins and capes is slowly becoming impossible.<br/>
						So, <b>please consider disabling your ad blocking software so we can keep this website running!</b><br/><br/><br/>

						<a href="https://patreon.com/Zeryther" target="_blank">
							<b>
								<u>
									Otherwise, please consider becoming a patron and support this website actively with a monthly subscription!
								</u>
							</b>
						</a>
					</div>

					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">No, show me the page</button>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>

		<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
			<div class="container">
				<a class="navbar-brand" href="<?= $app->routeUrl("/"); ?>"><img src="<?= $app->baseUrl("assets:img/MCSkinHistory-logo-white-small.png"); ?>" height="25"/></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarColor01" aria-controls="navbarColor01" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>

				<div class="collapse navbar-collapse" id="navbarColor01">
					<ul class="navbar-nav mr-auto">
						<!-- HOME -->
						<li class="nav-item<?php if(isset($nav) && $nav == NAV_HOME){ ?> active<?php } ?>">
							<a class="nav-link" href="<?= $app->routeUrl("/"); ?>"><?= tr("navigation.home") ?><?php if(isset($nav) && $nav == NAV_HOME){ ?> <span class="sr-only"><?= tr("navigation.current") ?></span><?php } ?></a>
						</li>

						<!-- SKINS -->
						<li class="nav-item dropdown<?php if(isset($nav) && $nav == NAV_SKINS){ ?> active<?php } ?>">
							<a href="#" class="nav-link dropdown-toggle" id="skinsDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<?= tr("navigation.skins") ?><?php if(isset($nav) && $nav == NAV_SKINS){ ?> <span class="sr-only"><?= tr("navigation.current") ?></span><?php } ?>
							</a>

							<div class="dropdown-menu" aria-labelledBy="skinsDropdown">
								<a href="<?= $app->routeUrl("/skins/browse"); ?>" class="dropdown-item"><i class="fas fa-search"></i> <?= tr("navigation.skins.browse") ?></a>
								<a href="<?= $app->routeUrl("/skins/new"); ?>" class="dropdown-item"><i class="fas fa-star"></i> <?= tr("navigation.skins.new") ?></a>
								<a href="<?= $app->routeUrl("/skins/popular"); ?>" class="dropdown-item"><i class="fas fa-thumbs-up"></i> <?= tr("navigation.skins.popular") ?></a>
							</div>
						</li>

						<!-- CAPES -->
						<li class="nav-item dropdown<?php if(isset($nav) && $nav == NAV_CAPES){ ?> active<?php } ?>">
							<a href="#" class="nav-link dropdown-toggle" id="capesDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<?= tr("navigation.capes") ?><?php if(isset($nav) && $nav == NAV_CAPES){ ?> <span class="sr-only"><?= tr("navigation.current") ?></span><?php } ?>
							</a>

							<div class="dropdown-menu" aria-labelledBy="capesDropdown">
								<h6 class="dropdown-header">Mojang</h6>
								<a href="<?= $app->routeUrl("/capes/mojang/browse"); ?>" class="dropdown-item"><i class="fas fa-search"></i> <?= tr("navigation.capes.mojang.browse") ?></a>

								<div class="dropdown-divider"></div>

								<h6 class="dropdown-header">OptiFine</h6>
								<a href="<?= $app->routeUrl("/capes/optifine/browse"); ?>" class="dropdown-item"><i class="fas fa-search"></i> <?= tr("navigation.capes.optifine.browse") ?></a>
								<a href="<?= $app->routeUrl("/capes/optifine/new"); ?>" class="dropdown-item"><i class="fas fa-star"></i> <?= tr("navigation.capes.optifine.new") ?></a>
								<a href="<?= $app->routeUrl("/capes/optifine/popular"); ?>" class="dropdown-item"><i class="fas fa-thumbs-up"></i> <?= tr("navigation.capes.optifine.popular") ?></a>

								<div class="dropdown-divider"></div>

								<h6 class="dropdown-header">LabyMod</h6>
								<a href="<?= $app->routeUrl("/capes/labymod/browse"); ?>" class="dropdown-item"><i class="fas fa-search"></i> <?= tr("navigation.capes.labymod.browse") ?></a>
								<a href="<?= $app->routeUrl("/capes/labymod/new"); ?>" class="dropdown-item"><i class="fas fa-star"></i> <?= tr("navigation.capes.labymod.new") ?></a>
								<a href="<?= $app->routeUrl("/capes/labymod/popular"); ?>" class="dropdown-item"><i class="fas fa-thumbs-up"></i> <?= tr("navigation.capes.labymod.popular") ?></a>
							</div>
						</li>

						<!-- SERVERS -->
						<li class="nav-item<?php if(isset($nav) && $nav == NAV_SERVERS){ ?> active<?php } ?>">
							<a class="nav-link" href="<?= $app->routeUrl("/servers"); ?>"><?= tr("navigation.servers") ?><?php if(isset($nav) && $nav == NAV_SERVERS){ ?> <span class="sr-only"><?= tr("navigation.current") ?></span><?php } ?></a>
						</li>

						<!-- FAQ -->
						<li class="nav-item<?php if(isset($nav) && $nav == NAV_FAQ){ ?> active<?php } ?>">
							<a class="nav-link" href="<?= $app->routeUrl("/faq"); ?>"><?= tr("navigation.faq") ?><?php if(isset($nav) && $nav == NAV_FAQ){ ?> <span class="sr-only"><?= tr("navigation.current") ?></span><?php } ?></a>
						</li>

						<!-- COMMUNITY -->
						<li class="nav-item">
							<a class="nav-link" href="<?= $app->routeUrl("/discord"); ?>" target="_blank"><?= tr("navigation.community") ?></a>
						</li>

						<!-- DONATE -->
						<li class="nav-item<?php if(isset($nav) && $nav == NAV_DONATE){ ?> active<?php } ?>">
							<a class="nav-link" href="https://patreon.com/Zeryther" target="_blank"><?= tr("navigation.donate") ?><?php if(isset($nav) && $nav == NAV_DONATE){ ?> <span class="sr-only"><?= tr("navigation.current") ?></span><?php } ?></a>
						</li>
					</ul>

					<ul class="navbar-nav">
						<li class="nav-item dropdown<?php if(isset($nav) && $nav == NAV_ACCOUNT){ ?> active<?php } ?>">
							<a href="#" class="nav-link dropdown-toggle" id="accountDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<i class="fas fa-user"></i><?php if(isset($nav) && $nav == NAV_ACCOUNT){ ?> <span class="sr-only">(current)</span><?php } ?>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledBy="accountDropdown">
								<?php
									
									if(Util::isLoggedIn() == true){
										?>
								<h6 class="dropdown-header"><?= Util::getCurrentAccount()->getUsername(); ?></h6>
								<a href="<?= $app->routeUrl("/account"); ?>" class="dropdown-item"><i class="fas fa-cog"></i> <?= tr("navigation.account.myAccount") ?></a>
								<a href="<?= $app->routeUrl("/account/linked"); ?>" class="dropdown-item"><i class="fas fa-link"></i> <?= tr("navigation.account.linkedMinecraftAccounts") ?></a>
								<a href="<?= $app->routeUrl("/account/favorites"); ?>" class="dropdown-item"><i class="fas fa-heart"></i> <?= tr("navigation.account.favorites") ?></a>
								<div class="dropdown-divider"></div>
								<a href="<?= $app->routeUrl("/logout"); ?>" class="dropdown-item"><i class="fas fa-sign-out-alt"></i> <?= tr("navigation.account.logout") ?></a>
										<?php
									} else {
										?>
								<a href="<?= $app->routeUrl("/login"); ?>" class="dropdown-item"><?= tr("navigation.account.login") ?></a>
								<a href="<?= $app->routeUrl("/register"); ?>" class="dropdown-item"><?= tr("navigation.account.register") ?></a>
										<?php
									}

								?>
							</div>
						</li>

						<?php $currentLanguage = \Gigadrive\i18n\i18n::getCurrentLanguage(); ?>

						<li class="nav-item dropdown">
							<a href="#" class="nav-link dropdown-toggle" id="languageDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
								<span class="flag-icon flag-icon-<?= $currentLanguage->getFlagIconCode() ?>"></span>
							</a>

							<div class="dropdown-menu dropdown-menu-right" aria-labelledBy="languageDropdown" style="min-width: 400px;">
								<a href="<?= $app->routeUrl("/translate") ?>" target="_blank" class="dropdown-item">
									<img src="https://translate.gigadrivegroup.com/weblate/widgets/mcskinhistory/-/website/svg-badge.svg" alt="Translation status" class="d-block m-auto"/>
								</a>

								<div class="dropdown-divider"></div>

								<div class="row no-gutters">
									<?php

										foreach (\Gigadrive\i18n\i18n::Instance()->getLocales() as $locale) {
											$code = $locale->getCode();
											$flag = $locale->getFlagIconCode();
											$name = $locale->getLocalizedName();

											$active = $code === $currentLanguage->getCode();

											switch($code){
												// chinese (simplified)
												case "zh_Hans":
													$name = $name . " (Simplified)";
													break;

												// chinese (traditional)
												case "zh_Hant":
													$name = $name . " (Traditional)";
													break;
											}

											?>
									<div class="col-6">
										<a class="dropdown-item" href="<?= $app->routeUrl("/set-language?code=" . $locale->getCode()) ?>">
											<span class="mr-1">
												<span class="flag-icon flag-icon-<?= $flag ?>"></span>
											</span>

											<?= ($active ? "<b>" :"") . $name . ($active ? "</b>" : "") ?>
										</a>
									</div>
											<?php
										}

									?>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</nav>

		<?php if(!isset($nav) || (isset($nav) && $nav != NAV_HOME)){ ?>
		<div class="homesearch py-3">
			<div class="container">
				<center>
					<form method="get" action="<?= $app->routeUrl("/search"); ?>">
						<div class="input-group input-group-lg">
							<input class="form-control" type="text" name="query" value="<?= isset($searchPlaceholder) ? $searchPlaceholder : false ?>" placeholder="<?= tr("navigation.search.placeholder") ?>" autocomplete="off" spellcheck="false" autofocus/>
							<div class="input-group-append">
								<button class="btn btn-primary px-4" type="submit" title="Search"><i class="fas fa-search"></i></button>
							</div>
						</div>
					</form>
				</center>
			</div>
		</div>
		<?php } ?>

		<div class="wrapper">
			<?php

				if(isset($includeAccountNav) && $includeAccountNav == true){
					?>
			<div class="container my-3">
				<div class="row">
					<div class="col-md-4">
						<div class="card">
							<div class="card-body">
								<ul class="nav nav-pills flex-column">
									<li class="nav-item"><a class="nav-link<?php if(isset($accountNav) && $accountNav == ACCOUNT_NAV_ACCOUNT) echo ' active'; ?>" href="/account"><?= tr("navigation.account.myAccount") ?></a></li>
									<li class="nav-item"><a class="nav-link<?php if(isset($accountNav) && $accountNav == ACCOUNT_NAV_LINKED) echo ' active'; ?>" href="/account/linked"><?= tr("navigation.account.linkedMinecraftAccounts") ?></a></li>
									<li class="nav-item"><a class="nav-link<?php if(isset($accountNav) && $accountNav == ACCOUNT_NAV_FAVORITES) echo ' active'; ?>" href="/account/favorites"><?= tr("navigation.account.favorites") ?></a></li>
									<li class="nav-item"><a class="nav-link" href="/logout"><?= tr("navigation.account.logout") ?></a></li>
								</ul>
							</div>
						</div>
						<center class="my-2">
							<?php Util::renderAd(AD_TYPE_BLOCK); ?>
						</center>
					</div>

					<div class="col-md-8">
						<?= $content_for_layout; ?>
					</div>
				</div>
			</div>
					<?php
				} else {
					echo $content_for_layout;
				}

			?>
		</div>

		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-4">
						<h5>Legal</h5>
						<ul>
							<li><a href="https://gigadrivegroup.com/legal/contact" target="_blank"><?= tr("footer.legal.notice") ?></a></li>
							<li><a href="https://gigadrivegroup.com/legal/terms-of-service" target="_blank"><?= tr("footer.legal.tos") ?></a></li>
							<li><a href="https://gigadrivegroup.com/legal/privacy-policy" target="_blank"><?= tr("footer.legal.privacy") ?></a></li>
							<li><a href="https://gigadrivegroup.com/legal/disclaimer" target="_blank"><?= tr("footer.legal.disclaimer") ?></a></li>
							<li><a href="https://gigadrivegroup.com/legal/refunds" target="_blank"><?= tr("footer.legal.refunds") ?></a></li>
						</ul>
					</div>

					<div class="col-md-4">
						<h5>Important Links</h5>
						<ul>
							<li><a href="https://blog.gigadrivegroup.com/category/mcskinhistory/" target="_blank"><?= tr("footer.important.changelogs") ?></a></li>
						</ul>
					</div>

					<div class="col-md-4">
						<h5>Social Media</h5>
						<a href="https://twitter.com/mcskinhistory" target="_blank">
							<img src="<?= $app->baseUrl("assets:img/social/new/twitter/24.png"); ?>"/>
						</a>
					</div>
				</div>
			</div>

			<hr/>

			<div class="container">
				<span class="float-left">
					&copy; Copyright 2016-<?= date("Y"); ?> Gigadrive Group - <?= tr("footer.copyright.rightsReserved") ?>
				</span>

				<span class="float-right">
					<a href="https://gigadrivegroup.com" target="_blank">
						<img src="<?= $app->baseUrl("assets:img/Logo-white-small.png"); ?>" height="15"/>
					</a>
				</span>
			</div>
		</footer>

		<?= $app->script(array(
			"assets:js/bootstrap.min.js"
		)); ?>

		<script type="text/javascript">
			var adBlockModalTriggered = false;

			function adBlockNotDetected() {
				console.log("Adblock not detected");
			}

			function adBlockDetected() {
				if(adBlockModalTriggered === false){
					adBlockModalTriggered = true;

					console.log("Adblock detected");

					<?php if(!isset($_COOKIE["ignoreAdblock"]) || $_COOKIE["ignoreAdblock"] != "true"){ ?>
					$("#adBlockModal").modal("show");
					<?php } ?>
				}
			}

			if(typeof blockAdBlock === 'undefined') {
				adBlockDetected();
			} else {
				blockAdBlock.onDetected(adBlockDetected);
				blockAdBlock.onNotDetected(adBlockNotDetected);
			}

			$("#adBlockModal").on('hidden.bs.modal', function (e) {
				setCookie("ignoreAdblock","true",30);
			});
		</script>
	</body>
</html>
