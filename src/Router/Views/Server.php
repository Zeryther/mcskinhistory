<?php

use Gigadrive\i18n\i18n;
use SkinHistory\Util\DisqusThread;
use SkinHistory\Util\Util;

?><div class="container my-3">
    <h1><?= (!is_null($server->getName()) ? $server->getName() : $server->getIP()) ?></h1>

    <hr/>

    <div class="row my-2">
        <div class="col-md-5">
            <div class="card mb-3">
                <h5 class="card-header"><?= tr("server.information") ?></h5>

                <table class="table my-0">
                    <tr>
                        <td style="width: 50%"><b><?= tr("server.ips") ?></b></td>

                        <td style="width: 50%">
                            <?php

                                $ips = [$server->getIP()];
                                $i = 0;

                                $children = $server->getChildren();
                                if(!is_null($children)){
                                    foreach($children as $child){
                                        array_push($ips,$child);
                                    }
                                }

                                foreach ($ips as $ip) {
                                    ?>
                            <div>
                                <span id="ip-<?= $i ?>">
                                    <?= $ip ?>
                                </span>
                                
                                <a class="float-right copyclip" data-clipboard-target="#ip-<?= $i ?>" href="#"><i class="fas fa-copy"></i></a>
                            </div>
                                    <?php

                                    $i++;
                                }

                            ?>
                        </td>
                    </tr>

                    <tr>
                        <td style="width: 50%"><b><?= tr("server.version") ?></b></td>
                        <td style="width: 50%"><?= is_null($server->getVersion()) ? "<em>" . tr("server.version.unknown") . "</em>" : $server->getVersion() ?></td>
                    </tr>

                    <tr>
                        <td style="width: 50%"><b><?= tr("server.uptime") ?></b></td>
                        <td style="width: 50%"><?= $server->getUptime() ?>%</td>
                    </tr>

                    <tr>
                        <td style="width: 50%"><b><?= tr("server.playerPeak") ?></b></td>
                        <td style="width: 50%"><?= $server->getPlayerPeak() ?></td>
                    </tr>

                    <?php if(!is_null($server->getWebsite())){ ?>
                    <tr>
                        <td style="width: 50%"><b><?= tr("server.website") ?></b></td>
                        <td style="width: 50%"><a href="<?= $server->getWebsite() ?>" target="_blank"><?= $server->getWebsite() ?></a></td>
                    </tr>
                    <?php } ?>

                    <tr>
                        <td style="width: 50%"><b><?= tr("server.timeAdded") ?></b></td>
                        <td style="width: 50%"><?= Util::timeago($server->getTime()) ?></td>
                    </tr>
                </table>
            </div>

            <?php if(!is_null($server->getTwitter())){ ?>
            <div class="card mb-3">
				<a class="twitter-timeline" data-lang="<?= i18n::getCurrentLanguage()->getFlagIconCode() ?>" data-height="500" data-theme="light" data-link-color="#D9230F" href="https://twitter.com/<?= $server->getTwitter() ?>?ref_src=twsrc%5Etfw" data-chrome="nofooter noborders">Tweets by <?= $server->getTwitter() ?></a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>
            <?php } ?>

            <center>
				<?php Util::renderAd(AD_TYPE_BLOCK); ?>
			</center>

            <center class="mt-3">
				<iframe src="https://www.instant-gaming.com/affgames/igr1710165/350x350" scrolling="no" frameborder="0" style="border: 1px solid #ccc; border-radius: 5px; overflow:hidden; width:350px; height:350px;" allowTransparency="true"></iframe>
			</center>
        </div>

        <div class="col-md-7">
            <div class="bg-dark text-white px-3 py-3 rounded" style="min-height: 96px">
                <img src="<?= !is_null($server->getIcon()) ? $server->getIcon() : $app->baseUrl("assets:img/defaultServerIcon.png") ?>" class="float-left"/>

                <div class="float-left ml-2" style="max-width: 65%; max-height: 60px; overflow: hidden">
                    <div class="font-weight-bold"><?= (!is_null($server->getName()) ? $server->getName() : $server->getIP()) ?></div>

                    <?= $server->getConvertedMotd() ?>
                </div>

                <div class="float-right">
                    <?= $server->getPlayersOnline() ?> / <?= $server->getPlayersMax() ?>
                </div>
            </div>

            <center class="my-3">
				<?php Util::renderAd(AD_TYPE_LEADERBOARD); ?>
			</center>

            <?php

                $samples = $server->getPlayerSamples();
                if(!is_null($samples) && count($samples) > 0){

            ?>
            <div class="card">
				<div class="card-body">
                    <div class="chart" id="player-chart" style="position: relative; height: 300px;"></div>

                    <script type="text/javascript">
                        Morris.Area({
                            element: 'player-chart',
                            data: [
                                <?php

                                    foreach ($samples as $data) {
                                        $date = $data["date"];
                                        $hour = $data["hour"];
                                        $players = $data["players"];

                                        ?>
                                        
                                { date: f("<?= $date . "T" . ($hour <= 9 ? "0" : "") . $hour . ":00:00Z" ?>"), players: <?= $players ?> },
                                        <?php
                                    }

                                ?>
                            ],
                            xkey: 'date',
                            ykeys: ['players'],
                            labels: ['<?= tr("server.chart.playersLabel") ?>']
                        });

                        function f(string){
                            let date = new Date(string);
                            return date.getFullYear() != "NaN" ? date.getFullYear() + "-" + a(date.getMonth()+1) + "-" + a(date.getDate()) + " " + a(date.getHours()) + ":" + a(date.getMinutes()) + ":" + a(date.getSeconds()) : string;
                        }

                        function a(i){
                            return i < 10 ? "0" + i : i;
                        }
                    </script>
				</div>
			</div>

            <center class="my-3">
				<?php Util::renderAd(AD_TYPE_LEADERBOARD); ?>
			</center>
            <?php } ?>

            <div class="card">
				<div class="card-body">
					<?php
					
						$disqus = new DisqusThread();
						
						$disqus->setPageURL("https://mcskinhistory.com/server/" . $server->getIP());
						$disqus->setPageIdentifier("mcskinhistory_server_" . $server->getIP());

						$disqus->render();

					?>
				</div>
			</div>
        </div>
    </div>
</div>