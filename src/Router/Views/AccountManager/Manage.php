<?php

use SkinHistory\Player\Player;
use SkinHistory\Skin\Texture;
use SkinHistory\Util\Util;

$self = $app->routeUrl("/account/linked/manage/" . $player->getUUID());

$activeTab = "info";

$platforms = [
	"facebook" => [
		"display" => "Facebook",
		"prefix" => "facebook.com/"
	],
	"github" => [
		"display" => "GitHub",
		"prefix" => "github.com/"
	],
	"googleplus" => [
		"display" => "Google+",
		"prefix" => "plus.google.com/"
	],
	"instagram" => [
		"display" => "Instagram",
		"prefix" => "instagram.com/"
	],
	"beam" => [
		"display" => "Mixer",
		"prefix" => "mixer.com/"
	],
	"reddit" => [
		"display" => "Reddit",
		"prefix" => "reddit.com/user/"
	],
	"soundcloud" => [
		"display" => "SoundCloud",
		"prefix" => "soundcloud.com/"
	],
	"steam" => [
		"display" => "Steam",
		"prefix" => "steamcommunity.com/"
	],
	"twitch" => [
		"display" => "Twitch",
		"prefix" => "twitch.tv/"
	],
	"twitter" => [
		"display" => "Twitter",
		"prefix" => "twitter.com/"
	],
	"youtube" => [
		"display" => "YouTube",
		"prefix" => "youtube.com/"
	],
];

$errorMsg = null;
$successMsg = null;

if(isset($_POST["action"]) && !empty($_POST["action"])){
	$action = $_POST["action"];

	if($action == "updateSocialMedia"){
		$b = true;

		foreach($platforms as $name => $data){
			if(!isset($_POST[$name])){
				$b = false;
			}
		}

		if($b){
			$socialMedia = $player->getSocialMedia();
			if($socialMedia == null) $socialMedia = array();

			foreach($platforms as $name => $data){
				$v = $_POST[$name];
				
				if(array_key_exists($name,$socialMedia) && ($v == null || empty($v))){
					unset($socialMedia[$name]);
				} else {
					if($v != null && !empty($v)) $socialMedia[$name] = $v;
				}
			}

			$player->setSocialMedia($socialMedia);
			$player->saveToDatabase();

			$successMsg = tr("account.linkedAccounts.manage.socialMedia.success");
		} else {
			$errorMsg = tr("account.linkedAccounts.manage.socialMedia.incorrectFields");
		}
	} else if($action == "removeTexture"){
		if(isset($_POST["textureID"]) && !empty($_POST["textureID"])){
			$textureID = $_POST["textureID"];
			$texture = Texture::getTextureFromID($textureID);

			if($texture != null && $texture->getUUID() == $player->getUUID()){
				if($texture->getSkin()->getType() == "SKIN") $activeTab = "skins";
				if($texture->getSkin()->getType() == "CAPE_OPTIFINE") $activeTab = "optifine";
				if($texture->getSkin()->getType() == "CAPE_LABYMOD") $activeTab = "laby";

				$r = $player->removeTexture($textureID);

				if($r == true){
					$successMsg = tr("account.linkedAccounts.manage.removeTexture.success");
				} else if($r == false) {
					$errorMsg = tr("error.any");
				} else {
					$errorMsg = $r;
				}
			} else {
				$errorMsg = tr("account.linkedAccounts.manage.removeTexture.invalidID");
			}
		}
	}
}

if($errorMsg != null && !empty($errorMsg)){
	Util::createAlert("errorMsg",$errorMsg,ALERT_TYPE_PRIMARY,true);
}

if($successMsg != null && !empty($successMsg)){
	Util::createAlert("successMsg",$successMsg,ALERT_TYPE_SUCCESS,true);
}

foreach($player->getTextures() as $texture){
	$skinFile = $texture->getSkin();
	?>
<div class="modal fade" id="removeModal-<?= $skinFile->getID(); ?>">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?= tr("account.linkedAccounts.removeTexture.modal.headline",[$skinFile->getTitle() != null ? $skinFile->getTitle() : "#" . $skinFile->getID()]) ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<form action="<?= $self; ?>" method="post">
				<input type="hidden" name="action" value="removeTexture"/>
				<input type="hidden" name="textureID" value="<?= $texture->getID(); ?>"/>

				<div class="modal-body">
					<?= tr("accounts.linkedAccounts.removeTexture.modal.areYouSure") ?><br/>
					<?= $skinFile->getType() == "SKIN" ? tr("accounts.linkedAccounts.manage.removeTexture.modal.skin") : tr("accounts.linkedAccounts.manage.removeTexture.modal.cape"); ?>
				</div>

				<div class="modal-footer">
					<button type="submit" class="btn btn-primary"><?= tr("accounts.linkedAccounts.manage.removeTexture.modal.removeButton") ?></button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= tr("modalButton.cancel") ?></button>
				</div>
			</form>
		</div>
	</div>
</div>
	<?php
}

?>
<div class="modal fade" id="switchModal">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title"><?= tr("account.linkedAccounts.manage.switchModal.headline") ?></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>

			<div class="modal-body">
			<?php

			if(count(Util::getCurrentAccount()->getLinkedAccounts()) > 0){
				?>
			<table class="table my-0">
			<tbody>
				<?php

					foreach(Util::getCurrentAccount()->getLinkedAccounts() as $account){
						$linked = Player::getPlayer($account["uuid"]);
						$time = $account["time"];

						?>
				<tr>
					<td style="width: 50%"><a href="<?= "/player/" . $linked->getUUID(); ?>"><img style="margin-right: 10px" src="https://crafatar.com/avatars/<?= $linked->getUUID(); ?>?overlay&size=24&default=MHF_Steve" /><?= $linked->getUsername(); ?></a></td>
					<td><?= Util::timeago($time); ?></td>
					<td>
						<div class="float-right">
							<a href="<?= "/account/linked/manage/" . $linked->getUUID(); ?>"><button type="button" class="btn btn-success btn-sm"><?= tr("account.linkedAccounts.manage.switchModal.switchButton") ?></button></a>
						</div>
					</td>
				</tr>
						<?php
					}

				?>
			</thead>
			</table>
				<?php
			} else {
				?>
			(<?= tr("account.linkedAccounts.manage.switchModal.noAccounts") ?>)
				<?php
			}

			?>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal"><?= tr("modalButton.cancel") ?></button>
			</div>
		</div>
	</div>
</div>

<h3>
	<img style="margin-right: 10px" src="https://crafatar.com/avatars/<?= $player->getUUID(); ?>?overlay&size=36&default=MHF_Steve" /><?= $player->getUsername(); ?>
	<button type="button" class="btn btn-success btn-sm float-right my-2" data-toggle="modal" data-target="#switchModal"><?= tr("account.linkedAccounts.manage.switchModal.headline") ?></button>
</h3>
<ul class="nav nav-pills nav-fill mb-3" id="tabs" role="tablist">
	<li class="nav-item"><a id="tablist-info" class="nav-link<?php if($activeTab == "info"){ echo ' active'; } else { echo ''; } ?>" href="#infoTab" aria-controls="#infoTab" aria-selected="<?php if($activeTab == "info"){ echo 'true'; } else { echo 'false'; } ?>" data-toggle="pill" ><?= tr("account.linkedAccounts.manage.information") ?></a></li>
	<li class="nav-item"><a id="tablist-skins" class="nav-link<?php if($activeTab == "skins"){ echo ' active'; } else { echo ''; } ?>" href="#skinsTab" aria-controls="#skinsTab" aria-selected="<?php if($activeTab == "skins"){ echo 'true'; } else { echo 'false'; } ?>" data-toggle="pill" ><?= tr("account.linkedAccounts.manage.skins") ?></a></li>
	<li class="nav-item"><a id="tablist-optifine" class="nav-link<?php if($activeTab == "optifine"){ echo ' active'; } else { echo ''; } ?>" href="#optifineTab" aria-controls="#optifineTab" aria-selected="<?php if($activeTab == "optifine"){ echo 'true'; } else { echo 'false'; } ?>" data-toggle="pill" ><?= tr("account.linkedAccounts.manage.optifine") ?></a></li>
	<li class="nav-item"><a id="tablist-laby" class="nav-link<?php if($activeTab == "laby"){ echo ' active'; } else { echo ''; } ?>" href="#labyTab" aria-controls="#labyTab" aria-selected="<?php if($activeTab == "laby"){ echo 'true'; } else { echo 'false'; } ?>" data-toggle="pill" ><?= tr("account.linkedAccounts.manage.labymod") ?></a></li>
	<li class="nav-item"><a id="tablist-misc" class="nav-link disabled<?php if($activeTab == "misc"){ echo ' active'; } else { echo ''; } ?>" href="#miscTab" aria-controls="#miscTab" aria-selected="<?php if($activeTab == "misc"){ echo 'true'; } else { echo 'false'; } ?>" data-toggle="pill" ><?= tr("account.linkedAccounts.manage.misc") ?></a></li>
</ul>

<div class="tab-content" id="tabsContent">
	<!-- INFORMATION -->
	<div class="tab-pane fade<?php if($activeTab == "info"){ echo ' show active'; } else { echo ''; } ?>" id="infoTab" role="tabpanel" aria-labelledby="tablist-info">
		<div class="card">
			<h5 class="card-header"><?= tr("profile.socialMedia") ?></h5>

			<div class="card-body">
				<form action="<?= $self; ?>" method="post">
					<input type="hidden" name="action" value="updateSocialMedia"/>
					<fieldset>
						<?php

						foreach($platforms as $name => $data){
							$value = ($player->getSocialMedia() != null && array_key_exists($name,$player->getSocialMedia())) ? $player->getSocialMedia()[$name] : "";

							?>
						<div class="form-group row">
							<label for="<?= $name; ?>" class="control-label col-sm-2 col-form-label"><?= $data["display"]; ?></label>
							<div class="col-sm-10 input-group mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text"><?= $data["prefix"]; ?></span>
								</div>
								<input class="form-control" type="text" name="<?= $name; ?>" id="<?= $name; ?>" value="<?= $value; ?>"/>
							</div>
						</div>
							<?php
						}

						?>

						<button type="submit" class="btn btn-primary btn-block"><?= tr("modalButton.submit") ?></button>
					</fieldset>
				</form>
			</div>
		</div>
	</div>

	<!-- SKINS -->
	<div class="tab-pane fade<?php if($activeTab == "skins"){ echo ' show active'; } else { echo ''; } ?>" id="skinsTab" role="tabpanel" aria-labelledby="tablist-skins">
		<div class="card">
			<h5 class="card-header"><?= tr("account.linkedAccounts.manage.skins") ?></h5>

			<div class="card-body text-center">
				<?php

					if(count($player->getTextures("SKIN")) > 0){
						$first = true;
						foreach($player->getTextures("SKIN") as $texture){
							$skin = $texture->getSkin();

							?>
				<img onmouseover="updateSkin(<?= $skin->getID(); ?>,'<?= $skin->getFullURL(); ?>');" height="48" id="skinfile-<?= $skin->getID(); ?>" src="/avatar/skin/face/<?= $skin->getID(); ?>" class="ml-1 mr-1<?php if($first == true){ ?> skinShadow<?php } ?>"/>
							<?php

							$first = false;
						}

						$firstID = count($player->getTextures("SKIN")) > 0 ? $player->getTextures("SKIN")[0]->getSkin()->getID() : "0";
						$firstFullURL = count($player->getTextures("SKIN")) > 0 ? $player->getTextures("SKIN")[0]->getSkin()->getFullURL() : "";
					} else {
						?>
				<p><?= tr("account.linkedAccounts.manage.noFiles") ?></p>
						<?php
					}

				?>
			</div>
		</div>

		<?php if(count($player->getTextures("SKIN")) > 0){ ?>
		<div class="card my-2">
			<div class="card-body text-center">
				<img id="skinDisplay" class="skinShadow" src="/avatar-3d-custom.php?skinFileID=<?= $firstID; ?>"/>

				<div class="mt-2">
					<a href="<?= $app->routeUrl("/skin/" . $firstID); ?>" id="skinViewLink"><button id="skinView" class="btn btn-info ml-1 mr-1" type="button">Vie<?= tr("account.linkedAccounts.manage.viewButton") ?>w</button></a>
					<a href="<?= $app->routeUrl("/download/" . $firstID); ?>" id="skinDownloadLink"><button id="skinDownload" class="btn btn-info ml-1 mr-1" type="button"><?= tr("account.linkedAccounts.manage.downloadButton") ?></button></a>
					<button id="skinRemove" class="btn btn-primary ml-1 mr-1" type="button" data-toggle="modal" data-target="#removeModal-<?= $firstID; ?>"><?= tr("account.linkedAccounts.manage.removeButton") ?></button>
					<a href="https://minecraft.net/profile/skin/remote?url=<?= $firstFullURL; ?>" id="skinChangeLink"><button id="skinChange" class="btn btn-success ml-1 mr-1" type="button"><?= tr("account.linkedAccounts.manage.changeSkinButton") ?></button></a>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>

	<!-- OPTIFINE CAPES -->
	<div class="tab-pane fade<?php if($activeTab == "optifine"){ echo ' show active'; } else { echo ''; } ?>" id="optifineTab" role="tabpanel" aria-labelledby="tablist-optifine">
		<div class="card">
			<h5 class="card-header"><?= tr("account.linkedAccounts.manage.optifine") ?></h5>

			<div class="card-body text-center">
				<?php

				if(count($player->getTextures("CAPE_OPTIFINE")) > 0){
					$first = true;
					foreach($player->getTextures("CAPE_OPTIFINE") as $texture){
						$skin = $texture->getSkin();

						?>
				<img onmouseover="updateOptiFineCape(<?= $skin->getID(); ?>);" height="48" id="skinfile-<?= $skin->getID(); ?>" src="/avatar/cape/<?= $skin->getID(); ?>" class="ml-1 mr-1<?php if($first == true){ ?> skinShadow<?php } ?>"/>
						<?php
						$first = false;
					}

					$firstID = count($player->getTextures("CAPE_OPTIFINE")) > 0 ? $player->getTextures("CAPE_OPTIFINE")[0]->getSkin()->getID() : "0";
					$firstFullURL = count($player->getTextures("CAPE_OPTIFINE")) > 0 ? $player->getTextures("CAPE_OPTIFINE")[0]->getSkin()->getFullURL() : "";
				} else {
					?>
				<p><?= tr("account.linkedAccounts.manage.noFiles") ?></p>
					<?php
				}

				?>
			</div>
		</div>

		<?php if(count($player->getTextures("CAPE_OPTIFINE")) > 0){ ?>
		<div class="card my-2">
			<div class="card-body text-center">
				<img id="optifineDisplay" class="skinShadow" src="/avatar/cape/<?= count($player->getTextures("CAPE_OPTIFINE")) > 0 ? $player->getTextures("CAPE_OPTIFINE")[0]->getSkin()->getID() : "0"; ?>"/>

				<div class="mt-2">
					<a href="<?= $app->routeUrl("/cape/" . $firstID); ?>" id="optifineViewLink"><button id="optifineView" class="btn btn-info ml-1 mr-1" type="button"><?= tr("account.linkedAccounts.manage.viewButton") ?></button></a>
					<a href="<?= $app->routeUrl("/download/" . $firstID); ?>" id="optifineDownloadLink"><button id="optifineDownload" class="btn btn-info ml-1 mr-1" type="button"><?= tr("account.linkedAccounts.manage.downloadButton") ?></button></a>
					<button id="optifineRemove" class="btn btn-primary ml-1 mr-1" type="button" data-toggle="modal" data-target="#removeModal-<?= $firstID; ?>"><?= tr("account.linkedAccounts.manage.removeButton") ?></button>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>

	<!-- LABYMOD CAPES -->
	<div class="tab-pane fade<?php if($activeTab == "laby"){ echo ' show active'; } else { echo ''; } ?>" id="labyTab" role="tabpanel" aria-labelledby="tablist-laby">
		<div class="card">
			<h5 class="card-header"><?= tr("account.linkedAccounts.manage.labymod") ?></h5>

			<div class="card-body text-center">
				<?php

				if(count($player->getTextures("CAPE_LABYMOD")) > 0){
					$first = true;
					foreach($player->getTextures("CAPE_LABYMOD") as $texture){
						$skin = $texture->getSkin();

						?>
				<img onmouseover="updateLabyModCape(<?= $skin->getID(); ?>);" height="48" id="skinfile-<?= $skin->getID(); ?>" src="/avatar/cape/<?= $skin->getID(); ?>" class="ml-1 mr-1<?php if($first == true){ ?> skinShadow<?php } ?>"/>
						<?php
						$first = false;
					}

					$firstID = count($player->getTextures("CAPE_LABYMOD")) > 0 ? $player->getTextures("CAPE_LABYMOD")[0]->getSkin()->getID() : "0";
					$firstFullURL = count($player->getTextures("CAPE_LABYMOD")) > 0 ? $player->getTextures("CAPE_LABYMOD")[0]->getSkin()->getFullURL() : "";
				} else {
					?>
				<p><?= tr("account.linkedAccounts.manage.noFiles") ?></p>
					<?php
				}

				?>
			</div>
		</div>

		<?php if(count($player->getTextures("CAPE_LABYMOD")) > 0){ ?>
		<div class="card my-2">
			<div class="card-body text-center">
				<img id="labyDisplay" class="skinShadow" src="/avatar/cape/<?= count($player->getTextures("CAPE_LABYMOD")) > 0 ? $player->getTextures("CAPE_LABYMOD")[0]->getSkin()->getID() : "0"; ?>"/>

				<div class="mt-2">
					<a href="<?= $app->routeUrl("/cape/" . $firstID); ?>" id="labyViewLink"><button id="labyView" class="btn btn-info ml-1 mr-1" type="button"><?= tr("account.linkedAccounts.manage.viewButton") ?></button></a>
					<a href="<?= $app->routeUrl("/download/" . $firstID); ?>" id="labyDownloadLink"><button id="labyDownload" class="btn btn-info ml-1 mr-1" type="button"><?= tr("account.linkedAccounts.manage.downloadButton") ?></button></a>
					<button id="labyRemove" class="btn btn-primary ml-1 mr-1" type="button" data-toggle="modal" data-target="#removeModal-<?= $firstID; ?>"><?= tr("account.linkedAccounts.manage.removeButton") ?></button>
				</div>
			</div>
		</div>
		<?php } ?>
	</div>

	<!-- MISC -->
	<div class="tab-pane fade<?php if($activeTab == "misc"){ echo ' show active'; } else { echo ''; } ?>" id="miscTab" role="tabpanel" aria-labelledby="tablist-misc">
		<div class="card">
			<h5 class="card-header"><?= tr("account.linkedAccounts.manage.misc") ?></h5>

			<div class="card-body">
				aD
			</div>
		</div>
	</div>
</div>
<script type="text/javascript">
var activeSkin = <?= count($player->getTextures("SKIN")) > 0 ? $player->getTextures("SKIN")[0]->getSkin()->getID() : "0"; ?>;
var activeOptiFineCape = <?= count($player->getTextures("CAPE_OPTIFINE")) > 0 ? $player->getTextures("CAPE_OPTIFINE")[0]->getSkin()->getID() : "0"; ?>;
var activeLabyModCape = <?= count($player->getTextures("CAPE_LABYMOD")) > 0 ? $player->getTextures("CAPE_LABYMOD")[0]->getSkin()->getID() : "0"; ?>;

function updateSkin(id,fullURL){
	if(activeSkin != id){
		var face = document.getElementById("skinfile-" + id);
		var oldFace = document.getElementById("skinfile-" + activeSkin);

		if(face != null){
			face.classList.add("skinShadow");
		}

		if(oldFace != null){
			oldFace.classList.remove("skinShadow");
		}

		var displayImg = document.getElementById("skinDisplay");

		if(displayImg != null){
			displayImg.src = "/avatar-3d-custom.php?skinFileID=" + id;
		}

		var removeButton = document.getElementById("skinRemove");

		if(removeButton != null){
			removeButton.setAttribute("data-target","#removeModal-" + id);
		}

		var downloadButton = document.getElementById("skinDownloadLink");

		if(downloadButton != null){
			downloadButton.href = "/download/" + id;
		}

		var viewButton = document.getElementById("skinViewLink");

		if(viewButton != null){
			viewButton.href = "/skin/" + id;
		}

		var changeURL = "https://minecraft.net/profile/skin/remote?url=" + fullURL;
		var changeLink = document.getElementById("skinChangeLink");

		if(changeLink != null){
			changeLink.href = changeURL;
		}

		activeSkin = id;
	}
}

function updateOptiFineCape(id){
	if(activeOptiFineCape != id){
		var face = document.getElementById("skinfile-" + id);
		var oldFace = document.getElementById("skinfile-" + activeOptiFineCape);

		if(face != null){
			face.classList.add("skinShadow");
		}

		if(oldFace != null){
			oldFace.classList.remove("skinShadow");
		}

		var displayImg = document.getElementById("optifineDisplay");

		if(displayImg != null){
			displayImg.src = "/avatar/cape/" + id;
		}

		var removeButton = document.getElementById("optifineRemove");

		if(removeButton != null){
			removeButton.setAttribute("data-target","#removeModal-" + id);
		}

		var downloadButton = document.getElementById("optifineDownloadLink");

		if(downloadButton != null){
			downloadButton.href = "/download/" + id;
		}

		var viewButton = document.getElementById("optifineViewLink");

		if(viewButton != null){
			viewButton.href = "/cape/" + id;
		}

		activeOptiFineCape = id;
	}
}

function updateLabyModCape(id){
	if(activeLabyModCape != id){
		var face = document.getElementById("skinfile-" + id);
		var oldFace = document.getElementById("skinfile-" + activeLabyModCape);

		if(face != null){
			face.classList.add("skinShadow");
		}

		if(oldFace != null){
			oldFace.classList.remove("skinShadow");
		}

		var displayImg = document.getElementById("labyDisplay");

		if(displayImg != null){
			displayImg.src = "/avatar/cape/" + id;
		}

		var removeButton = document.getElementById("labyRemove");

		if(removeButton != null){
			removeButton.setAttribute("data-target","#removeModal-" + id);
		}

		var downloadButton = document.getElementById("labyDownloadLink");

		if(downloadButton != null){
			downloadButton.href = "/download/" + id;
		}

		var viewButton = document.getElementById("labyViewLink");

		if(viewButton != null){
			viewButton.href = "/cape/" + id;
		}

		activeLabyModCape = id;
	}
}
</script>