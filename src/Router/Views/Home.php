<?php

use SkinHistory\Util\Util;
use SkinHistory\Server\Server;

?><div class="homesearch py-3">
	<div class="container">
		<h1><?= tr("home.headline") ?></h1>
		<h5><?= tr("home.headline.sub") ?></h5>

		<center>
			<form method="get" action="<?= $app->routeUrl("/search"); ?>">
				<div class="input-group input-group-lg">
					<input class="form-control" type="text" name="query" placeholder="<?= tr("navigation.search.placeholder") ?>" autocomplete="off" spellcheck="false" autofocus/>
					<div class="input-group-append">
						<button class="btn btn-primary px-4" type="submit" title="Search"><i class="fas fa-search"></i></button>
					</div>
				</div>
			</form>
		</center>
	</div>
</div>

<div class="container my-3">
	<div class="row">
		<div class="col-md-7">
			<h1><?= tr("home.trendingServers") ?></h1>
			<hr/>

			<center class="mb-3">
				<?php Util::renderAd(AD_TYPE_LEADERBOARD); ?>
			</center>

			<?php

				$i = 1;

				$s = Util::isDevMode() ? "?ip=" : "";

				$trendingServers = Server::getTrendingServers();
				foreach($trendingServers as $server){
					?>
			<a href="<?= $app->routeUrl("/server/" . $s . $server->getIP()) ?>">
				<div class="bg-dark text-white px-3 py-3 rounded mb-2" style="min-height: 96px">
					<img src="<?= !is_null($server->getIcon()) ? $server->getIcon() : $app->baseUrl("assets:img/defaultServerIcon.png") ?>" class="float-left"/>

					<div class="float-left ml-2" style="max-width: 65%; max-height: 60px; overflow: hidden">
						<div class="font-weight-bold"><?= (!is_null($server->getName()) ? $server->getName() : $server->getIP()) ?></div>

						<?= $server->getConvertedMotd() ?>
					</div>

					<div class="float-right">
						<?= $server->getPlayersOnline() ?> / <?= $server->getPlayersMax() ?> | #<?= $i ?>
					</div>
				</div>
			</a>
					<?php

					$i++;
				}

			?>

			<center>
				<?php Util::renderAd(AD_TYPE_LEADERBOARD); ?>
			</center>
		</div>

		<div class="col-md-5">
			<?php if(Util::isLoggedIn()){ ?>
			<div class="card mb-2">
				<h5 class="card-header"><?= tr("dashboard.yourFavorites.headline") ?><a href="<?= $app->routeUrl("/account/favorites"); ?>" class="float-right"><?= tr("dashboard.yourFavorites.more") ?> &raquo;</a></h5>

				<?php

				if(count(Util::getCurrentAccount()->getFavorites()) > 0){
					$limit = 5;
					$i = 0;

					foreach(Util::getCurrentAccount()->getFavorites() as $favorite){
						$i++;
						if($i-1 == $limit) break;

						$skin = $favorite->getSkinFile();
						$img = $skin->getType() == "SKIN" ? "/avatar-3d-custom.php?skinFileID=" . $skin->getID() : "/avatar/cape/" . $skin->getID();

						$s = $skin->getType() == "SKIN" ? " pr-4" : "";
						$skinTitle = $skin->getTitle() != null ? $skin->getTitle() : ($skin->getType() == "SKIN" ? tr("skin.headline",["#" . $skin->getID()]) : tr("cape.headline",["#" . $skin->getID()]));
						$link = $skin->getType() == "SKIN" ? "/skin/" . $skin->getID() : "/cape/" . $skin->getID();

						?>
				<div class="row" style="border-bottom: 1px solid #CCC">
					<div class="col-lg-3 col-xs-3 col-sm-3 text-right py-2<?= $s; ?>">
						<a href="<?= $link; ?>" class="clearUnderline"><img src="<?= $img; ?>" height="70"/></a>
					</div>

					<div class="col-lg-9 col-xs-9 col-sm-9 py-3">
						<a href="<?= $link; ?>" style="font-size: 17px"><?= $skinTitle; ?></a><br/>
						<?= Util::timeago($favorite->getTime()); ?>
					</div>
				</div>
						<?php
					}
				} else {
					?>
				<div class="card-body text-center">
					<?= tr("dashboard.yourFavorites.noFavorites") ?>
				</div>
					<?php
				}

				?>
			</div>
			<?php } else { ?>
			<div class="card mb-2">
				<div class="card-body text-center">
					<?= tr("dashboard.yourFavorites.loginPlaceholder") ?>

					<a href="<?= $app->routeUrl("/login") ?>" class="btn btn-success btn-block btn-lg mt-2">
						<?= tr("dashboard.yourFavorites.loginPlaceholder.button") ?>
					</a>
				</div>
			</div>
			<?php } ?>

			<div class="card mb-2">
				<a class="twitter-timeline" data-lang="<?= \Gigadrive\i18n\i18n::getCurrentLanguage()->getFlagIconCode() ?>" data-height="500" data-theme="light" data-link-color="#D9230F" href="https://twitter.com/mcskinhistory?ref_src=twsrc%5Etfw" data-chrome="nofooter noborders">Tweets by mcskinhistory</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
			</div>

			<center>
				<?php Util::renderAd(AD_TYPE_BLOCK); ?>
			</center>

			<center class="mt-3">
				<iframe src="https://www.instant-gaming.com/affgames/igr1710165/350x350" scrolling="no" frameborder="0" style="border: 1px solid #ccc; border-radius: 5px; overflow:hidden; width:350px; height:350px;" allowTransparency="true"></iframe>
			</center>
		</div>
	</div>
</div>
