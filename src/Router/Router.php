<?php

$app = new Lime\App();
$app['config.begin_point'] = microtime();
$app['config.site'] = array(
    "name" => "MCSkinHistory.com"
);

// define paths
$app->path("root",__DIR__ . "/../../");
$app->path("library",__DIR__ . "/../../library/");
$app->path("assets",__DIR__ . "/../../assets/");
$app->path("routes",__DIR__ . "/Routes/");
$app->path("views",__DIR__ . "/Views/");

// api
include $app->path("routes:API/APIRoute.php");
include $app->path("routes:API/AvatarRoute.php");
include $app->path("routes:API/ProxyRoute.php");

// cronjobs
include $app->path("routes:Cronjobs/ClearCache.php");
include $app->path("routes:Cronjobs/DeleteStaleServers.php");
include $app->path("routes:Cronjobs/DeleteStaleServerPings.php");
include $app->path("routes:Cronjobs/UpdateQueue.php");
include $app->path("routes:Cronjobs/UpdateTextures.php");

// skin file lists
include $app->path("routes:Lists/Cape/LabyMod/BrowseRoute.php");
include $app->path("routes:Lists/Cape/LabyMod/NewRoute.php");
include $app->path("routes:Lists/Cape/LabyMod/PopularRoute.php");
include $app->path("routes:Lists/Cape/OptiFine/BrowseRoute.php");
include $app->path("routes:Lists/Cape/OptiFine/NewRoute.php");
include $app->path("routes:Lists/Cape/OptiFine/PopularRoute.php");
include $app->path("routes:Lists/Cape/Mojang/BrowseRoute.php");
include $app->path("routes:Lists/Skin/BrowseRoute.php");
include $app->path("routes:Lists/Skin/NewRoute.php");
include $app->path("routes:Lists/Skin/PopularRoute.php");

// redirects
include $app->path("routes:Redirects/DiscordRoute.php");
include $app->path("routes:Redirects/DownloadRoute.php");
include $app->path("routes:Redirects/SearchRoute.php");
include $app->path("routes:Redirects/SetLanguageRoute.php");
include $app->path("routes:Redirects/TranslateRoute.php");

include $app->path("routes:AccountsRoute.php");
include $app->path("routes:CapeRoute.php");
//include $app->path("routes:ConvertScripts.php");
include $app->path("routes:FAQRoute.php");
include $app->path("routes:FavoritesRoute.php");
include $app->path("routes:HomeRoute.php");
include $app->path("routes:ManageAccountsRoute.php");
include $app->path("routes:NameRoute.php");
include $app->path("routes:ProfileRoute.php");
include $app->path("routes:ScriptsRoute.php");
include $app->path("routes:ServerRoute.php");
include $app->path("routes:SkinRoute.php");

$app->on("after",function() {
	if($this->response->status == "404"){
		$data = array(
			"title" => "Not found",
			"description" => DEFAULT_DESCRIPTION,
			"twitterImage" => "https://pbs.twimg.com/profile_images/819307463170211841/FvYYjVHb.jpg"
		);

		$this->response->body = $this->render("views:ErrorPages/404.php with views:Layout.php",$data);
	}
});

$app->run();