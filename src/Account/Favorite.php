<?php

namespace SkinHistory\Account;

use SkinHistory\Skin\SkinFile;


/**
 * Represents a favorite made by a user containing data about it
 * 
 * @package Account
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class Favorite {
	/**
	 * Gets a favorite by its ID.
	 * 
	 * @access public
	 * @param int $id The favorite ID.
	 * @return Favorite|null The favorite object, null if the favorite does not exist.
	 */
	public static function getFavoriteById($id){
		$cacheName = "favorite_" . $id;

		if(\CacheHandler::existsInCache($cacheName)){
			return \CacheHandler::getFromCache($cacheName);
		} else {
			$favorite = new Favorite($id);
			$favorite->load();
			if($favorite->exists == true){
				return $favorite;
			} else {
				return null;
			}
		}
	}

	/**
	 * Creates a favorite with the passed account and skin file IDs.
	 * 
	 * @access public
	 * @param int $accountID The account ID.
	 * @param int $skinFileID The skin file ID.
	 * @return int|null The ID of the newly created favorite, null if the favorite could not be created.
	 */
	public static function createFavorite($accountID,$skinFileID){
		$id = null;
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_favorites` (`account`,`skinFile`) VALUES(?,?);");
		$stmt->bind_param("ii",$accountID,$skinFileID);
		$stmt->execute();
		$id = $stmt->insert_id;
		$stmt->close();

		if($id == null || $id <= 0){
			$stmt = $mysqli->prepare("SELECT `id` FROM `skinhistory_favorites` WHERE `account` = ? AND `skinFile` = ?");
			$stmt->bind_param("ii",$accountID,$skinFileID);
			$stmt->execute();
			$result = $stmt->get_result();

			if($result->num_rows){
				$row = $result->fetch_assoc();
				$id = $row["id"];
			}

			$stmt->close();
		}

		return $id;
	}

	/**
	 * @access private
	 */
	private function load(){
		$mysqli = \Database::Instance()->get();
		$stmt = $mysqli->prepare("SELECT f.id AS favoriteID,f.account,f.time AS favoriteTime,s.id AS skinFileID,s.title,s.md5,s.full_url,s.firstUser,s.type,s.model,s.isBanned,s.randomizer,s.textureData,s.time AS skinFileTime FROM `skinhistory_favorites` AS f INNER JOIN `skinhistory_skinfiles` AS s ON s.id = f.skinFile WHERE f.id = ?");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows){
			$row = $result->fetch_assoc();

			$this->id = $row["favoriteID"];
			$this->account = $row["account"];
			$this->skinFile = $row["skinFileID"];
			$this->time = $row["favoriteTime"];
			$this->exists = true;

			// load data
			SkinFile::getSkinFileFromData($row["skinFileID"],$row["title"],$row["md5"],$row["full_url"],$row["firstUser"],$row["type"],$row["model"],$row["isBanned"],$row["randomizer"],$row["textureData"],$row["skinFileTime"]);
			Account::getAccount($this->account);

			$this->saveToCache();
		}

		$stmt->close();
	}

	/**
	 * Creates a favorite object with the passed data and saves it to the cache
	 * 
	 * @access public
	 * @param int $id The favorite ID.
	 * @param int $account The account ID.
	 * @param int $skinFile The skin file ID.
	 * @param string $time The creation timestamp.
	 * @return Favorite
	 */
	public static function getFavoriteFromData($id,$account,$skinFile,$time){
		if(is_array($skinFile)){
			// load data
			SkinFile::getSkinFileFromData($skinFile["id"],$skinFile["title"],$skinFile["md5"],$skinFile["full_url"],$skinFile["firstUser"],$skinFile["type"],$skinFile["model"],$skinFile["isBanned"],$skinFile["randomizer"],$skinFile["textureData"],$skinFile["time"]);
		}

		if(is_int($account)){
			// load data
			Account::getAccount($account);
		}

		$favorite = new Favorite($id);

		$favorite->id = $id;
		$favorite->account = $account;
		$favorite->skinFile = $skinFile;
		$favorite->time = $time;
		$favorite->exists = true;

		$favorite->saveToCache();

		return $favorite;
	}

	/**
	 * @access private
	 * @var int $id The favorite ID
	 */
	private $id;

	/**
	 * @access private
	 * @var int $account The account ID
	 */
	private $account;

	/**
	 * @access private
	 * @var int $skinFile The skin file ID
	 */
	private $skinFile;

	/**
	 * @access private
	 * @var string $time The creation timestamp
	 */
	private $time;

	/**
	 * @access private
	 * @var bool Whether this account exists, used for object construction.
	 */
	private $exists;

	/**
	 * Constructor
	 * 
	 * @access public
	 * @param int $id The favorite ID
	 */
	public function __construct($id){
		$this->id = $id;
	}

	/**
	 * @access public
	 * @return int The favorite ID.
	 */
	public function getId(){
		return $this->id;
	}

	/**
	 * @access public
	 * @return int The account ID.
	 */
	public function getAccountID(){
		return $this->account;
	}

	/**
	 * @access public
	 * @return Account The account object, fetched from the account ID.
	 */
	public function getAccount(){
		return Account::getAccount($this->account);
	}

	/**
	 * @access public
	 * @return int The skin file ID.
	 */
	public function getSkinFileID(){
		return $this->skinFile;
	}

	/**
	 * @access public
	 * @return SkinFile The skin file object, fetched from the skin file ID.
	 */
	public function getSkinFile(){
		return SkinFile::getSkinFileFromID($this->skinFile);
	}

	/**
	 * @access public
	 * @return string The creation timestamp.
	 */
	public function getTime(){
		return $this->time;
	}

	/**
	 * @access public
	 * @return bool True if the account is valid and exists in the database.
	 */
	public function exists(){
		return $this->exists;
	}

	/**
	 * Deletes this favorite from the database and the cache.
	 * 
	 * @access public
	 */
	public function delete(){
		$mysqli = \Database::Instance()->get();

		$stmt = $mysqli->prepare("DELETE FROM `skinhistory_favorites` WHERE `id` = ?");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$stmt->close();

		\CacheHandler::deleteFromCache("favorite_" . $this->id);
	}

	/**
	 * Saves this favorite to the cache.
	 *
	 * @access public
	 */
	public function saveToCache(){
		$cacheName = "favorite_" . $this->id;

		if($this->exists == true){
			\CacheHandler::setToCache($cacheName,$this,20*60);
		}
	}
}