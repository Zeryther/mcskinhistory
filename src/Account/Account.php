<?php

namespace SkinHistory\Account;

use SkinHistory\Skin\SkinFile;


/**
 * Represents a MCSkinHistory account
 * 
 * @package Account
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class Account {
	/**
	 * Gets a MCSkinHistory account by it's ID.
	 * 
	 * @access public
	 * @param int $id The ID of the account.
	 * @return Account|null The account object, null if the account could not be found.
	 */
	public static function getAccount($id){
		$cacheName = "account_" . $id;

		if(\CacheHandler::existsInCache($cacheName)){
			return \CacheHandler::getFromCache($cacheName);
		} else {
			$account = new Account($id);
			if($account->exists){
				return $account;
			} else {
				return null;
			}
		}
	}

	/**
	 * Registers an account with the provided data, updates the data if the account already exists.
	 * 
	 * @access public
	 * @param int $id The account ID.
	 * @param string $username The account username.
	 * @param string $email The account email.
	 * @param string $token The Gigadrive API token for this account.
	 */
	public static function registerAccount($id,$username,$email,$token){
		$mysqli = \Database::Instance()->get();
		$account = Account::getAccount($id);

		if($account == null){
			$stmt = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_accounts` (`id`,`username`,`email`,`token`) VALUES(?,?,?,?);");
			$stmt->bind_param("isss",$id,$username,$email,$token);
			$stmt->execute();
			$stmt->close();

			Account::getAccount($id); // cache data after registering
		} else {
			$stmt = $mysqli->prepare("UPDATE `skinhistory_accounts` SET `username` = ?, `email` = ?, `token` = ? WHERE `id` = ?");
			$stmt->bind_param("sssi",$username,$email,$token,$id);
			$stmt->execute();
			$stmt->close();

			$account->setUsername($username);
			$account->setEmail($email);
			$account->saveToCache();
		}
	}

	/**
	 * @access private
	 * @var int $id The ID of this account.
	 */
	private $id;

	/**
	 * @access private
	 * @var string $username The username of this account.
	 */
	private $username;

	/**
	 * @access private
	 * @var string $email The email of this account.
	 */
	private $email;

	/**
	 * @access private
	 * @var string $token The Gigadrive API token for this account.
	 */
	private $token;

	/**
	 * @access private
	 * @var int|null $legacyAccount The legacy account ID of this account, null if no legacy account has been linked.
	 */
	private $legacyAccount;

	/**
	 * @access private
	 * @var string|null $primaryPlayer The UUID of the primary player of this account, null if there is no primary player to this account.
	 */
	private $primaryPlayer;

	/**
	 * @access private
	 * @var string $registerDate The registration date of this account.
	 */
	private $registerDate;

	/**
	 * @access private
	 * @var array[]|null $linkedAccounts The linked player data of this account, null if it has not been fetched yet.
	 */
	private $linkedAccounts;

	/**
	 * @access private
	 * @var Favorite[]|null $favorites The favorites of this account, null if they have not been fetched yet.
	 */
	private $favorites;

	/**
	 * @access private
	 * @var bool Whether this account exists, used for object construction.
	 */
	private $exists;

	/**
	 * Constructor.
	 * 
	 * @access private
	 * @param int $id The account ID.
	 */
	private function __construct($id){
		$mysqli = \Database::Instance()->get();
		$this->id = $id;

		$stmt = $mysqli->prepare("SELECT * FROM `skinhistory_accounts` WHERE `id` = ?");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows){
			$row = $result->fetch_assoc();

			$this->id = $row["id"];
			$this->username = $row["username"];
			$this->email = $row["email"];
			$this->token = $row["token"];
			$this->legacyAccount = $row["legacyAccount"];
			$this->primaryPlayer = $row["primaryPlayer"];
			$this->registerDate = $row["register_date"];

			$this->exists = true;
			$this->saveToCache();
		}

		$stmt->close();
	}

	/**
	 * @access public
	 * @return int The ID of this account.
	 */
	public function getID(){
		return $this->id;
	}

	/**
	 * @access public
	 * @return string The username of this account.
	 */
	public function getUsername(){
		return $this->username;
	}

	/**
	 * @access private
	 */
	public function setUsername($username){
		$this->username = $username;
	}

	/**
	 * @access public
	 * @return string The email of this account.
	 */
	public function getEmail(){
		return $this->email;
	}

	/**
	 * @access private
	 */
	public function setEmail($email){
		$this->email = $email;
	}

	/**
	 * @access public
	 * @return string The Gigadrive API of this account.
	 */
	public function getToken(){
		return $this->token;
	}

	/**
	 * @access private
	 */
	public function setToken($token){
		$this->token = $token;
	}

	/**
	 * @access public
	 * @return int|null The account ID of the legacy account of this account, null if none is associated.
	 */
	public function getLegacyAccount(){
		return $this->legacyAccount;
	}

	/**
	 * @access private
	 */
	public function setLegacyAccount($legacyAccount){
		$this->legacyAccount = $legacyAccount;

		$mysqli = \Database::Instance()->get();
		$stmt = $mysqli->prepare("UPDATE `skinhistory_accounts` SET `legacyAccount` = ? WHERE `id` = ?");
		$stmt->bind_param("ii",$legacyAccount,$this->id);
		$stmt->execute();
		$stmt->close();

		$this->saveToCache();
	}

	/**
	 * @access public
	 * @return string|null The UUID of the primary player of this account, null if there is no primary player.
	 */
	public function getPrimaryPlayer(){
		$linkedAccounts = $this->getLinkedAccounts();

		if((is_null($linkedAccounts)) || (!is_null($this->primaryPlayer) && !array_key_exists($this->primaryPlayer,$linkedAccounts))){
			$this->setPrimaryPlayer(null);
		}

		return $this->primaryPlayer;
	}

	/**
	 * @access private
	 */
	public function setPrimaryPlayer($player){
		$linkedAccounts = $this->getLinkedAccounts();

		if(is_null($player) || (!is_null($linkedAccounts) && array_key_exists($player,$linkedAccounts))){
			$this->primaryPlayer = $player;

			$mysqli = \Database::Instance()->get();

			$stmt = $mysqli->prepare("UPDATE `skinhistory_accounts` SET `primaryPlayer` = ? WHERE `id` = ?");
			$stmt->bind_param("si",$this->primaryPlayer,$this->id);
			if($stmt->execute()){
				$this->saveToCache();
			}
			$stmt->close();
		}
	}

	/**
	 * @access public
	 * @return string The registration date of this account.
	 */
	public function getRegisterDate(){
		return $this->registerDate;
	}

	/**
	 * @access private
	 */
	public function setRegisterDate($registerDate){
		$this->registerDate = $registerDate;

		$mysqli = \Database::Instance()->get();
		$stmt = $mysqli->prepare("UPDATE `skinhistory_accounts` SET `register_date` = ? WHERE `id` = ?");
		$stmt->bind_param("si",$registerDate,$this->id);
		$stmt->execute();
		$stmt->close();

		$this->saveToCache();
	}

	/**
	 * @access public
	 * @return array[] The linked account data of this account.
	 */
	public function getLinkedAccounts(){
		if($this->linkedAccounts == null){
			$this->reloadLinkedAccounts();
		}

		return $this->linkedAccounts;
	}

	/**
	 * @access private
	 */
	public function reloadLinkedAccounts(){
		$mysqli = \Database::Instance()->get();
		$this->linkedAccounts = array();

		$stmt = $mysqli->prepare("SELECT * FROM `skinhistory_linkedPlayers` WHERE `account` = ?");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$result = $stmt->get_result();

		if($result->num_rows){
			while($row = $result->fetch_assoc()){
				if(!array_key_exists($row["uuid"],$this->linkedAccounts)){
					$this->linkedAccounts[$row["uuid"]] = array("uuid" => $row["uuid"],"time" => $row["time"]);
				}
			}
		}

		$stmt->close();
		$this->saveToCache();
	}

	/**
	 * @return Favorite[] The favorites of this account.
	 */
	public function getFavorites(){
		if($this->favorites == null){
			$this->reloadFavorites();
		}

		return $this->favorites;
	}

	/**
	 * @access private
	 */
	public function reloadFavorites(){
		$mysqli = \Database::Instance()->get();
		$this->favorites = array();

		$stmt = $mysqli->prepare("SELECT * FROM `skinhistory_favorites` WHERE `account` = ? ORDER BY `time` DESC");
		$stmt->bind_param("i",$this->id);
		$stmt->execute();
		$result = $stmt->get_result();
		if($result->num_rows){
			while($row = $result->fetch_assoc()){
				array_push($this->favorites,Favorite::getFavoriteFromData($row["id"],$row["account"],$row["skinFile"],$row["time"]));
			}
		}
		$stmt->close();
		$this->saveToCache();
	}

	/**
	 * Creates a favorite with the associated skin file id and saves it to the database and the cache.
	 * 
	 * @access public
	 * @param int $skinFileID The ID of the skin file to favorite.
	 */
	public function addFavorite($skinFileID){
		$skinFile = SkinFile::getSkinFileFromID($skinFileID);

		if($skinFile != null){
			if($this->hasFavorite($skinFile) == false){
				$favorite = Favorite::createFavorite($this->id,$skinFileID);
				if($favorite != null && $favorite > 0){
					$fav = Favorite::getFavoriteById($favorite);
					array_unshift($this->favorites,$fav);
					$this->saveToCache();
				}
			}
		}
	}

	/**
	 * Removes a favorite with the associated skin file id from the database and the cache.
	 * 
	 * @access public
	 * @param int $skinFileID The ID of the skin file to unfavorite.
	 */
	public function removeFavorite($skinFileID){
		if($this->hasFavorite($skinFileID)){
			$newFavorites = array();

			foreach($this->getFavorites() as $favorite){
				if($favorite->getSkinFileID() != $skinFileID){
					array_push($newFavorites,$favorite);
				} else {
					$favorite->delete();
				}
			}

			$this->favorites = $newFavorites;
			$this->saveToCache();
		}
	}

	/**
	 * @access public
	 * @param int $skinFileID The ID of the skin file to check.
	 * @return bool True if the account has a favorite with the passed skin file ID.
	 */
	public function hasFavorite($skinFileID){
		foreach($this->getFavorites() as $favorite){
			if($favorite->getSkinFileID() == $skinFileID){
				return true;
			}
		}

		return false;
	}

	/**
	 * @access public
	 * @return bool True if the account is valid and exists in the database.
	 */
	public function exists(){
		return $this->exists;
	}

	/**
	 * Saves this account object to the cache for future retrieval for 60 seconds.
	 * 
	 * @access public
	 */
	public function saveToCache(){
		$cacheName = "account_" . $this->id;

		if($this->exists){
			\CacheHandler::setToCache($cacheName,$this,60);
		}
	}
}