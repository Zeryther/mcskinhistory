<?php

namespace SkinHistory\Util;

/**
 * Represents an object used for rendering a disqus thread
 * 
 * @package Util
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class DisqusThread {
	private $pageURL;
	private $pageIdentifier;

	public function __construct($pageURL = NULL, $pageIdentifier = NULL){
		$this->pageURL;
		$this->pageIdentifier;
	}

	public function getPageURL(){
		return $this->pageURL;
	}

	public function getPageIdentifier(){
		return $this->pageIdentifier;
	}

	public function setPageURL($pageURL){
		$this->pageURL = $pageURL;
	}

	public function setPageIdentifier($pageIdentifier){
		$this->pageIdentifier = $pageIdentifier;
	}

	public function render(){
		if(!is_null($this->pageURL) && !is_null($this->pageIdentifier) && !is_null(DISQUS_EMBED_URL) && !empty($this->pageURL) && !empty($this->pageIdentifier) && !empty(DISQUS_EMBED_URL)){
			echo '<div id="disqus_thread"></div><script>var disqus_config = function () {this.page.url = "' . $this->pageURL . '"; this.page.identifier = "' . $this->pageIdentifier . '";};(function() {var d = document, s = d.createElement(\'script\');s.src = \'' . DISQUS_EMBED_URL . '\';s.setAttribute(\'data-timestamp\', +new Date());(d.head || d.body).appendChild(s);})();</script><noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>';
		} else {
			throw new IllegalArgumentException("Missing variables, check pageURL, pageIdentifier and DISQUS_EMBED_URL!");
		}
	}
}