<?php

namespace SkinHistory\Util;

use SkinHistory\Account\Account;
use SkinHistory\Player\Name\MinecraftName;
use SkinHistory\Player\Name\MinecraftNameHolder;
use SkinHistory\Player\Player;
use Gigadrive\i18n\i18n;

define("USER_AGENT_BOT_NAME","MCSkinHistory");

define("DEFAULT_DESCRIPTION","Do you want to find your old Minecraft skins, LabyMod capes or Optifine capes? Here's your place to go!");
define("DEFAULT_TWITTER_IMAGE","https://pbs.twimg.com/profile_images/819307463170211841/FvYYjVHb.jpg");

define("SKIN_TYPE_MOJANG","mojang");
define("SKIN_TYPE_OPTIFINE","optifine");
define("SKIN_TYPE_LABYMOD","labymod");

define("SKIN_FILE_LIST_TYPE_NEW","NEW");
define("SKIN_FILE_LIST_TYPE_POPULAR","POPULAR");
define("SKIN_FILE_LIST_TYPE_BROWSE","BROWSE");

define("NAV_HOME","nHOME");
define("NAV_SKINS","nSKINS");
define("NAV_CAPES","nCAPES");
define("NAV_FAQ","nFAQ");
define("NAV_DONATE","nDONATE");
define("NAV_ACCOUNT","nACCOUNT");
define("NAV_SERVERS","nSERVERS");

define("ACCOUNT_NAV_ACCOUNT","anACCOUNT");
define("ACCOUNT_NAV_FAVORITES","anFAVORITES");
define("ACCOUNT_NAV_LINKED","anLINKED");

define("AD_TYPE_LEADERBOARD","adLeaderboard");
define("AD_TYPE_HORIZONTAL","adLeaderboard");
define("AD_TYPE_BLOCK","adBlock");
define("AD_TYPE_VERTICAL","adVertical");

define("ALERT_TYPE_INFO","info");
define("ALERT_TYPE_WARNING","warning");
define("ALERT_TYPE_DANGER","danger");
define("ALERT_TYPE_SUCCESS","success");
define("ALERT_TYPE_SECONDARY","secondary");
define("ALERT_TYPE_LIGHT","light");
define("ALERT_TYPE_PRIMARY","primary");

/**
 * Utility functions
 * 
 * @package Util
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class Util {
	/**
	 * Sets a cookie for a specific amount of days to the user's browser
	 * 
	 * @access public
	 * @param string $name The cookie name
	 * @param string $value The cookie value
	 * @param int $days The time in days the cookie will last for
	 */
	public static function setCookie($name,$value,$days){
		setcookie($name,$value,time()+(60*60*24)*$days);
	}

	/**
	 * Removes a cookie from the user's browser
	 * 
	 * @access public
	 * @param string $name The cookie name
	 */
	public static function unsetCookie($name){
		setcookie($name,"",time()-3600);
	}

	public static function isDevMode(){
		return (isset($_SERVER["HTTP_HOST"]) && (explode(":",$_SERVER["HTTP_HOST"])[0] == "localhost" || explode(":",$_SERVER["HTTP_HOST"])[0] == "127.0.0.1"));
	}

	/**
     * Gets whether the user is currently logged in.
	 * 
	 * @access public
     * @return bool
     */
	public static function isLoggedIn(){
		if(isset($_SESSION["id"]) && isset($_SESSION["username"])){
			return true;
		} else {
			return false;
		}
	}

	/**
     * Gets the currently logged in user as an object, returns null if the user is not logged in
	 * 
	 * @access public
     * @return Account
     */
	public static function getCurrentAccount(){
		if(Util::isLoggedIn()){
			return Account::getAccount($_SESSION["id"]);
		} else {
			return null;
		}
	}

	public static function tryLoggingName($uuid,$name,$timeChanged){
		$mysqli = \Database::Instance()->get();
		$uuid = str_replace("-","",$uuid);

		$stmt = $mysqli->prepare("SELECT * FROM `skinhistory_usedNames` WHERE `uuid` = ? AND `name` = ? AND `timeChanged` = ?");
		$stmt->bind_param("sss",$uuid,$name,$timeChanged);
		$stmt->execute();

		if(!$stmt->fetch()){
			$insert = $mysqli->prepare("INSERT IGNORE INTO `skinhistory_usedNames` (`uuid`,`name`,`timeChanged`) VALUES(?,?,?);");
			$insert->bind_param("sss",$uuid,$name,$timeChanged);
			$insert->execute();
			$insert->close();
		}

		$stmt->close();
	}

	/**
     * Returns HTML Code that is converted to "x minutes ago" format via timeago.js
	 * 
	 * @access public
     * @param string $timestamp
     * @return string
     */
	public static function timeago($timestamp){
		$str = strtotime($timestamp);

		$timestamp = date("Y",$str) . "-" . date("m",$str) . "-" . date("d",$str) . "T" . date("H",$str) . ":" . date("i",$str) . ":" . date("s",$str) . "Z";

		return '<time class="timeago" datetime="' . $timestamp . '" title="' . date("d",$str) . "." . date("m",$str) . "." . date("Y",$str) . " " . date("H",$str) . ":" . date("i",$str) . ":" . date("s",$str) . ' UTC">' . $timestamp . '</time>';
	}

	public static function storeTexture($path){
		try {
			$curl = curl_init();
			curl_setopt_array($curl,array(
				CURLOPT_URL => "https://gigadrivegroup.com/api/v3/file",
				CURLOPT_POST => 1,
				CURLOPT_POSTFIELDS => array(
					"secret" => GIGADRIVE_API_KEY,
					"data" => base64_encode(file_get_contents(realpath($path)))
				),
				CURLOPT_RETURNTRANSFER => 1
			));
	
			$result = curl_exec($curl);

			if($result === false){
				throw new Exception(curl_error($curl),curl_errno($curl));
			}

			curl_close($curl);

			$fileData = json_decode($result,true);
			if(isset($fileData["success"]) && isset($fileData["file"]) && isset($fileData["file"]["url"])){
				return $fileData["file"]["url"];
			}
	
			return null;
		} catch(Exception $e){
			return null;
		}
	}

	/**
     * Checks whether a string contains another string
	 * 
	 * @access public
     * @param string $string The full string
	 * @param string $check The substring to be checked
	 * @return bool
     */
	public static function contains($string,$check){
		return strpos($string,$check) !== false;
	}

	/**
     * Limits a string to a specific length and adds "..." to the end if needed
	 * 
	 * @access public
     * @param string $string
	 * @param int $length
	 * @param bool $addDots
     * @return string
     */
	public static function limitString($string,$length,$addDots = false){
		if(strlen($string) > $length)
			$string = substr($string,0,($addDots ? $length-3 : $length)) . ($addDots ? "..." : "");

		return $string;
	}

	/**
	 * Returns a sanatized string that avoids prepending or traling spaces and XSS attacks
	 * 
	 * @access public
	 * @param string $string The string to sanatize
	 * @return string
	 */
	public static function sanatizeString($string){
		return trim(htmlentities($string));
	}

	/**
	 * Proxies an image.
	 * 
	 * @access public
	 * @param string $url The image URL.
	 * @return string
	 */
	public static function proxyImage($url){
		$d = isset($_SERVER["HTTPS"]) ? "s" : (isset($_SERVER["HTTP_CF_VISITOR"]) && Util::contains($_SERVER["HTTP_CF_VISITOR"],"https") ? "s" : "");
		//return "http" . $d . "://" . $_SERVER["HTTP_HOST"] . "/proxy?url=" . urlencode($url);
		//return str_replace("http://","https://",$url);
		return "https://images.weserv.nl/?url=" . urlencode($url);
	}

	public static function getNameInfoResults($query){
		$cacheName = "nameInfoResults_" . $query;

		if(\CacheHandler::existsInCache($cacheName)){
			return \CacheHandler::getFromCache($cacheName);
		} else {
			$mysqli = \Database::Instance()->get();
			
			$player = Player::getPlayer($query);

			if($player != null){
				$player->getNameHistory();
			}

			$results = array();
			$used = array();

			$stmt = $mysqli->prepare("SELECT n.uuid,b.name,b.timeChanged,b.id FROM `skinhistory_usedNames` AS n INNER JOIN `skinhistory_usedNames` AS b ON n.uuid = b.uuid WHERE n.name = ? ORDER BY b.timeChanged DESC");
			$stmt->bind_param("s",$query);
			$stmt->execute();
			$result = $stmt->get_result();

			if($result->num_rows){
				while($row = $result->fetch_assoc()){
					if(!in_array($row["id"],$used)){
						if(isset($results[$row["uuid"]])){
							$r = $results[$row["uuid"]];
							$names = $r->getNames();
							$time = null;
							if($row["timeChanged"] != 0) $time = gmdate("Y-m-d H:i:s",$row["timeChanged"]/1000);
							array_push($names,new MinecraftName($row["uuid"],$row["name"],$time));
	
							$holder = new MinecraftNameHolder($row["uuid"],$names);
	
							$results[$row["uuid"]] = $holder;
						} else {
							$time = null;
							if($row["timeChanged"] != 0) $time = gmdate("Y-m-d H:i:s",$row["timeChanged"]/1000);
							$names = array(new MinecraftName($row["uuid"],$row["name"],$time));
							$results[$row["uuid"]] = new MinecraftNameHolder($row["uuid"],$names);
						}
	
						array_push($used,$row["id"]);
					}
				}
			}

			$stmt->close();

			if($player != null && isset($results[$player->getUUID()])){
				$results = array($player->getUUID() => $results[$player->getUUID()]) + $results;
			}

			\CacheHandler::setToCache($cacheName,$results,10*60);

			return $results;
		}
	}

	/**
	 * @access public
	 * @return string The HTML embed code of the newest tweet.
	 */
	public static function renderNewestTweet(){
		// TODO: automatically fetch newest tweet
		echo '<blockquote class="twitter-tweet" data-lang="' . i18n::getCurrentLanguage()->getFlagIconCode() . '"><p lang="' . i18n::getCurrentLanguage()->getFlagIconCode() . '" dir="ltr">If you want to contribute to MCSkinHistory saving more player skins and capes, install our new <a href="https://twitter.com/LabyMod?ref_src=twsrc%5Etfw">@LabyMod</a> addon!<br><br>⬇️ Download here: <a href="https://t.co/PbScNgj8c4">https://t.co/PbScNgj8c4</a></p>&mdash; MCSkinHistory.com (@mcskinhistory) <a href="https://twitter.com/mcskinhistory/status/1068374884777172992?ref_src=twsrc%5Etfw">November 30, 2018</a></blockquote>
		<script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>';
	}

	/**
	 * @access public
	 * @param string $type
	 * @return string The HTML code of the ad banner.
	 */
	public static function renderAd($type){
		if($type == AD_TYPE_LEADERBOARD){
			echo '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<ins class="adsbygoogle"
				style="display:block"
				data-ad-client="ca-pub-6156128043207415"
				data-ad-slot="1055807482"
				data-ad-format="auto"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>';
		} else if($type == AD_TYPE_VERTICAL){
			echo '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<ins class="adsbygoogle"
				 style="display:inline-block;width:120px;height:600px"
				 data-ad-client="ca-pub-6156128043207415"
				 data-ad-slot="1788401303"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
			</script>';
		} else if($type == AD_TYPE_BLOCK){
			echo '<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
			<ins class="adsbygoogle"
				style="display:inline-block;width:336px;height:280px"
				data-ad-client="ca-pub-6156128043207415"
				data-ad-slot="7069637483"></ins>
			<script>
			(adsbygoogle = window.adsbygoogle || []).push({});
		</script>';
		}
	}

	/**
     * Returns HTML code for a bootstrap alert
	 * 
	 * @access public
     * @param string $id A string that identifies this alert, mainly used for saving dismissal
	 * @param string $text The text displayed in this alert (may contain HTML)
	 * @param string $type Use ALERT_TYPE_* constants
	 * @param bool $dismissible If true, an icon will be displayed to dismiss this alert
	 * @param bool $saveDismiss If true, a cookie will be set on dismissal and this alert won't be shown again
     * @return string HTML code for the alert
     */
	public static function createAlert($id,$text,$type = ALERT_TYPE_INFO,$dismissible = FALSE,$saveDismiss = FALSE){
		$cookieName = "registeredAlert" . $id;

		if($dismissible == false){
			echo '<div id="registeredalert' . $id . '" class="alert alert-' . $type . '">' . $text . '</div>';
		} else {
			if($saveDismiss == false || ($saveDismiss == true && !isset($_COOKIE[$cookieName]))){
				$d = $saveDismiss == true ? ' onClick="saveDismiss(\'' . $id . '\');"' : "";
				echo '<div id="registeredalert' . $id . '" class="text-left alert alert-dismissible alert-' . $type . '"><button id="registeredalertclose' . $id . '" type="button" class="close" data-dismiss="alert"' . $d . '>&times;</button>' . $text . '</div>';
			}
		}
	}

	/**
     * Returns a random string of characters
	 * 
	 * @access public
     * @param int $length The maximum length of the string (the actual length will be something between this number and the half of it)
     * @return array
     */
	public static function getRandomString($length = 16) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < rand(4,$length); $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}

	/**
     * Gets the current user's IP address
	 * 
	 * @access public
     * @return string The IP address (might be an IPv6 address)
     */
	public static function getIP(){
		$ip = "undefined";
		
		if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
			$ip = $_SERVER['HTTP_CLIENT_IP'];
		} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
			$ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
		} else {
			$ip = $_SERVER['REMOTE_ADDR'];
		}
		
		return $ip;
	}

	/**
     * Removes outdated files and folders from the tmp folder (this check is being done automatically, there is no need to call this function!)
	 * 
	 * @access public
     */
	public static function cleanupTempFolder(){
		$files = glob($_SERVER["DOCUMENT_ROOT"] . "/tmp/*");
		$now = time();

		foreach($files as $file){
			if(is_file($file) && basename($file) != ".keep"){
				if($now - filemtime($file) >= 60*60*24){
					unlink($file);
				}
			}
		}
	}

	/**
	 * Gets whether a string starts with another
	 * 
	 * @access public
	 * @param string $string The string in subject
	 * @param string $start The string to be checked whether it is the start of $string
	 * @param bool $ignoreCase If true, the case of the strings won't affect the result
	 * @return bool
	 */
	public static function startsWith($string,$start,$ignoreCase = false){
		if(strlen($start) <= strlen($string)){
			if($ignoreCase == true){
				return substr($string,0,strlen($start)) == $start;
			} else {
				return strtolower(substr($string,0,strlen($start))) == strtolower($start);
			}
		} else {
			return false;
		}
	}

	/**
	 * @access public
	 * @param int $skinFileID
	 * @param string $float
	 * @return string The HTML code for a favorite button.
	 */
	public static function renderFavoriteButton($skinFileID,$float = "left"){
		if(Util::isLoggedIn()){
			$account = Util::getCurrentAccount();

			if($account != null){
				$color = $account->hasFavorite($skinFileID) == true ? "FF0000" : "ADB5BD";
				$tooltip = $account->hasFavorite($skinFileID) == true ? "Remove from favorites" : "Add to favorites";
				echo '<div class="float-' . $float . '" data-toggle="tooltip" title="' . $tooltip . '" style="cursor: pointer; z-index: 999999;" onclick="toggleFavorite(this,' . $skinFileID . ');">';
				echo '<i class="fas fa-heart" style="font-size: 24px; color: #' . $color . ';"></i>';
				echo '</div>';
			}
		}
	}

	/**
	 * @access public
	 * @return bool If true, the request is a valid cronjob request.
	 */
	public static function validateCronjob(){
		if(isset($_GET["secret"]) && $_GET["secret"] == CRONJOB_SECRET){
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @access public
	 * @param string $d The domain
	 * @return bool If true, the passed domain is valid.
	 */
	public static function isValidDomain($d){
		$tlds = "/^(?:[-A-Za-z0-9]+\.)+[A-Za-z]{2,6}$/";

		return preg_match($tlds, $d) ? true : false;
	}

	/**
	 * @access public
	 * @param string $domain
	 * @return string The passed domain, stripped off it's subdomain.
	 */
	// https://gist.github.com/pocesar/5366899
	public static function stripSubdomain($domain, $debug = false){
		$original = $domain = strtolower($domain);
		if (filter_var($domain, FILTER_VALIDATE_IP)) { return $domain; }
		$debug ? print('<strong style="color:green">&raquo;</strong> Parsing: '.$original) : false;
		$arr = array_slice(array_filter(explode('.', $domain, 4), function($value){
			return $value !== 'www';
		}), 0); //rebuild array indexes
		if (count($arr) > 2)
		{
			$count = count($arr);
			$_sub = explode('.', $count === 4 ? $arr[3] : $arr[2]);
			$debug ? print(" (parts count: {$count})") : false;
			if (count($_sub) === 2) // two level TLD
			{
				$removed = array_shift($arr);
				if ($count === 4) // got a subdomain acting as a domain
				{
					$removed = array_shift($arr);
				}
				$debug ? print("<br>\n" . '[*] Two level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
			}
			elseif (count($_sub) === 1) // one level TLD
			{
				$removed = array_shift($arr); //remove the subdomain
				if (strlen($_sub[0]) === 2 && $count === 3) // TLD domain must be 2 letters
				{
					array_unshift($arr, $removed);
				}
				else
				{
					// non country TLD according to IANA
					$tlds = array(
						'aero',
						'arpa',
						'asia',
						'biz',
						'cat',
						'com',
						'coop',
						'edu',
						'gov',
						'info',
						'jobs',
						'mil',
						'mobi',
						'museum',
						'name',
						'net',
						'org',
						'post',
						'pro',
						'tel',
						'travel',
						'xxx',
					);
					if (count($arr) > 2 && in_array($_sub[0], $tlds) !== false) //special TLD don't have a country
					{
						array_shift($arr);
					}
				}
				$debug ? print("<br>\n" .'[*] One level TLD: <strong>'.join('.', $_sub).'</strong> ') : false;
			}
			else // more than 3 levels, something is wrong
			{
				for ($i = count($_sub); $i > 1; $i--)
				{
					$removed = array_shift($arr);
				}
				$debug ? print("<br>\n" . '[*] Three level TLD: <strong>' . join('.', $_sub) . '</strong> ') : false;
			}
		}
		elseif (count($arr) === 2)
		{
			$arr0 = array_shift($arr);
			if (strpos(join('.', $arr), '.') === false
				&& in_array($arr[0], array('localhost','test','invalid')) === false) // not a reserved domain
			{
				$debug ? print("<br>\n" .'Seems invalid domain: <strong>'.join('.', $arr).'</strong> re-adding: <strong>'.$arr0.'</strong> ') : false;
				// seems invalid domain, restore it
				array_unshift($arr, $arr0);
			}
		}
		$debug ? print("<br>\n".'<strong style="color:gray">&laquo;</strong> Done parsing: <span style="color:red">' . $original . '</span> as <span style="color:blue">'. join('.', $arr) ."</span><br>\n") : false;
		return join('.', $arr);
	}
}