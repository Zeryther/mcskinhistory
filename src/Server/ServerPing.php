<?php

namespace SkinHistory\Server;

use SkinHistory\Server\Server;
use xPaw\MinecraftPing;
use xPaw\MinecraftPingException;
use \Spirit55555\Minecraft\MinecraftColors;
use \Spirit55555\Minecraft\MinecraftJsonColors;

/**
 * Represents a ping to a Minecraft server, returning data
 * 
 * @package Server
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class ServerPing {
    /**
     * @return ServerPing
     */
    public static function executeServerPing($ip,$updateData = true){
        if(!Server::isValidIP($ip)) return null;

        if(\CacheHandler::existsInCache("serverPing_" . $ip)){
            return \CacheHandler::getFromCache("serverPing_" . $ip);
        }

        $ip = strtolower($ip);

        $ping = new ServerPing($ip,$updateData);
        $ping->doPing();

        return $ping;
    }

    /**
     * @access private
     * @var string $ip The IP of the pinged server.
     */
    private $ip;

    /**
     * @access private
     * @var string|null $icon The icon of the pinged server, null if there is no icon.
     */
    private $icon;

    /**
     * @access private
     * @var string $motd The motd of the pinged server
     */
    private $motd;

    /**
     * @access private
     * @var int $playersOnline The amount of players online on this server at the time of the ping.
     */
    private $playersOnline;

    /**
     * @access private
     * @var int $playersMax The maximum amount of players this server can hold at the time of the ping.
     */
    private $playersMax;

    /**
     * @access private
     * @var string $version The version string of this server at the time of the ping.
     */
    private $version;

    /**
     * @access private
     * @var bool $successful Whether the ping was successful.
     */
    private $successful = false;

    /**
     * @access private
     * @var string $date The date of this ping.
     */
    private $date;

    /**
     * @access private
     * @var int $hour The hour of this ping.
     */
    private $hour;

    /**
     * @access private
     */
    private $updateData;

    protected function __construct($ip,$updateData){
        $this->ip = $ip;
        $this->updateData = $updateData;
    }

    private function doPing(){
        if(Server::isValidIP($this->ip)){
            try {
                $ping = new MinecraftPing($this->ip,25565);
                $data = $ping->Query();

                if(isset($data["version"])){
                    if(isset($data["version"]["name"])){
                        $this->version = $data["version"]["name"];
                    }
                }

                if(isset($data["players"])){
                    if(isset($data["players"]["online"])){
                        if(is_int($data["players"]["online"])){
                            $this->playersOnline = $data["players"]["online"];
                        }
                    }

                    if(isset($data["players"]["max"])){
                        if(is_int($data["players"]["max"])){
                            $this->playersMax = $data["players"]["max"];
                        }
                    }

                    $this->successful = true;
                }

                if(isset($data["description"])){
                    if(is_string($data["description"])){
                        // motd is passed as string
                        $this->motd = $data["description"];
                    } else if(is_array($data["description"])){
                        // motd is passed as text component object
                        $this->motd = MinecraftJsonColors::convertToLegacy($data["description"]);
                    }

                    if(!is_null($this->motd)){
                        $this->motd = str_replace("\u00A7","§",$this->motd);
                    }
                }

                if(isset($data["favicon"]) && is_string($data["favicon"])){
                    $this->icon = $data["favicon"];
                }

                $this->date = gmdate("Y-m-d");
                $this->hour = (int)gmdate("H");

                // insert ping into database
                $mysqli = \Database::Instance()->get();

                $stmt = $mysqli->prepare("INSERT INTO `skinhistory_serverpings` (`ip`,`icon`,`motd`,`players.online`,`players.max`,`version`,`successful`,`date`,`hour`) VALUES(?,?,?,?,?,?,?,?,?);");
                $stmt->bind_param("sssiisisi",$this->ip,$this->icon,$this->motd,$this->playersOnline,$this->playersMax,$this->version,$this->successful,$this->date,$this->hour);
                $stmt->execute();
                $stmt->close();

                \CacheHandler::setToCache("serverPing_" . $this->ip,$this,3*60);

                if($this->updateData){
                    $server = Server::getServer($this->ip);

                    if(!is_null($server)){
                        $server->updateData($this);
                    }
                }
            } catch(MinecraftPingException $e){
                $this->successful = false;
            } finally {
                if($ping){
                    $ping->Close();
                }
            }
        }
    }

    /**
     * @access public
     * @return string The IP of the pinged server.
     */
    public function getIP(){
        return $this->ip;
    }

    /**
     * @access public
     * @return string|null The icon of the pinged server, null if there is no icon.
     */
    public function getIcon(){
        return $this->icon;
    }

    /**
     * @access public
     * @return string The motd of the pinged server.
     */
    public function getMotd(){
        return $this->motd;
    }

    /**
     * @access public
     * @return string The motd of the pinged server with it's color codes converted to HTML.
     */
    public function getConvertedMotd(){
        if(!is_null($this->motd)){
            return MinecraftColors::convertToHTML($this->motd,true);
        }

        return null;
    }

    /**
     * @access public
     * @return int The amount of players online on this server at the time of the ping.
     */
    public function getPlayersOnline(){
        return $this->playersOnline;
    }

    /**
     * @access public
     * @return int The maximum amount of players this server can hold at the time of the ping.
     */
    public function getPlayersMax(){
        return $this->playersMax;
    }

    /**
     * @access public
     * @return string The version string of this server at the time of the ping.
     */
    public function getVersion(){
        return $this->version;
    }

    /**
     * @access public
     * @return bool Whether the ping was successful.
     */
    public function isSuccessful(){
        return $this->successful;
    }

    /**
     * @access public
     * @return string The date of this ping.
     */
    public function getDate(){
        return $this->date;
    }

    /**
     * @access public
     * @return int The hour of this ping.
     */
    public function getHour(){
        return $this->hour;
    }
}