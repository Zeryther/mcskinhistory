<?php

namespace SkinHistory\Server;

use SkinHistory\Util\Util;
use \Spirit55555\Minecraft\MinecraftColors;

/**
 * Represents a Minecraft server from MCSkinHistory's database
 * 
 * @package Server
 * @author Gigadrive (support@gigadrivegroup.com)
 * @copyright 2016-2018 Gigadrive
 * @link https://gigadrivegroup.com/dev/technologies
 */
class Server {
    /**
     * Gets a server by it's IP.
     * 
     * @param string $ip The server IP.
     * @param bool $allowChildSkipping If true, will return the parent server, if available.
     * @return Server|null The server object, null if the server could not be found.
     */
	public static function getServer($ip,$allowChildSkipping = true){
        if(!self::isValidIP($ip)) return null;
        $ip = strtolower($ip);

		$n = "server_" . strtolower($ip);

		if(\CacheHandler::existsInCache($n)){
			return \CacheHandler::getFromCache($n);
		} else {
            if($allowChildSkipping){
                if(Util::isValidDomain($ip)){
                    $stripped = Util::stripSubdomain($ip);
    
                    if($stripped != $ip){
                        $server = self::getServer($stripped,false);
    
                        if(!is_null($server)){
                            return $server;
                        }
                    }

                    $stripped = self::matchIPRegex($ip);

                    if($stripped != $ip){
                        $server = self::getServer($stripped,false);
        
                        if(!is_null($server)){
                            return $server;
                        }
                    }
                }
            }

            $server = new Server($ip);
            $server->load();

            return $server->exists == true ? $server : null;
		}
    }

    /**
     * Creates a server object from the passed data and saves it to the cache.
     * 
     * @access public
     * @param string $ip
     * @param string $name
     * @param string $icon
     * @param string $motd
     * @param int $playersOnline
     * @param int $playersMax
     * @param int $playerPeak
     * @param string $version
     * @param double $uptime
     * @param string $website
     * @param string $twitter
     * @param string $parent
     * @param bool $blacklisted
     * @param string $time
     * @return Server The server object.
     */
    public static function getServerFromData($ip,$name,$icon,$motd,$playersOnline,$playersMax,$playerPeak,$version,$uptime,$website,$twitter,$parent,$blacklisted,$time){
        $server = new Server($ip);

        $server->ip = $ip;
        $server->name = $name;
        $server->icon = $icon;
        $server->motd = $motd;
        $server->playersOnline = $playersOnline;
        $server->playersMax = $playersMax;
        $server->playerPeak = $playerPeak;
        $server->version = $version;
        $server->uptime = $uptime;
        $server->website = $website;
        $server->twitter = $twitter;
        $server->parent = $parent;
        $server->blacklisted = $blacklisted;
        $server->time = $time;

        $server->saveToCache();

        return $server;
    }

    /**
     * @return Server[] The currently trending servers.
     */
    public static function getTrendingServers(){
        $servers = [];

        $n = "trendingServers";
        if(\CacheHandler::existsInCache($n)){
            $servers = \CacheHandler::getFromCache($n);
        } else {
            $mysqli = \Database::Instance()->get();

            $stmt = $mysqli->prepare("SELECT s.ip,s.name,s.icon,s.motd,s.`players.online`,s.`players.max`,s.`players.peak`,s.version,s.uptime,s.website,s.twitter,s.parent,s.blacklisted,s.time FROM skinhistory_servers AS s WHERE `parent` IS NULL AND `blacklisted` = 0 ORDER BY `players.online` DESC LIMIT 10");
            if($stmt->execute()){
                $result = $stmt->get_result();

                if($result->num_rows > 0){
                    while($row = $result->fetch_assoc()){
                        array_push($servers,Server::getServerFromData(
                            $row["ip"],
                            $row["name"],
                            $row["icon"],
                            $row["motd"],
                            $row["players.online"],
                            $row["players.max"],
                            $row["players.peak"],
                            $row["version"],
                            $row["uptime"],
                            $row["website"],
                            $row["twitter"],
                            $row["parent"],
                            $row["blacklisted"],
                            $row["time"]
                        ));
                    }
                }

                \CacheHandler::setToCache($n,$servers,3*60);
            }
            $stmt->close();
        }

        return $servers;
    }
    
    /**
     * @access public
     * @param string $ip The server IP
     * @return bool If true, the passed IP is a valid server IP.
     */
    public static function isValidIP($ip){
        if(!is_null($ip)){
            if(strlen($ip) >= 3 && strlen($ip) <= 64){
                if(Util::isValidDomain($ip) || filter_var($ip,FILTER_VALIDATE_IP,FILTER_FLAG_IPV4)){
                    return true;
                }
            }
        }

        return false;
    }

    /**
     * Matches a server IP against the regex database and returns a match if available.
     * 
     * @access public
     * @param string $ip The server IP.
     * @return string The matched server IP, returns the passed IP if no match was found.
     */
    public static function matchIPRegex($ip){
        if(self::isValidIP($ip)){
            $mysqli = \Database::Instance()->get();

            $stmt = $mysqli->prepare("SELECT `server` FROM `skinhistory_server_regex` WHERE ? RLIKE `regex`");
            $stmt->bind_param("s",$ip);
            if($stmt->execute()){
                $result = $stmt->get_result();

                if($result->num_rows){
                    $row = $result->fetch_assoc();

                    $ip = $row["server"];
                }
            }
            $stmt->close();
        }

        return $ip;
    }

    /**
     * @access private
     * @var string $ip The IP address of this server.
     */
    private $ip;

    /**
     * @access private
     * @var string|null $name The custom name of this server, null if there is no custom name.
     */
    private $name;

    /**
     * @access private
     * @var string|null $icon The icon of this server, null if there is no icon.
     */
    private $icon;

    /**
     * @access private
     * @var string $motd The motd of this server.
     */
    private $motd;

    /**
     * @access private
     * @var int $playersOnline The players currently online on this server.
     */
    private $playersOnline;

    /**
     * @access private
     * @var int $playersMax The maximum amount of players this server holds.
     */
    private $playersMax;

    /**
     * @access private
     * @var int $playerPeak The highest amount of players that was ever online on this server.
     */
    private $playerPeak;

    /**
     * @access private
     * @var string $version The version string of this server.
     */
    private $version;

    /**
     * @access private
     * @var double $uptime The uptime percentage of this server.
     */
    private $uptime;

    /**
     * @access private
     * @var string|null $website The website of this server, null if none has been set.
     */
    private $website;

    /**
     * @access private
     * @var string|null $twitter The twitter handle of this server, null if none has been set.
     */
    private $twitter;

    /**
     * @access private
     * @var string|null $parent The IP of the parent server of this server, null if none has been set.
     */
    private $parent;

    /**
     * @access private
     * @var bool $blacklisted If true, the server has been blacklisted and will not be shown on the server lists.
     */
    private $blacklisted;

    /**
     * @access private
     * @var string $time The timestamp when this server was added to the database.
     */
    private $time;

    /**
     * @access private
     * @var string[]|null The children IPs of this server, null if the children have not been loaded yet.
     */
    private $children;
    
    /**
	 * @access private
	 * @var bool Whether this server exists, used for object construction.
	 */
    private $exists = false;

    /**
     * Constructor
     * 
     * @access protected
     * @param string $ip The server IP.
     */
	protected function __construct($ip) {
		$this->ip = $ip;
    }

    /**
     * @access private
     */
    private function load(){
        $mysqli = \Database::Instance()->get();

        if($stmt = $mysqli->prepare("SELECT `ip`,`name`,`icon`,`motd`,`players.online`,`players.max`,`players.peak`,`version`,`uptime`,`website`,`twitter`,`parent`,`time` FROM `skinhistory_servers` WHERE `ip` = ?")){
			$stmt->bind_param("s",$this->ip);
			$stmt->execute();
			$stmt->bind_result($ip,$name,$icon,$motd,$playersOnline,$playersMax,$playerPeak,$version,$uptime,$website,$twitter,$parent,$time);

			if($stmt->fetch()){
				// SERVER IS ALREADY IN DATABASE
                $this->ip = $ip;
                $this->name = $name;
                $this->icon = $icon;
                $this->motd = $motd;
                $this->playersOnline = $playersOnline;
                $this->playersMax = $playersMax;
                $this->playerPeak = $playerPeak;
                $this->version = $version;
                $this->uptime = $uptime;
                $this->website = $website;
                $this->twitter = $twitter;
                $this->parent = $parent;
                $this->time = $time;

                $this->exists = true;
                
				$this->saveToCache();
				$stmt->close();
			} else {
                // SERVER IS NOT IN DATABASE
                
                $ping = ServerPing::executeServerPing($this->ip,false);
                if(!is_null($ping)){
                    if($ping->isSuccessful()){
                        $this->updateData($ping,false);
    
                        $this->uptime = 100.00;
                        $this->time = gmdate("Y-m-d H:i:s");

                        $this->blacklisted = filter_var($this->ip,FILTER_VALIDATE_IP,FILTER_FLAG_IPV4) ? true : false;
    
                        $mysqli = \Database::Instance()->get();

                        $stmt = $mysqli->prepare("INSERT INTO `skinhistory_servers` (`ip`,`name`,`icon`,`motd`,`players.online`,`players.max`,`players.peak`,`version`,`uptime`,`blacklisted`,`time`) VALUES(?,?,?,?,?,?,?,?,?,?,?);");
                        $stmt->bind_param("ssssiiisdis",$this->ip,$this->name,$this->icon,$this->motd,$this->playersOnline,$this->playersMax,$this->playerPeak,$this->version,$this->uptime,$this->blacklisted,$this->time);
                        if($stmt->execute()){
                            $this->exists = true;
                            $this->saveToCache();
                        }
                        $stmt->close();
                    }
                }
			}
		}
    }

    /**
     * @access public
     * @return string The IP address of this server.
     */
    public function getIP(){
        return $this->ip;
    }

    /**
     * @access public
     * @return string The custom name of this server, null if there is no custom name.
     */
    public function getName(){
        return $this->name;
    }

    /**
     * @access public
     * @return string The icon of this server, null if there is no icon.
     */
    public function getIcon(){
        return $this->icon;
    }

    /**
     * @access public
     * @return string The motd of this server.
     */
    public function getMotd(){
        return $this->motd;
    }

    /**
     * @access public
     * @return string The motd of this server with it's color codes converted to HTML.
     */
    public function getConvertedMotd(){
        if(!is_null($this->motd)){
            return MinecraftColors::convertToHTML($this->motd,true);
        }

        return null;
    }

    /**
     * @access public
     * @return int The players currently online on this server.
     */
    public function getPlayersOnline(){
        return $this->playersOnline;
    }

    /**
     * @access public
     * @return int The maximum amount of players this server holds.
     */
    public function getPlayersMax(){
        return $this->playersMax;
    }

    /**
     * @access public
     * @return int The highest amount of players that was ever online on this server.
     */
    public function getPlayerPeak(){
        return $this->playerPeak;
    }

    /**
     * @access public
     * @return string The version string of this server.
     */
    public function getVersion(){
        return $this->version;
    }

    /**
     * @access public
     * @return double The uptime percentage of this server.
     */
    public function getUptime(){
        return $this->uptime;
    }

    /**
     * @access public
     * @return string|null The website of this server, null if none has been set.
     */
    public function getWebsite(){
        return $this->website;
    }

    /**
     * @access public
     * @return string|null The twitter handle of this server, null if none has been set.
     */
    public function getTwitter(){
        return $this->twitter;
    }

    /**
     * @access public
     * @return Server|null The parent server of this server, fetched from the parent IP, null if no parent has been set.
     */
    public function getParent(){
        return !is_null($this->parent) ? self::getServer($this->parent) : null;
    }

    /**
     * @access public
     * @return string|null The IP of the parent server of this server, null if none has been set.
     */
    public function getParentIP(){
        return $this->parent;
    }

    /**
     * @access public
     * @return string[] The children IPs of this server.
     */
    public function getChildren(){
        if(is_null($this->children)){
            $this->children = [];

            $mysqli = \Database::Instance()->get();

            $stmt = $mysqli->prepare("SELECT `ip` FROM `skinhistory_servers` WHERE `parent` = ?");
            $stmt->bind_param("s",$this->ip);
            if($stmt->execute()){
                $result = $stmt->get_result();

                if($result->num_rows){
                    while($row = $result->fetch_assoc()){
                        array_push($this->children,$row["ip"]);
                    }
                }
            }
            $stmt->close();

            $this->saveToCache();
        }

        return $this->children;
    }

    /**
     * @access public
     * @return bool If true, the server has been blacklisted and will not be shown on the server lists.
     */
    public function isBlacklisted(){
        return $this->blacklisted;
    }

    /**
     * @access public
     * @return string The timestamp when this server was added to the database.
     */
    public function getTime(){
        return $this->time;
    }

    /**
     * Updates this servers data with the passed server ping.
     * 
     * @access public
     * @param ServerPing $ping The server ping.
     * @param bool $updateDatabase If true, the server data in the database will be updated as well.
     */
    public function updateData($ping,$updateDatabase = true){
        $this->icon = $ping->getIcon();
        $this->motd = $ping->getMotd();
        $this->playersOnline = $ping->getPlayersOnline();
        $this->playersMax = $ping->getPlayersMax();
        if(is_null($this->playerPeak) || $this->playersOnline > $this->playerPeak) $this->playerPeak = $this->playersOnline;
        $this->version = $ping->getVersion();

        if($updateDatabase){
            $mysqli = \Database::Instance()->get();

            $stmt = $mysqli->prepare("UPDATE `skinhistory_servers` SET `icon` = ?, `motd` = ?, `players.online` = ?, `players.max` = ?, `players.peak` = ?, `version` = ?");
            $stmt->bind_param("ssiiis",$this->icon,$this->motd,$this->playersOnline,$this->playersMax,$this->playerPeak,$this->version);
            $stmt->execute();
            $stmt->close();

            $parent = $this->getParent();
            if(!is_null($parent)){
                $parent->updateData($ping,$updateDatabase);
            }

            $this->saveToCache();
        }
    }

    /**
     * @access public
     * @return array[] Player samples for use with a chart.
     */
    public function getPlayerSamples(){
        $n = "playerSamples_" . $this->ip;

        if(\CacheHandler::existsInCache($n)){
            return \CacheHandler::getFromCache($n);
        } else {
            $samples = [];

            $mysqli = \Database::Instance()->get();

            $stmt = $mysqli->prepare("SELECT `date`,`hour`,`players.online` FROM `skinhistory_serverpings` WHERE `ip` = ? AND `successful` = 1 ORDER BY `date` DESC, `hour` DESC LIMIT 24");
            $stmt->bind_param("s",$this->ip);

            if($stmt->execute()){
                $result = $stmt->get_result();

                if($result->num_rows){
                    while($row = $result->fetch_assoc()){
                        array_push($samples,["date" => $row["date"],"hour" => $row["hour"],"players" => $row["players.online"]]);
                    }
                }
            }

            $stmt->close();

            \CacheHandler::setToCache($n,$samples,5*60);

            return $samples;
        }
    }
    
    /**
     * Saves this server to the cache.
     * 
     * @access public
     */
    public function saveToCache(){
        \CacheHandler::setToCache("server_" . $this->ip,$this,5*60);
    }
}