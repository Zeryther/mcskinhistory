# MCSkinHistory

MCSkinHistory is an automatic database for Minecraft skins, capes, names and servers.

## Dependencies
- PHP 7.2
- MySQL 5
- Apache 2
- composer

## Internationalization

[Check out this repository for information on our i18n project.](https://gitlab.com/Gigadrive/MCSkinHistory/Locales.git)
